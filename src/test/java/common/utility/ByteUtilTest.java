/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.utility
 * FILE      : ByteUtilTest.java
 * DATE      : 2022.09.13
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.utility;

import common.enums.CharacterEnum;
import common.enums.EncodingEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ByteUtilTest
{
	protected Logger logger = LoggerFactory.getLogger( StringUtilTest.class );
	
	@Test
	public void substringByteTest()
	{
		String data1 = "12345678901234567890123456789012345678901234567890";
		
		byte[] array = data1.getBytes( EncodingEnum.EUCKR.getCharset() );
		
		byte[] temp1 = ByteUtil.substringByte( array, 0, 15 );
		String sub1 = new String(temp1, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub1 : [{}]", sub1 );
		assertEquals( sub1, "123456789012345" );
		
		byte[] temp2 = ByteUtil.substringByte( array, 15, 0 );
		assertEquals( temp2, null );
	}
	
	@Test
	public void getStringByteByLengthTest()
	{
		String data1 = "12345678901234567890123456789012345678901234567890";
		
		byte[] array = data1.getBytes( EncodingEnum.EUCKR.getCharset() );
		
		byte[] output1 = ByteUtil.getStringByteByLength( array, 100, CharacterEnum.HYPHEN.getByte() );
		String sub1 = new String(output1, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub1 : [{}]", sub1 );
		
		byte[] output2 = ByteUtil.getStringByteByLength( array, 50, CharacterEnum.HYPHEN.getByte() );
		String sub2 = new String(output2, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub1 : [{}]", sub2 );
		
		
		byte[] output3 = ByteUtil.getStringByteByLength( array, 25, CharacterEnum.HYPHEN.getByte() );
		String sub3 = new String(output3, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub1 : [{}]", sub3 );
	}
	
	@Test
	public void indexOfTest()
	{
		String data1 = "12345678901234567890123456789012345678901234567890";
		
		byte[] array = data1.getBytes( EncodingEnum.EUCKR.getCharset() );
		
		byte[] output1 = ByteUtil.getStringByteByLength( array, 100, CharacterEnum.HYPHEN.getByte() );
		String sub1 = new String(output1, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub1 : [{}]", sub1 );
		
		int index = ByteUtil.indexOf( output1, CharacterEnum.HASH.getByte(), 40 );
		logger.info( "index :[{}]", index );
		assertEquals( index, -1 );
		
		index = ByteUtil.indexOf( output1, CharacterEnum.HYPHEN.getByte(), 40 );
		logger.info( "index :[{}]", index );
		assertEquals( index, 50 );
		
		index = ByteUtil.indexOf( output1, (byte)'0' );
		logger.info( "index :[{}]", index );
		assertEquals( index, 9 );
	}
	
	@Test
	public void getNumericByteByLengthTest()
	{
		String data1 = "1234567890";
		
		byte[] array = data1.getBytes( EncodingEnum.EUCKR.getCharset() );
		
		byte[] output1 = ByteUtil.getNumericByteByLength( array, 15, 0, CharacterEnum.ZERO.getByte() );
		String sub1 = new String(output1, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub1 : [{}]", sub1 );
		assertEquals( sub1, "000001234567890" );
		
		byte[] output2 = ByteUtil.getNumericByteByLength( array, 18, 3, CharacterEnum.ZERO.getByte() );
		String sub2 = new String(output2, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub2 : [{}]", sub2 );
		assertEquals( sub2, "00001234567890.000" );
		
		String data2 = "12345678901234567890";
		
		byte[] array2 = data2.getBytes( EncodingEnum.EUCKR.getCharset() );
		
		byte[] output3 = ByteUtil.getNumericByteByLength( array2, 15, 0, CharacterEnum.ZERO.getByte() );
		String sub3 = new String(output3, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub3 : [{}]", sub3 );
		assertEquals( sub3, "123456789012345" );
		
		byte[] output4 = ByteUtil.getNumericByteByLength( array2, 18, 3, CharacterEnum.ZERO.getByte() );
		String sub4 = new String(output4, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub4 : [{}]", sub4 );
		assertEquals( sub4, "12345678901234.000" );
		
		
		String data3 = "1234567890.";
		
		byte[] array3 = data3.getBytes( EncodingEnum.EUCKR.getCharset() );
		
		byte[] output5 = ByteUtil.getNumericByteByLength( array3, 15, 3, CharacterEnum.ZERO.getByte() );
		String sub5 = new String(output5, EncodingEnum.EUCKR.getCharset() );
		logger.info( "sub5 : [{}]", sub5 );
	}
}
