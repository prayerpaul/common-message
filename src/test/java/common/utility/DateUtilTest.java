/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.utility
 * FILE      : DateUtilTest.java
 * DATE      : 2022.09.22
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.text.ParseException;

import static org.testng.AssertJUnit.assertEquals;

public class DateUtilTest
{
	private static Logger logger = LoggerFactory.getLogger( DateUtilTest.class );
	
	@Test
	public void changeDateFormatTest() throws ParseException
	{
		String format1 = "yyyy-MM-dd HH:mm:ss.SSS";
		String format2 = "yyyyMMdd";
		
		String date = DateUtil.getDateTime( format1 );
		String newDate = DateUtil.changeDateFormat( date, format1, format2 );
		logger.info( "[{}] : [{}]", date, newDate );
		assertEquals( newDate, "20220922" );
	}
}
