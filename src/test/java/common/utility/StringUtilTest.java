/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.utility
 * FILE      : StringUtilTest.java
 * DATE      : 2022.09.11
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.utility;

import common.enums.CharacterEnum;
import common.exception.MessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class StringUtilTest
{
	protected Logger logger = LoggerFactory.getLogger( StringUtilTest.class );
	
	@Test
	public void getNumberValueByLengthTest() throws MessageException
	{
		String longVal0 = StringUtil.getNumberValueByLength( "", 13, 0, CharacterEnum.ZERO.getChar() );
		logger.info( "[{}]", longVal0 );
		assertEquals( longVal0, "0000000000000" );
		
		String doubleVal0 = StringUtil.getNumberValueByLength( "", 18, 3, CharacterEnum.ZERO.getChar() );
		logger.info( "[{}]", doubleVal0 );
		assertEquals( doubleVal0, "00000000000000.000" );
		
		String longVal1 = StringUtil.getNumberValueByLength( "12345", 13, 0, CharacterEnum.ZERO.getChar() );
		logger.info( "[{}]", longVal1 );
		assertEquals( longVal1, "0000000012345" );
		
		String longVal2 = StringUtil.getNumberValueByLength( "12345.12345", 13, 0, CharacterEnum.SPACE.getChar() );
		logger.info( "[{}]", longVal2 );
		assertEquals( longVal2, "        12345" );
		
		String doubleVal1 = StringUtil.getNumberValueByLength( "12345", 18, 3, CharacterEnum.ZERO.getChar() );
		logger.info( "[{}]", doubleVal1 );
		assertEquals( doubleVal1, "00000000012345.000" );
		
		String doubleVal2 = StringUtil.getNumberValueByLength( "12345.67890", 18, 3, CharacterEnum.ZERO.getChar() );
		logger.info( "[{}]", doubleVal2 );
		assertEquals( doubleVal2, "00000000012345.678" );
		
		String doubleVal3 = StringUtil.getNumberValueByLength( "12345.678", 18, 3, CharacterEnum.ZERO.getChar() );
		logger.info( "[{}]", doubleVal3 );
		assertEquals( doubleVal3, "00000000012345.678" );
		
		assertEquals( "12345".matches("^([-+]?[0-9]+)(.[0-9]*)?$"), true );
		assertEquals( "-12345".matches("^([-+]?[0-9]+)(.[0-9]*)?$"), true );
		assertEquals( "-12345.123".matches("^([-+]?[0-9]+)(.[0-9]*)?$"), true );
		assertEquals( "12345.123".matches("^([-+]?[0-9]+)(.[0-9]*)?$"), true );
	}
	
	@Test
	public void combineHangulTest() throws MessageException, NoSuchAlgorithmException
	{
		Random sr = new SecureRandom();
		
		for( int i=0, n=100; i < n; ++i )
		{
			String t1 = null, t2 = null, t3 = null;
			for( int j=0, k=3; j < k; ++ j )
			{
				int c = sr.nextInt( StringUtil.HANGUL_CHOSUNG.length );
				int m = sr.nextInt( StringUtil.HANGUL_JUNGSUNG.length );
				int l = sr.nextInt( StringUtil.HANGUL_JONGSUNG.length );
				
				if( j == 0 ) t1 = StringUtil.assembleHangul( c, m, l );
				if( j == 1 ) t2 = StringUtil.assembleHangul( c, m, l );
				if( j == 2 ) t3 = StringUtil.assembleHangul( c, m, l );
			}
			//String t = t1 + t2 + t3;
			logger.info( "HANGUL String {},{},{}", t1, t2, t3 );
		}
	}
	
	@Test
	public void randomHangulTest() throws MessageException, NoSuchAlgorithmException
	{
		for( int i=0, n=1000; i < n; ++i )
		{
			logger.info( "{}", StringUtil.randomHangul(4) );
		}
	}
	
	@Test
	public void randomHangulNameTest() throws MessageException, NoSuchAlgorithmException
	{
		for( int i=0, n=1000; i < n; ++i )
		{
			logger.info( "{}", StringUtil.randomHangulName(3) );
		}
	}
}
