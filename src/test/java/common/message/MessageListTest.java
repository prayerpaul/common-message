package common.message;

import common.enums.EncodingEnum;
import common.enums.FieldEnum;
import common.enums.MessageMacroEnum;
import common.exception.MessageException;
import common.message.control.MessageTextBuilder;
import common.message.control.MessageXMLParser;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

public class MessageListTest extends MessageTestCase
{
	@Test
	public void findElementGroupTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001787_RES" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageGroup mg = (MessageGroup)m.findElement( "연금및보험확인_사회보험_건강보험B_여신.건강·장기요양보험료납부확인서지역가입자" );
		assertNotNull( mg );
		
		logger.info( "[{}] : [{}], [{}]", mg.getElement(), mg.getElementId(), mg.getElementCount() );
	}
	
	@Test
	public void findElementListTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001787_RES" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageList ml = (MessageList)m.findElement( "연금및보험확인_사회보험_건강보험B_여신.건강·장기요양보험료납부확인서지역가입자.발급항목" );
		assertNotNull( ml );
		
		logger.info( "[{}] : [{}], [{}]", ml.getElement(), ml.getElementId(), ml.getLayoutElementCount() );
	}
	
	@Test
	public void cloneMessageListTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001787_RES" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageList ml = (MessageList)m.findElement( "연금및보험확인_사회보험_건강보험B_여신.건강·장기요양보험료납부확인서지역가입자.발급항목" );
		assertNotNull( ml );
		
		logger.info( "[{}] : [{}], [{}]", ml.getElement(), ml.getElementId(), ml.getLayoutElementCount() );
		
		MessageList clone = ml.clone();
		assertNotEquals( ml, clone );
	}
	
	@Test
	public void fixedRecordMacroTest() throws MessageException
	{
		String data = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
				"\t\t<주민등록표등·초본>\n" +
				"\t\t\t<세대원정보>\n" +
				"\t            <성명>홍길동</성명>\n" +
				"\t            <세대주관계>본인</세대주관계>\n" +
				"\t            <세대편입일>20100302</세대편입일>\n" +
				"\t            <변동일>20100302</변동일>\n" +
				"\t            <변동사유>전입</변동사유>\n" +
				"\t        </세대원정보>\n" +
				"\t\t\t<세대원정보>\n" +
				"\t            <성명>홍영희</성명>\n" +
				"\t            <세대주관계>배우자</세대주관계>\n" +
				"\t            <세대편입일>20151001</세대편입일>\n" +
				"\t            <변동일>20151001</변동일>\n" +
				"\t            <변동사유>전입</변동사유>\n" +
				"\t        </세대원정보>\n" +
				"\t        <세대원정보>\n" +
				"\t            <성명>홍일일</성명>\n" +
				"\t            <세대주관계>자녀</세대주관계>\n" +
				"\t            <세대편입일>20180101</세대편입일>\n" +
				"\t            <변동일>20180101</변동일>\n" +
				"\t            <변동사유>출생등록</변동사유>\n" +
				"\t        </세대원정보>\n" +
				"\t        <세대원정보>\n" +
				"\t            <성명>홍이이</성명>\n" +
				"\t            <세대주관계>자녀</세대주관계>\n" +
				"\t            <세대편입일>20190101</세대편입일>\n" +
				"\t            <변동일>20190101</변동일>\n" +
				"\t            <변동사유>출생등록</변동사유>\n" +
				"\t        </세대원정보>\n" +
				"\t\t</주민등록표등·초본>";
		
		MessageGroup mg = new MessageGroup("주민등록표등·초본", "" );
		MessageList  ml = new MessageList ( "세대원정보", "" );
		ml.addPreMacro( MessageMacroEnum.MACRO_FIXED_SIZE);
		ml.addPreMacro( MessageMacroEnum.MACRO_DELIMITER_BAR );
		ml.setDefaultValue( "10" );
		
		MessageField mf1 = new MessageField( "성명", "" );
		mf1.setLength( 100 );
		mf1.setFieldType( FieldEnum.STRING );
		MessageField mf2 = new MessageField( "세대주관계", "" );
		mf2.setLength( 40 );
		mf2.setFieldType( FieldEnum.STRING );
		MessageField mf3 = new MessageField( "세대편입일", "" );
		mf3.setLength( 8 );
		mf3.setFieldType( FieldEnum.STRING );
		MessageField mf4 = new MessageField( "변동일", "" );
		mf4.setLength( 8 );
		mf4.setFieldType( FieldEnum.STRING );
		MessageField mf5 = new MessageField( "변동사유", "" );
		mf5.setLength( 50 );
		mf5.setFieldType( FieldEnum.STRING );
		
		ml.appendLayoutElement( mf1 );
		ml.appendLayoutElement( mf2 );
		ml.appendLayoutElement( mf3 );
		ml.appendLayoutElement( mf4 );
		ml.appendLayoutElement( mf5 );
		
		mg.appendElement( ml );
		
		MessageXMLParser mxp = new MessageXMLParser( data, EncodingEnum.UTF8 );
		mxp.visitGroup( mg );
		
		MessageTextBuilder mtb = new MessageTextBuilder( EncodingEnum.MS949 );
		mtb.visitGroup( mg );
		logger.info( "[{}]", mtb.buildString() );
	}
}
