/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message
 * FILE      : MessageTest.java
 * DATE      : 2022.08.15
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class MessageTest extends MessageTestCase
{
	@Test
	public void findElementFieldTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "DATASET_REQ" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageField mf = (MessageField)m.findElement( "Envelope.Header.commonHeader.useSystemCode" );
		assertNotNull( mf );
		
		logger.info( "[{}] : [{}]", mf.getElement(), mf.getElementId() );
	}
}
