package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.Message;
import org.testng.annotations.Test;

public class MessageTextBuilderTest extends ControlTestCase
{
	@Test
	public void MDS0001617_IN_Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001617_IN";
		String dataId = msgId + "_TXT";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.info( "[{}] : [{}]", dataId, data );
		
		AbstractVisitor tpv = new MessageTextParser( data, EncodingEnum.EUCKR );
		m.accept( tpv );
		
		MessageTextPrinter pv1 = new MessageTextPrinter( EncodingEnum.EUCKR );
		m.accept( pv1 );
		logger.info( pv1.print() );
		
		MessageTextPrinter pv2 = new MessageTextPrinter( EncodingEnum.UTF8 );
		m.accept( pv2 );
		logger.info( pv2.print() );
		
		MessageTextBuilder mtb1 = new MessageTextBuilder( EncodingEnum.UTF8 );
		m.accept( mtb1 );
		logger.info( "[{}], [{}]", mtb1.buildBytes().length, mtb1.buildString() );
		
		
		MessageTextBuilder mtb2 = new MessageTextBuilder( EncodingEnum.EUCKR );
		m.accept( mtb2 );
		logger.info( "[{}], [{}]", mtb2.buildBytes().length, mtb2.buildString() );
	}
	
	@Test
	public void MDS0001617_RES_Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001617_RES";
		String dataId = msgId + "_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.info( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser tpv = new MessageXMLParser( data, EncodingEnum.UTF8 );
		tpv.setElementPath( "Message" );
		m.accept( tpv );
		
		MessageTextBuilder mtb1 = new MessageTextBuilder( EncodingEnum.EUCKR );
		m.accept( mtb1 );
		logger.info( "[{}], [{}]", mtb1.buildBytes().length, mtb1.buildString() );
		
		MessageTextPrinter pv1 = new MessageTextPrinter( EncodingEnum.EUCKR );
		m.accept( pv1 );
		logger.info( pv1.print() );
	}
	
	@Test
	public void SOCKET_COMMON_Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "SOCKET_COMMON";
		//String dataId = msgId + "_XML";
		//String data   = dataRepo.get( dataId );
		Message m1     = mm.getMessage( msgId );
		Message m2    = mm.getMessage( "MDS0001617_RES" );
		String data   = dataRepo.get( "MDS0001617_RES_XML" );
		
		MessageXMLParser mxp = new MessageXMLParser( data, EncodingEnum.UTF8 );
		mxp.setElementPath( "Message" );
		m2.accept( mxp );
		
		MessageTextBuilder mtb1 = new MessageTextBuilder( EncodingEnum.EUCKR );
		m1.accept( mtb1 );
		m2.accept( mtb1 );
		
		logger.info( "[{}], [{}]", mtb1.buildBytes().length, mtb1.buildString() );
		
		MessageTextPrinter pv1 = new MessageTextPrinter( EncodingEnum.EUCKR );
		m1.accept( pv1 );
		m2.accept( pv1 );
		logger.info( pv1.print() );
	}
}
