package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.Message;
import org.testng.annotations.Test;

public class MessageTextParserTest extends ControlTestCase
{
	@Test
	public void parseTextTest() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001617_IN";
		String dataId = msgId + "_TXT";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.info( "[{}] : [{}]", dataId, data );
		
		AbstractVisitor tpv = new MessageTextParser( data, EncodingEnum.EUCKR );
		m.accept( tpv );
		
		MessageTextPrinter pv1 = new MessageTextPrinter( EncodingEnum.EUCKR );
		m.accept( pv1 );
		logger.info( pv1.print() );
		
		MessageTextPrinter pv2 = new MessageTextPrinter( EncodingEnum.UTF8 );
		m.accept( pv2 );
		logger.info( pv2.print() );
	}
}
