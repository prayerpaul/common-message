package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.Message;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class MessageJSONBuilderTest extends ControlTestCase
{
	@Test
	public void setElementPathTest()
	{
		MessageJSONBuilder mjb = new MessageJSONBuilder( EncodingEnum.UTF8 );
		mjb.setElementPath( "Envelope/Body/response" );
		logger.info( "{}", mjb.build() );
	}
	
	@Test
	public void createElementPathTest()
	{
		JSONObject root = new JSONObject();
		MessageJSONBuilder mjb = new MessageJSONBuilder( EncodingEnum.UTF8 );
		mjb.createElementPath( root, "Envelope/Body/response" );
		logger.info( "{}", root );
		JSONObject obj2 = mjb.createElementPath( root, "Envelope/Body/response/Test" );
		JSONObject obj3 = new JSONObject();
		obj2.put( "Test2", obj3 );
		logger.info( "{}", root );
	}
	
	@Test
	public void XMLMDS0001617Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001617_RES";
		String dataId = msgId + "_JSON";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageJSONParser xpv = new MessageJSONParser( data, EncodingEnum.UTF8 );
		m.accept( xpv );
		
		MessageJSONBuilder jbl1 = new MessageJSONBuilder( EncodingEnum.UTF8 );
		jbl1.setElementPath( "Response" );
		m.accept( jbl1 );
		
		MessageJSONBuilder jbl2 = new MessageJSONBuilder( EncodingEnum.UTF8 );
		m.accept( jbl2 );

		logger.info( "CASE 1. [{}]", jbl1.build() );
		logger.info( "CASE 2. [{}]", jbl2.build() );
	}
	
	@Test
	public void XMLMDS0001791Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001791_RES";
		String dataId = msgId + "_JSON";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageJSONParser xpv = new MessageJSONParser( data, EncodingEnum.UTF8 );
		m.accept( xpv );
		
		MessageJSONBuilder jbl = new MessageJSONBuilder( EncodingEnum.UTF8 );
		m.accept( jbl );
		logger.info( "{}", jbl.build() );
	}
	
	@Test
	public void buildMultiMessagesTest() throws MessageException, CloneNotSupportedException
	{
		String msgId1  = "MDS0001617_RES";
		String dataId1 = msgId1 + "_JSON";
		String data1   = dataRepo.get( dataId1 );
		Message m1     = mm.getMessage( msgId1 );
		logger.debug( "[{}] : [{}]", dataId1, data1 );
		
		String msgId2 = "MDS0001760_RES";
		String dataId2 = msgId2 + "_JSON";
		String data2   = dataRepo.get( dataId2 );
		Message m2     = mm.getMessage( msgId2 );
		logger.debug( "[{}] : [{}]", dataId2, data2 );
		
		String msgId3 = "MDS0001791_RES";
		String dataId3 = msgId3 + "_JSON";
		String data3   = dataRepo.get( dataId3 );
		Message m3     = mm.getMessage( msgId3 );
		logger.debug( "[{}] : [{}]", dataId3, data3 );
		
		MessageJSONParser xpv1 = new MessageJSONParser( data1, EncodingEnum.UTF8 );
		//xpv1.setDocumentRoot( "Message" );
		m1.accept( xpv1 );
		MessageJSONParser xpv2 = new MessageJSONParser( data2, EncodingEnum.UTF8 );
		//xpv2.setDocumentRoot( "Message" );
		m2.accept( xpv2 );
		MessageJSONParser xpv3 = new MessageJSONParser( data3, EncodingEnum.UTF8 );
		//xpv2.setDocumentRoot( "Message" );
		m3.accept( xpv3 );
		
		MessageJSONBuilder mxb = new MessageJSONBuilder( EncodingEnum.UTF8 );
		mxb.setElementPath( "Envelope/Body/Response");
		m1.accept( mxb );
		//mxb.setElementPath( "Envelope/Body/Response/MDS0001760_RES");
		m2.accept( mxb );
		//mxb.setElementPath( "Envelope/Body/Response/MDS0001791_RES");
		m3.accept( mxb );
		logger.info( mxb.build() );
	}
	
	@Test
	public void buildMultiXMLMessages2JSONTest() throws MessageException, CloneNotSupportedException
	{
		String msgId1  = "MDS0001617_RES";
		String dataId1 = msgId1 + "_XML";
		String data1   = dataRepo.get( dataId1 );
		Message m1     = mm.getMessage( msgId1 );
		logger.debug( "[{}] : [{}]", dataId1, data1 );
		
		String msgId2 = "MDS0001760_RES";
		String dataId2 = msgId2 + "_XML";
		String data2   = dataRepo.get( dataId2 );
		Message m2     = mm.getMessage( msgId2 );
		logger.debug( "[{}] : [{}]", dataId2, data2 );
		
		String msgId3 = "MDS0001791_RES";
		String dataId3 = msgId3 + "_XML";
		String data3   = dataRepo.get( dataId3 );
		Message m3     = mm.getMessage( msgId3 );
		logger.debug( "[{}] : [{}]", dataId3, data3 );
		
		MessageXMLParser xpv1 = new MessageXMLParser( data1, EncodingEnum.UTF8 );
		xpv1.setElementPath( "Message" );
		m1.accept( xpv1 );
		MessageXMLParser xpv2 = new MessageXMLParser( data2, EncodingEnum.UTF8 );
		xpv2.setElementPath( "Message" );
		m2.accept( xpv2 );
		MessageXMLParser xpv3 = new MessageXMLParser( data3, EncodingEnum.UTF8 );
		xpv3.setElementPath( "Message" );
		m3.accept( xpv3 );
		
		MessageJSONBuilder mxb = new MessageJSONBuilder( EncodingEnum.UTF8 );
		mxb.setElementPath( "Envelope/Body/Response" );
		m1.accept( mxb );
		m2.accept( mxb );
		m3.accept( mxb );
		logger.info( mxb.build() );
	}
}
