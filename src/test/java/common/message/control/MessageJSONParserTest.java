/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message.control
 * FILE      : MessageJSONParserTest.java
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.Message;
import org.testng.annotations.Test;

public class MessageJSONParserTest extends ControlTestCase
{
	@Test
	public void MDS0001617Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001617_RES";
		String dataId = msgId + "_JSON";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageJSONParser xpv = new MessageJSONParser( data, EncodingEnum.UTF8 );
		//xpv.setDocumentRoot( "Message" );
		m.accept( xpv );
	}
	
	@Test
	public void MDS0001760Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001760_RES";
		String dataId = msgId + "_JSON";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageJSONParser xpv = new MessageJSONParser( data, EncodingEnum.UTF8 );
		//xpv.setDocumentRoot( "Message" );
		m.accept( xpv );
	}
	
	@Test
	public void MDS0001791Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001791_RES";
		String dataId = msgId + "_JSON";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageJSONParser xpv = new MessageJSONParser( data, EncodingEnum.UTF8 );
		//xpv.setDocumentRoot( "Message" );
		m.accept( xpv );
	}
	
	@Test
	public void MULTI_RES1_Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MULTI_RES";
		String dataId = msgId + "_JSON1";
		String data   = dataRepo.get( dataId );
		Message m1     = mm.getMessage( "MDS0001617_RES" );
		Message m2     = mm.getMessage( "MDS0001760_RES" );
		Message m3     = mm.getMessage( "MDS0001791_RES" );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageJSONParser mjp = new MessageJSONParser( data, EncodingEnum.UTF8 );
		//mjp.setElementPath( "본인정보확인_여신" );
		m1.accept( mjp );
		//mjp.setElementPath( "개인자산확인_소득_여신" );
		m2.accept( mjp );
		//jp.setElementPath( "연금및보험확인_사회보험_건강보험A_여신" );
		m3.accept( mjp );
		
		MessageTextPrinter mtp = new MessageTextPrinter( EncodingEnum.MS949 );
		m1.accept( mtp );
		m2.accept( mtp );
		m3.accept( mtp );
		logger.info( "{}", mtp.print() );
	}
	
	@Test
	public void MULTI_RES2_Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MULTI_RES";
		String dataId = msgId + "_JSON2";
		String data   = dataRepo.get( dataId );
		Message m1     = mm.getMessage( "MDS0001617_RES" );
		Message m2     = mm.getMessage( "MDS0001760_RES" );
		Message m3     = mm.getMessage( "MDS0001791_RES" );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageJSONParser mjp = new MessageJSONParser( data, EncodingEnum.UTF8 );
		mjp.setElementPath( "Envelope/Body/Response" );
		m1.accept( mjp );
		mjp.setElementPath( "Envelope/Body/Response" );
		m2.accept( mjp );
		mjp.setElementPath( "Envelope/Body/Response" );
		m3.accept( mjp );
		
		MessageJSONBuilder mtp = new MessageJSONBuilder( EncodingEnum.MS949 );
		mtp.setElementPath( "Envelope/Body/Response" );
		m1.accept( mtp );
		mtp.setElementPath( "Envelope/Body/Response" );
		m2.accept( mtp );
		mtp.setElementPath( "Envelope/Body" );
		m3.accept( mtp );
		logger.info( "{}", mtp.build() );
	}
}
