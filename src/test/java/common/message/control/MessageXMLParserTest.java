package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.Message;
import org.testng.annotations.Test;

public class MessageXMLParserTest extends ControlTestCase
{
	@Test
	public void XMLMDS0001617Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001617_RES";
		String dataId = msgId + "_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser xpv = new MessageXMLParser( data, EncodingEnum.UTF8 );
		xpv.setElementPath( "Message" );
		m.accept( xpv );
	}
	
	@Test
	public void XMLMDS0001760Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001760_RES";
		String dataId = msgId + "_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser xpv = new MessageXMLParser( data, EncodingEnum.UTF8 );
		xpv.setElementPath( "Message" );
		m.accept( xpv );
	}
	
	
	@Test
	public void XMLMDS0001791Test() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001791_RES";
		String dataId = msgId + "_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser xpv = new MessageXMLParser( data, EncodingEnum.UTF8 );
		xpv.setElementPath( "Message" );
		m.accept( xpv );
	}
}
