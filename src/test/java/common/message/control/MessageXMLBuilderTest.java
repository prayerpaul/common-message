package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.Message;
import org.testng.annotations.Test;

public class MessageXMLBuilderTest extends ControlTestCase
{
	@Test
	public void XMLMDS0001617Test() throws MessageException, CloneNotSupportedException
	{
		String msgId  = "MDS0001617_RES";
		String dataId = msgId + "_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.info( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser xpv = new MessageXMLParser( data, EncodingEnum.EUCKR );
		xpv.setElementPath( "Message" );
		m.accept( xpv );
		
		MessageXMLBuilder mxb = new MessageXMLBuilder( EncodingEnum.UTF8 );
		mxb.setElementPath( "Envelope/Body/Response" );
		m.accept( mxb );
		logger.info( mxb.build() );
	}
	
	@Test
	public void XMLMDS0001760Test() throws MessageException, CloneNotSupportedException
	{
		String msgId  = "MDS0001760_RES";
		String dataId = msgId + "_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.info( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser xpv = new MessageXMLParser( data, EncodingEnum.EUCKR );
		xpv.setElementPath( "Message" );
		m.accept( xpv );
		
		MessageXMLBuilder mxb = new MessageXMLBuilder( EncodingEnum.UTF8 );
		mxb.setElementPath( "Response" );
		m.accept( mxb );
		logger.info( mxb.build() );
	}
	
	@Test
	public void XMLMDS0001791Test() throws MessageException, CloneNotSupportedException
	{
		String msgId  = "MDS0001791_RES";
		String dataId = msgId + "_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.info( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser xpv = new MessageXMLParser( data, EncodingEnum.EUCKR );
		xpv.setElementPath( "Message" );
		m.accept( xpv );
		
		MessageXMLBuilder mxb = new MessageXMLBuilder( EncodingEnum.UTF8 );
		mxb.setElementPath( "response" );
		m.accept( mxb );
		logger.info( mxb.build() );
		
		MessageTextPrinter mtp = new MessageTextPrinter( EncodingEnum.MS949 );
		m.accept( mtp );
		logger.info( mtp.print() );
	}
	
	@Test
	public void buildMultiMessagesTest() throws MessageException, CloneNotSupportedException
	{
		String msgId1  = "MDS0001617_RES";
		String dataId1 = msgId1 + "_XML";
		String data1   = dataRepo.get( dataId1 );
		Message m1     = mm.getMessage( msgId1 );
		logger.debug( "[{}] : [{}]", dataId1, data1 );
		
		String msgId2 = "MDS0001760_RES";
		String dataId2 = msgId2 + "_XML";
		String data2   = dataRepo.get( dataId2 );
		Message m2     = mm.getMessage( msgId2 );
		logger.debug( "[{}] : [{}]", dataId2, data2 );
		
		String msgId3 = "MDS0001791_RES";
		String dataId3 = msgId3 + "_XML";
		String data3   = dataRepo.get( dataId3 );
		Message m3     = mm.getMessage( msgId3 );
		logger.debug( "[{}] : [{}]", dataId3, data3 );
		
		MessageXMLParser xpv1 = new MessageXMLParser( data1, EncodingEnum.UTF8 );
		xpv1.setElementPath( "Message" );
		m1.accept( xpv1 );
		MessageXMLParser xpv2 = new MessageXMLParser( data2, EncodingEnum.UTF8 );
		xpv2.setElementPath( "Message" );
		m2.accept( xpv2 );
		MessageXMLParser xpv3 = new MessageXMLParser( data3, EncodingEnum.UTF8 );
		xpv3.setElementPath( "Message" );
		m3.accept( xpv3 );
		
		MessageXMLBuilder mxb = new MessageXMLBuilder( EncodingEnum.UTF8 );
		mxb.setElementPath( "response" );
		m1.accept( mxb );
		m2.accept( mxb );
		m3.accept( mxb );
		logger.info( mxb.build() );
		
		MessageTextPrinter mtp = new MessageTextPrinter( EncodingEnum.UTF8 );
		m1.accept( mtp );
		m2.accept( mtp );
		m3.accept( mtp );
		logger.info( mtp.print() );
	}
}
