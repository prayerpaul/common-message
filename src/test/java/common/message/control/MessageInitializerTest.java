/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message.control
 * FILE      : MessageInitializerTest.java
 * DATE      : 2022.08.09
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.Message;
import org.testng.annotations.Test;

public class MessageInitializerTest extends ControlTestCase
{
	@Test
	public void clearMessageTest() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001617_RES";
		String dataId = msgId + "_JSON";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		logger.debug( "[{}] : [{}]", dataId, data );
		
		MessageJSONParser xpv = new MessageJSONParser( data, EncodingEnum.UTF8 );
		m.accept( xpv );
		
		MessageTextPrinter mtp = new MessageTextPrinter( EncodingEnum.MS949 );
		m.accept( mtp );
		logger.info( "{}", mtp.print() );
		
		MessageInitializer mi = new MessageInitializer();
		m.accept( mi );
		
		MessageJSONBuilder jbl = new MessageJSONBuilder( EncodingEnum.UTF8 );
		jbl.setElementPath( "Response" );
		m.accept( jbl );
		logger.info( "{}", jbl.build() );
	}
}
