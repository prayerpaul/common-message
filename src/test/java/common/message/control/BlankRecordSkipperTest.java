/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message.control
 * FILE      : BlankRecordSkipperTest.java
 * DATE      : 2022.08.11
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.Message;
import common.utility.StringUtil;
import org.testng.annotations.Test;

public class BlankRecordSkipperTest extends ControlTestCase
{
	@Test
	public void visitListTest() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0001617_RES";
		String dataId = "MDS0001617_OUT_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		//logger.info( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser tpv = new MessageXMLParser( data, EncodingEnum.UTF8 );
		tpv.setElementPath( "Message" );
		m.accept( tpv );
		
		logger.info( "##### MessageTextBuilder start #####" );
		
		MessageTextBuilder mtb1 = new MessageTextBuilder( EncodingEnum.EUCKR );
		m.accept( mtb1 );
		String textData = mtb1.buildString();
		logger.info( "[{}], [{}]", mtb1.buildBytes().length, textData.substring(10) );
		//logger.info( "{}", StringUtil.HexDump( mtb1.buildBytes()) );
		
		
		Message m1     = mm.getMessage( msgId );
		MessageTextParser mtp = new MessageTextParser( textData.substring(10), EncodingEnum.EUCKR );
		m1.accept( mtp );
		
		MessageTextPrinter printer = new MessageTextPrinter( EncodingEnum.EUCKR );
		m1.accept( printer );
		logger.info( "{}", printer.print() );
	}

	@Test
	public void visitListOfListTest() throws CloneNotSupportedException, MessageException
	{
		String msgId  = "MDS0009999_RES";
		String dataId = "MDS0009999_OUT_XML";
		String data   = dataRepo.get( dataId );
		Message m     = mm.getMessage( msgId );
		
		//logger.info( "[{}] : [{}]", dataId, data );
		
		MessageXMLParser tpv = new MessageXMLParser( data, EncodingEnum.UTF8 );
		tpv.setElementPath( "Message" );
		m.accept( tpv );
		
		logger.info( "##### MessageTextBuilder start #####" );
		
		MessageTextBuilder mtb1 = new MessageTextBuilder( EncodingEnum.EUCKR );
		m.accept( mtb1 );
		String textData = mtb1.buildString();
		logger.info( "[{}], [{}]", mtb1.buildBytes().length, textData.substring(10) );
		//logger.info( "{}", StringUtil.HexDump( mtb1.buildBytes()) );
		
		
		Message m1     = mm.getMessage( msgId );
		MessageTextParser mtp = new MessageTextParser( textData.substring(10), EncodingEnum.EUCKR );
		m1.accept( mtp );
		
		MessageTextPrinter printer = new MessageTextPrinter( EncodingEnum.EUCKR );
		m1.accept( printer );
		logger.info( "{}", printer.print() );
	}
}
