/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message
 * FILE      : MessageArrayTest.java
 * DATE      : 2022.09.06
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import common.enums.ArrayEnum;
import common.enums.EncodingEnum;
import common.enums.FieldEnum;
import common.enums.MessageMacroEnum;
import common.exception.MessageException;
import common.message.control.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;


public class MessageArrayTest  extends MessageTestCase
{
	@Test
	public void formatTest()
	{
		JSONObject root = new JSONObject();
		
		JSONArray array  = new JSONArray();
		
		root.put( "출력정보", array );
		
		JSONObject rec1 = new JSONObject();
		rec1.put( "과세년도", "2019" );
		rec1.put( "자료유형", "B 종합소득세, 연말정산" );
		
		JSONObject rec2 = new JSONObject();
		rec2.put( "과세년도", "2018" );
		rec2.put( "자료유형", "C 연말정산" );
		
		JSONObject rec3 = new JSONObject();
		rec3.put( "과세년도", "2020" );
		rec3.put( "자료유형", "C 연말정산" );
		
		array.add( rec3 );
		array.add( rec1 );
		array.add( rec2 );
		
		logger.info( "{}", root.toJSONString() );
	}
	
	@Test
	public void arrayFormatTest()
	{
		JSONObject root = new JSONObject();
		
		JSONArray array1  = new JSONArray();
		array1.add("2021");
		array1.add("2020");
		array1.add("2019");
		
		JSONArray array2  = new JSONArray();
		array2.add("C 연말정산");
		array2.add("C 연말정산");
		array2.add("C 연말정산");
		
		JSONObject obj = new JSONObject();
		root.put( "출력정보", obj );
		obj.put("과세년도", array1);
		obj.put("자료유형", array2);
		
		
		logger.info( "{}", root.toJSONString() );
	}
	
	@Test
	public void MessageArrayXMLTest() throws MessageException
	{
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
				"<증명내용>\n" +
				"    <출력정보>\n" +
				"        <과세년도>2021</과세년도>\n" +
				"        <자료유형>C 연말정산</자료유형>\n" +
				"        <과세년도>2020</과세년도>\n" +
				"        <자료유형>C 연말정산</자료유형>\n" +
				"        <과세년도>2019</과세년도>\n" +
				"        <자료유형>C 연말정산</자료유형>\n" +
				"    </출력정보>\n" +
				"</증명내용>";
		
		MessageGroup mg1 = new MessageGroup( "증명내용", "" );
		MessageGroup mg2 = new MessageGroup( "출력정보", "" );
		MessageArray ar1 = new MessageArray( "과세년도", "" );
		MessageArray ar2 = new MessageArray( "자료유형", "" );
		ar1.setLength( 4 );
		ar1.setArrayType( ArrayEnum.STRING );
		ar2.setLength( 50 );
		ar2.setArrayType( ArrayEnum.STRING );
		
		mg2.appendElement( ar1 );
		mg2.appendElement( ar2 );
		mg1.appendElement( mg2 );
		
		MessageXMLParser xmp = new MessageXMLParser( xml, EncodingEnum.UTF8 );
		mg1.accept( xmp );
		
		MessageXMLBuilder xmb = new MessageXMLBuilder( EncodingEnum.UTF8 );
		mg1.accept( xmb );
		logger.info( "{}", xmb.build() );
	}
	
	@Test
	public void DefaultTest()
	{
		Long aaa = new Long( "0" );
		logger.info( "[{}]", aaa.longValue() );
	}
	
	@Test
	public void MessageArrayJSONTest() throws MessageException
	{
		String data = "{ \"증명내용\": { \"출력정보\": { \"과세년도\": [2021, 2020, 2019], \"자료유형\": [\"A 연말정산\", \"B 연말정산\", \"C 연말정산\"] } } }";
		
		MessageGroup mg1 = new MessageGroup( "증명내용", "" );
		MessageGroup mg2 = new MessageGroup( "출력정보", "" );
		MessageArray ar1 = new MessageArray( "과세년도", "" );
		MessageArray ar2 = new MessageArray( "자료유형", "" );
		ar1.setLength( 4 );
		ar1.setArrayType( ArrayEnum.NUMBER );
		ar1.addPreMacro( MessageMacroEnum.MACRO_FIXED_SIZE );
		ar1.setDefaultValue( "5" );
		ar2.setLength( 50 );
		ar2.setArrayType( ArrayEnum.STRING );
		
		mg2.appendElement( ar1 );
		mg2.appendElement( ar2 );
		mg1.appendElement( mg2 );
		
		MessageJSONParser mjp = new MessageJSONParser( data, EncodingEnum.UTF8 );
		mg1.accept( mjp );
		
		///*
		MessageTextBuilder xtb = new MessageTextBuilder( EncodingEnum.UTF8 );
		mg1.accept( xtb );
		logger.info( "[{}]", xtb.buildString() );
		//*/
	}
	
	@Test
	public void MessageArrayTextTest() throws MessageException
	{
		String data = "0000320212020201900003A 연말정산B 연말정산C 연말정산";
		
		MessageGroup mg1 = new MessageGroup( "증명내용", "" );
		MessageGroup mg2 = new MessageGroup( "출력정보", "" );
		MessageField mf1 = new MessageField( "과세년도건수", "" );
		MessageField mf2 = new MessageField( "자료유형건수", "" );
		MessageArray ar1 = new MessageArray( "과세년도", "" );
		MessageArray ar2 = new MessageArray( "자료유형", "" );
		mf1.setLength( 5 );
		mf1.setFieldType( FieldEnum.NUMBER );
		mf1.addPreMacro( MessageMacroEnum.MACRO_LIST_CNT );
		mf1.setReferElement( "과세년도" );
		mf1.setPrintable( true );
		mf2.setLength( 5 );
		mf2.setFieldType( FieldEnum.NUMBER );
		mf2.addPreMacro( MessageMacroEnum.MACRO_LIST_CNT );
		mf2.setReferElement( "과세년도" );
		mf2.setPrintable( true );
		
		ar1.setLength( 4 );
		ar1.setArrayType( ArrayEnum.NUMBER );
		ar1.setReferElement( "과세년도건수" );
		
		ar2.setLength( 10 );
		ar2.setArrayType( ArrayEnum.STRING );
		ar2.setReferElement( "자료유형건수" );
		
		mg2.appendElement( mf1 );
		mg2.appendElement( ar1 );
		mg2.appendElement( mf2 );
		mg2.appendElement( ar2 );
		mg1.appendElement( mg2 );
		
		MessageTextParser mtp = new MessageTextParser( data, EncodingEnum.MS949 );
		mg1.accept( mtp );
		
		ar1.setLength( 6 );
		logger.info( "MessageTextBuilder start" );
		///*
		MessageTextBuilder xtb = new MessageTextBuilder( EncodingEnum.MS949 );
		mg1.accept( xtb );
		logger.info( "{}", xtb.build() );
		
		MessageTextPrinter xtp = new MessageTextPrinter( EncodingEnum.MS949 );
		mg1.accept( xtp );
		logger.info( "{}", xtp.print() );
		//*/
	}
}
