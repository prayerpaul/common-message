package common.message;

import common.exception.MessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.io.File;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MessageManagerTest implements TestCase
{
	protected Logger logger = LoggerFactory.getLogger( MessageManagerTest.class );
	private MessageManager mm;
	
	public MessageManagerTest()
	{
		mm = MessageManager.getInstance();
	}
	
	@Test
	public void loadFileTest() throws MessageException, CloneNotSupportedException
	{
		String msgId    = "MDS0001617_IN";
		String filePath = CONFIG_PATH + "/msg/" + msgId + ".xml";
		mm.load( filePath );
		Message m = mm.getMessage( msgId );
		logger.info( "[{}]", m.getElementId() );
	}
	
	@Test
	public void loadFileListTest() throws MessageException
	{
		File dir = new File(CONFIG_PATH + "/msg" );
		assertTrue(dir.isDirectory());
		
		File[] list = dir.listFiles();
		if( null == list )
			return;
			
		for( File f: list )
		{
			logger.info( "FILE : [{}]", f.getPath() );
			mm.load( f.getPath() );
		}
		assertEquals( mm.getMessageCount(), list.length );
	}
}
