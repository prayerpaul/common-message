package common.message;

import common.enums.EncodingEnum;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

public class MessageGroupTest extends MessageTestCase
{
	@Test
	public void cloneMessageFieldTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001617_IN" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageGroup mg = (MessageGroup)m.findElement( "bundleInfo" );
		MessageGroup clone = mg.clone();
		assertNotEquals( mg, clone );
		
		MessageField mf1 = (MessageField) mg.findElement( "bundleInfo.L001K001" );
		mf1.setStringValue( "테스트", EncodingEnum.UTF8 );
		
		MessageField mf2 = (MessageField) clone.findElement( "bundleInfo.L001K001" );
		//mf2.setStringValue( "테스트", EncodingEnum.UTF8 );
		
		assertNotEquals( mf1.getStringValue(EncodingEnum.UTF8), mf2.getStringValue(EncodingEnum.UTF8) );
	}
	
	@Test
	public void cloneMessageGroupTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001617_IN" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageGroup mg = (MessageGroup)m.findElement( "bundleInfo" );
		MessageGroup clone = mg.clone();
		assertNotEquals( mg, clone );
		
		logger.info( "[{}] ::: [{}]", mg, clone );
	}
	
	@Test
	public void findElementGroupTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001617_RES" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageGroup mg = (MessageGroup)m.findElement( "본인정보확인_여신.주민등록표등·초본.열람대상자정보" );
		assertNotNull( mg );
		logger.info( "[{}] : [{}], [{}]", mg.getElement(), mg.getElementId(), mg.getElementCount() );
		
		MessageList ml = (MessageList)m.findElement( "본인정보확인_여신.주민등록표등·초본.세대원정보" );
		assertNotNull( ml );
		logger.info( "[{}] : [{}], [{}]", ml.getElement(), ml.getElementId(), ml.getDataRecordCount() );
	}
	
	@Test
	public void findElementListTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001617_RES" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageList ml = (MessageList)m.findElement( "본인정보확인_여신.주민등록표등·초본.세대원정보" );
		assertNotNull( ml );
		
		logger.info( "[{}] : [{}], [{}]", ml.getElement(), ml.getElementId(), ml.getLayoutElementCount() );
	}
}
