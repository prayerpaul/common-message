package common.message;

import common.exception.MessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;

import java.io.File;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public abstract class MessageTestCase
{
	protected Logger logger = LoggerFactory.getLogger( MessageTestCase.class );
	public static final String CONFIG_PATH = "config";
	protected MessageManager mm = MessageManager.getInstance();
	
	public MessageTestCase() {}
	
	@BeforeTest
	public void setupBeforeTest() throws MessageException
	{
		File dir = new File(CONFIG_PATH + "/msg" );
		assertTrue(dir.isDirectory());
		
		File[] list = dir.listFiles();
		if( null == list )
			return;
		
		for( File f: list )
		{
			logger.info( "FILE : [{}]", f.getPath() );
			mm.load( f.getPath() );
		}
		assertEquals( mm.getMessageCount(), list.length );
	}

}
