package common.message;

import common.enums.CharacterEnum;
import common.enums.ElementEnum;
import common.enums.EncodingEnum;
import common.enums.FieldEnum;
import common.exception.MessageException;
import common.utility.StringUtil;
import org.testng.annotations.*;

import static org.testng.Assert.*;

public class MessageFieldTest extends MessageTestCase
{
	private MessageField cardNo;
	private MessageField userNm;
	
	@BeforeTest
	public void beforeTest()
	{
		cardNo = new MessageField( "카드번호", "" );
		cardNo.setFieldType( FieldEnum.STRING );
		cardNo.setLength( 16 );
		cardNo.setDefaultValue( "1111222233334444" );
		
		userNm = new MessageField( "사용자이름", "" );
		userNm.setFieldType( FieldEnum.STRING );
		userNm.setLength( 50 );
		userNm.setDefaultValue( "헬로우 월드핳!" );
		userNm.setStringValue( "헬로우 월드~~", EncodingEnum.UTF8 );
	}
	
	@Test
	public void DoubleFieldTest() throws MessageException
	{
		MessageField currUSD = new MessageField( "USD", "" );
		currUSD.setFieldType( FieldEnum.NUMBER );
		currUSD.setLength( 18 );
		currUSD.setPoint( 3 );
		currUSD.setDoubleValue( 663.12345 );
		
		logger.info( "[{}]", currUSD.getNumberValue() );
		logger.info( "[{}]", currUSD.getStringValueByLength(EncodingEnum.UTF8, CharacterEnum.ZERO.getByte()) );
		
		logger.info( "[{}]", currUSD.getData() );
		logger.info( "[{}]", currUSD.getData().indexOf(CharacterEnum.PERIOD.getChar()) );
		logger.info( "[{}][{}]", currUSD.getData().substring(0, currUSD.getData().indexOf(CharacterEnum.PERIOD.getChar())), currUSD.getData().substring(currUSD.getData().indexOf(CharacterEnum.PERIOD.getChar())+1) );
		int periodPos = currUSD.getData().indexOf( CharacterEnum.PERIOD.getChar() );
		if( periodPos > 0 )
		{
			String naturalPart = currUSD.getData().substring( 0, periodPos );
			String realPart = currUSD.getData().substring( periodPos + 1 );
			logger.info( "[{}][{}]", naturalPart, realPart );
			logger.info( "{}.{}", StringUtil.lpad(naturalPart, currUSD.getLength()-currUSD.getPointLength()-1, CharacterEnum.ZERO.getChar()), StringUtil.rpad(realPart, currUSD.getPointLength(), CharacterEnum.ZERO.getChar()) );
		}
		else
		{
			logger.info( "{}", String.format("%s.%s", StringUtil.lpad(currUSD.getData(), currUSD.getLength()-currUSD.getPointLength()-1, CharacterEnum.ZERO.getChar())), StringUtil.rpad(CharacterEnum.BLANK.getString(), currUSD.getPointLength(), CharacterEnum.ZERO.getChar()) );
		}
		
		
	}
	
	@Test
	public void cloneTest() throws CloneNotSupportedException
	{
		MessageField clone = cardNo.clone();
		logger.info( "객체 비교" );
		assertNotEquals( clone, cardNo);
		
		logger.info( "ElementEnum 비교" );
		assertEquals( clone.getElement(), ElementEnum.FIELD );
		
		logger.info( "Element ID 비교 [{}]", clone.getElementId() );
		assertEquals( clone.getElementId(), cardNo.getElementId() );
		
		logger.info( "LENGTH 비교 [{}]", clone.getLength() );
		assertEquals( clone.getLength(), cardNo.getLength() );
		
		logger.info( "DEFAULT 비교 [{}]", clone.getDefaultValue() );
		assertEquals( clone.getDefaultValue(), cardNo.getDefaultValue() );
	}
	
	@Test
	public void getByteLengthTest()
	{
		logger.info( "UTF-8 : [{}][{}]", userNm.getStringValue(EncodingEnum.UTF8), userNm.getByteLength(EncodingEnum.UTF8) );
		assertEquals( userNm.getByteLength(EncodingEnum.UTF8), 18 );
		
		logger.info( "EUC-KR : [{}][{}]", userNm.getStringValue(EncodingEnum.EUCKR), userNm.getByteLength(EncodingEnum.EUCKR) );
		assertEquals( userNm.getByteLength(EncodingEnum.EUCKR), 13 );
	}
	
	@Test
	public void getByteValueTest()
	{
		logger.info( "UTF-8 : [{}][{}]", userNm.getByteValue(EncodingEnum.UTF8), userNm.getByteValue(EncodingEnum.UTF8).length );
		assertEquals( userNm.getByteValue(EncodingEnum.UTF8).length, 18 );
		
		logger.info( "EUC-KR : [{}][{}]", userNm.getByteValue(EncodingEnum.EUCKR), userNm.getByteValue(EncodingEnum.EUCKR).length );
		assertEquals( userNm.getByteValue(EncodingEnum.EUCKR).length, 13 );
	}
	
	@Test
	public void getByteValueByLengthTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001617_IN" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageField mf = (MessageField)m.findElement( "bundleInfo.L001K001" );
		mf.setStringValue( "홍길동", EncodingEnum.UTF8 );
		logger.info( "UTF-8 : [{}] , [{}]", mf.getByteValue(EncodingEnum.UTF8), mf.getByteValueByLength(CharacterEnum.SPACE.getByte(), EncodingEnum.UTF8) );
		logger.info( "EUC-KR : [{}] , [{}]", mf.getByteValue(EncodingEnum.EUCKR), mf.getByteValueByLength(CharacterEnum.SPACE.getByte(), EncodingEnum.EUCKR) );
	}
	
	@Test
	public void getStringValueByLengthTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001617_IN" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		
		MessageField mf = (MessageField)m.findElement( "bundleInfo.L001K001" );
		mf.setStringValue( "홍길동", EncodingEnum.UTF8 );
		logger.info( "UTF-8 : [{}] , [{}]", mf.getStringValue(EncodingEnum.UTF8), mf.getStringValueByLength(EncodingEnum.UTF8, CharacterEnum.SPACE.getByte()) );
		logger.info( "EUC-KR : [{}] , [{}]", mf.getStringValue(EncodingEnum.EUCKR), mf.getStringValueByLength(EncodingEnum.EUCKR, CharacterEnum.SPACE.getByte()) );
	}
	
	@Test
	public void findElementTest() throws CloneNotSupportedException
	{
		Message m = mm.getMessage( "MDS0001617_IN" );
		assertNotNull( m );
		logger.info( "[{}]", m.getElementId() );
		for( MessageElement me: m.getElementList() )
		{
			logger.info( "[{}] : [{}]", me.getElement(), me.getElementId() );
		}
		
		MessageField mf = (MessageField)m.findElement( "bundleInfo.L001K001" );
		assertNotNull( mf );
		logger.info( "[{}] : [{}] : [{}]", mf.getElement(), mf.getElementId(), mf.getStringValue(EncodingEnum.UTF8) );
	}
}
