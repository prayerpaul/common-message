/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : mydata
 * FILE      : PublicMydataDatasetManager.java
 * DATE      : 2022.07.25
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package mydata;

import common.exception.MessageException;
import mydata.pub.PublicMydataDatasetManager;
import org.testng.annotations.Test;

public class PublicMydataDatasetManagerTest
{
	@Test
	public void parseDatasetXMLTest() throws MessageException, CloneNotSupportedException
	{
		PublicMydataDatasetManager pmdm = PublicMydataDatasetManager.getInstance();
		pmdm.parsePublicMydataDatasetXML( "config/dataset/MDS0001617.xml" );
	}
}
