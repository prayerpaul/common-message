/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : mydata
 * FILE      : PublicMydataDatasetTest.java
 * DATE      : 2022.08.05
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package mydata;

import common.enums.EncodingEnum;
import common.enums.FormatEnum;
import common.exception.MessageException;

import common.utility.DateUtil;
import common.utility.StringUtil;
import mydata.pub.PublicMydataDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;

public class PublicMydataDatasetTest extends DatasetTestCase
{
	protected Logger logger = LoggerFactory.getLogger( PublicMydataDatasetTest.class );
	private HashMap<String, String> testDataMap;

	// TODO 각 데이터세트별로 REQUEST, RESPONSE 테스트 케이스 작성 필요. 반드시 테스트 데이터로 테스트 케이스 작성할 것.
	// TODO config/data 디렉토리에 데이터세트별 테스트 데이터 XML 로 저장 필요.
	
	@Test
	public void MDS0001621_IN_Test() throws Exception
	{
		logger.info( "########## MDS0001621_IN_Test START ##########" );
		
		String datasetId = "MDS0001621";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		//String dataId = datasetId + "_FULL_RES";
		//String data = dataRepo.get( dataId );
		
		String plainText = pmd.buildRequestInput();
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printRequestInput(FormatEnum.TEXT, EncodingEnum.MS949) );
		//logger.info( "{}", pmd.printResponseInput(FormatEnum.XML, EncodingEnum.UTF8) );
		
		String encryptedData = pmd.transformRequest( plainText );
		logger.info( "{}", encryptedData );
		
		logger.info( "########## MDS0001621_IN_Test   END ##########" );
	}
	
	@Test
	public void MDS0001621_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001621_RES_Test START ##########" );
		
		String datasetId = "MDS0001621";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001621_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001634_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001634_RES_Test START ##########" );
		
		String datasetId = "MDS0001634";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001634_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001742_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001742_RES_Test START ##########" );
		
		String datasetId = "MDS0001742";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001742_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001742_RES_REV_Test() throws Exception
	{
		logger.info( "########## MDS0001742_RES_REV_Test START ##########" );
		
		String datasetId = "MDS0001742_REV";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_OUT_TXT";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformRequest( data );
		logger.info( "{}", pmd.printRequestInput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.debug( "[{}]", plainText );
		logger.info( "{}", pmd.printRequestOutput(FormatEnum.XML, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001742_RES_REV_Test   END ##########" );
	}
	
	@Test
	public void MDS0001756_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001756_RES_Test START ##########" );
		
		String datasetId = "MDS0001756";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001756_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001760_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001760_RES_Test START ##########" );
		
		String datasetId = "MDS0001760";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001760_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001787_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001787_RES_Test START ##########" );
		
		String datasetId = "MDS0001787";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001787_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001791_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001791_RES_Test START ##########" );
		
		String datasetId = "MDS0001791";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001791_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001802_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001802_RES_Test START ##########" );
		
		String datasetId = "MDS0001802";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001802_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001816_RES_Test() throws Exception
	{
		logger.info( "########## MDS0001816_RES_Test START ##########" );
		
		String datasetId = "MDS0001816";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001816_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001617_RES_Test() throws CloneNotSupportedException, MessageException
	{
		logger.info( "########## MDS0001617_RES_Test START ##########" );
		
		String datasetId = "MDS0001617";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_OUT_XML";
		String data = dataRepo.get( dataId );
		
		pmd.parseResponseInput( data );
		
		logger.info( "{}", pmd.printResponseInput(FormatEnum.XML, EncodingEnum.UTF8) );
		
		logger.info( "########## MDS0001617_RES_Test   END ##########" );
	}
	
	@Test
	public void MDS0001617_mappingRequest_Test() throws CloneNotSupportedException, MessageException
	{
		logger.info( "########## MDS0001617_mappingRequest_Test START ##########" );
		
		String datasetId = "MDS0001617";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_OUT_XML";
		String data = dataRepo.get( dataId );
		
		pmd.mappingRequest();
		
		// 3. Build
		String buildData = pmd.buildRequestOutput();
		logger.info( "3. {}", buildData );
		
		//logger.info( "{}", pmd.printResponseInput(FormatEnum.XML, EncodingEnum.UTF8) );
		
		logger.info( "########## MDS0001617_mappingRequest_Test   END ##########" );
	}
	
	@Test
	public void MDS0001617_mappingResponse_Test() throws CloneNotSupportedException, MessageException
	{
		logger.info( "########## MDS0001617_mappingResponse_Test() START ##########" );
		
		String datasetId = "MDS0001617";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_OUT_XML";
		String data = dataRepo.get( dataId );
		
		pmd.mappingResponse();
		
		//logger.info( "{}", pmd.printResponseInput(FormatEnum.XML, EncodingEnum.UTF8) );
		logger.info( "########## MDS0001617_mappingResponse_Test()   END ##########" );
	}
	
	
	
	@Test
	public void RES_IN_Transform_Logic_Test() throws Exception
	{
		logger.info( "########## RES_IN_Transform_Logic_Test() START ##########" );
		
		String data2 = "11r4PcXqzLAGPVrX3z1BWnTFW6Qlxac39MFdK5PZGMLaxPUo0b+jHSm7yXttDGua2wE7AGhVgkNjmtJv0wrc8JqC/KWxR1Ia5Z6oZvAf+8f/rYwga21zULrqTpKgcfc61ZnLl9d/XahDIjerKz6IF7+myrroGwBSQ0JjZfVBBSr5s6g05d7wqVYlzB6Q8PVczGdiuWmdj8dPoxXXtdcEL1Q4du0flQOyDL5C9CMgiqBzevnHMxzYnu7yGE1GHUBzuVbqfX+bt/vKsgR6zpWwOSBuftk0WeNLJkKvqvAtfShsmKYhAPVxoPKNrOg7LPui9ckIFPEklCAtJIqU+wU5CGhze3BNFhZMZvj/5TPivjVMIXovMauWNxftMKHdRZmhOJR25n78KjxCMfzQ2Jw3cBZJ7zudHVVuXSrggFxnzqyncVC4gsn4m/WUE2ZpIo/lpCffcQDEiUN9Qc74tObJrOe9pCrxCURYjlCyTMo4y8FT2uEgsNMIU9ngReKyG4OxwkJsoy47U85oVyVF5wmKRXXF3+QZkfHhs0tEZ9OmAEB2aPz5TDzJ7HlNtsqLMIYIXQ5P8dKDjs/osIifMfa8vFf525ovSzWTmjLxBn510sJ6PA+Vh8oLzcW47iwOFao7f68hjgqU1RB1P/CAev2UXY32RdVRsgqgD0ldJhuo1LNZHDWtJbYQHXb3lQZa9e4B0gwHqK2jlpgfnKbMDIqv8ZT0HTp/n/O+RiGGA2/O1wf3tlvKFel/6d1K0uQFQ3LXG223kqF4Taio5wSFQXakahY0R64lvm4D9dB45QDnnGP2Oofh+8jXil0MvImUFkFZ7GQo7CIGSzUBznSq8iXzVmLTTaa9+m9rPOjYJlK9Wzmh6VRRaOmtlmOB/aE9gPXGRJ1s9ksXgUq0UvQpuJsQy21s6/fiSMhjreqmedtjRPza5hULnUF3jo5CmNyXabeFefeKkMXPk8BHUvF1YElLNhIi/qHXX4qJ+S4H+GxAX4KZcTHV2PCqgRqbYzbj5zmsSWusWD9gSoqzPACHOBW4bJQ8PJnd1YsnUpPsm1EZBjAO+EJrFAEcGpeG8TkSloa9elaq/oepMDTnBAl4QHHCwRx3dQeEupzhhqWxk49ZxN7e2GfaZIi5YqqtYmkipxjKU9HWuHBrV/+FoxJeAyVO0DamlSdIRxHG9vG01OKZSr0V6QBnSyLEFuU/82FDfelbPpz6cv5yixEp2GQ2ixvBjuVxV3ahCSFpweep+lDwJWQDD3sWI8D51OkMtJe5hHcKlJ6foHna26w2RBocijojELc/AG59JOFFtqL8Z5dPvNhUeWA5nHslLHGeMye3XKiO6i2FosdKCnfIhcZss0CT8xhaf4YWQfuv187r8inZ0EDzNZrNDwJ8A50S+L1C4S11fAo26x5rVVmyiTAYrayPP0Js7+Qjna4tSGBtlcq3FSivXg/HFuMQFuNjDUPePxdoUT27bp3PmlAjwMkdEbnyrH42c1McincaGTS22DZNlHCzH4uFzg/wnDinSusNI+GItB8gaXnC1oKW3gVyBRpWMxIcWTLjbHKbplBujghZEO8ioyrQgTkEaGWvdCK6+ZH3nrYy9EHclMPA+sSeYlXI/3a7UO7HKycvvLk99Lx8r2p98HJLdlF36eCNo5PIGxdVdBIyUidps5y857nYp6PSjluh8eHl+6epvj12OaBMQhckQgKELb+p2uMqes4rIPkWgpP0yqutEQ05UUOwprc4sRsnONEaH3YyCgRlozyJZQgiDVSYTGX8q7nJKLDZQNkOncRZNP6P1A6k3nEVXx9AMlHGflnCaidFAW3droh/SPQ9RfMP4c9kYMVaTuIJl0+FXI8dWLyQhbS4rT5WWooPVFvX+5l+zn5ZeI2qpHaH8K50MdKjU3BWvo8uSCGEkWwGZSLObA+ikbTqIV4iLdYcLZlMz61UhU8JdaQeVSlsixdhfSqBKs3mPsY0c1TRJwug5EYuSM1rzWnr8/l6ZDcKLN7t/oJaRg4qmvCg6hreIh5Rsod2JD1fEG8rM3QhYz2PlUH5ecGlFZQd3njf1nHoaEt6kBdlAB0aPU8dTB1OVM8gkU3gbAPtNB41SleTid3AHrUB3jXo/DIW2xRou7uw9iTfgJd8TAtWr19O/PvjRvulBRPQ/FIkt2HICezw+Sp5hzBJ4R8Ys+GB6rv3dFx7ZyHoKmY7fTVjkvNTI6i/aR+e0eBtX3mthJmX4mXbCHUx2qL7anx4XM6X0IxVJiNT/CQI7e6KQrmSFZ34x4DzUNqin/bojqx0LOFrmox/94Ar/Ufdtr82gk46UswooGgSHhcCd0YGcCgZygecnkbsjTfntlcTJfvKfSadQ+ts6YnwKPkIUUm01bzP9+mlVCZaX+dQkOrCgD/6Ka5ughDc33rlJe4+mENPmrLBa7OslviYOUE8r5UTlisBxj7mORnEcWgWFu62Un0eizZHX6v5AzXY09JNxrjsR89x/00hItuQjSA3U1dreoye3F55JYC2W58UttGMKwPIzZ+W9U4gxtqx81LIAGFgY/30RzJB9mBwnbvywSamxcAh1KDQbdzrAZfPe/9pnTl2+7XIhRw+p/pW3ZUDC9lkLjyRW0i4T5d8rJ3vfCFjmB2yaYNzcFacPnKWPX0ZgDYKaxcPoEYHP8IvBnEjf5hcPyDDX1n9lTioQN3qczJtOeVYE0kNySXpyk14fLacJk6GUH06iD5dCQHSWSxwBHpaVNNakSwZc5wYVmsKdSxyjnbJcQboLRrLx6t7yRKf7BpvXXhOHQPNBEvl3zi/enbn+ojrQi4qj9oR3WGb717K2RJcmE6ebbzPwcutG5cyedBpkwEXh/Ou8vQYKNX67XYgYWwpenheetBG+9BtuHwHtDzbhiOOoETs3iMvjY/9vAf08rxtU7D9KjJLud+h0O6GFTyw2M23Y0bIKgQuXTuRJ5s2tr1Z6T4Xr9+QvpAD/wjfuf2hsLcYedKwuyVjvSGB8MeYtL2EH2NfPzLYQBrQcOzPijFoqGEA/YM44X7JqDagS2QNWkEvMenZjBI3ewUTZv2Y+h8bVV62b2nS0l6XaxX+xHOycMg31yUjh7i8t0uafrFBJ6yOxBThr/Lzc6SSN8G+lJQfxPFLstaN0NDQZRZI4jdxrnnkzeeDoDv1P3a9XTs5PpMpw/MAInTCv8/G20t0DXkTUp/HXEV+4fac1QBqgit8ICOL00tTtW8XlOIcDNpp+icpwVdSQlFc2WMiYL9Ou23rpepMppGwoF9A2UZhhnxw7uJor/euHQJUwdmcDI934EmGtwc5QOiDZzvg1jNEAXHsuB998d1HKp70lSK6mZaAsO+aUvsZ+60HJtHZj2KwtYM2ZOTJa0mKET+AJ2aZa1Q38rMsIioyXcJJ7lVVUWJYJdzdwa/gLKn5mPq57yC9pAe0xdRGZUScmv05seKEWH/+SPlsMCP7f+zV1Jo8zW8toYu+Olcen4mHogrvvj2V42uTnacfNJPOejiVB1CmLKm0DDcwQBo/m7ZlH7i4kpUsMVLzUnMrALSv6o5NdvA/JKUL8R4167vXe1zqOgTKpoJCV7apt5kY6v5iLlLyuQJyn86HIVZOS+x3TCNpdAQbqWXZDURFqTAcjfs6uFoSAgg+JcHf9eW6/40otiff9L83dEwmKhnWyYo2Cfmw7urA7NrbSMK7bVtHcOLb/DSurQiv6nNgekSijnDzX3lJ9k3R2/veIBPZBgYQka2gmq94E2qrENe0YV3Ru+j+2+Aag6kvRaeO83e4G3EaBY85Uni9HkroitDgL5J42Rb56qBQj+ZGOnWCM+1XTgUKjkalO/qTDiytnow9uKWNaysbtnz4Qa5LwvIdm+KY1PwNk44s5ImOQ3KyYHY8GD4nB0JNhod5D2dXBAVoer4lKooAGvA2hY+WtxogUruO3M0CGYWe3rwnTmVx0d3LXhXm7ZdLmBwUQEa4WGWYsL89QuP4k25v6OQmKXqYVZ1BfiiiKlQUkVsHzmE1evfwZ5X1Rd2foZr5E4clmaXwNjtIU9p9uNx2MM0cHpn0ivtsQBjTRa7z7hsOlb/ayi9aun5Dn23ZT0/mWHa2UdWRXaX+ZRWmtN7VC90WPqpL6Bp7zVPEmYtnuUcLHz/NzbQLaCM8Hg+5PLRcDfB/hYp4+5m+fKIUACfb6Bi+GUpLQti+dJQH8bWnwg9bIkGaZaKyPvVk0xYs8yBOgu38JPM4YrFdq0RvCNrpUmuQDhScFyjl+db30J4qQ3cSCUUjecjuImFKzQH1ankoS8ZFEXL+Jst5J3ynw95JFf1nIt4tXzVLtoiNHvGOS4UqbToKKdavMUJ3AYeVtH1hYnnSH+VfSRwoIw9Oe+x1+oBI20PUciAAtAo/Iimkqi3JQ1SGkryEMTgAPY3ECSHyz91o3eax6JyKLdpFQInViWbMkC/sY8bEY9wdRia/haTgu7zFyXui7lURI4lun2jK9zzhxLL2b/rQx8vba7wE4d5I/jSf6vtsrE9IEfMsp72wGFSwYGb/wPkweP+k9bJXj+ED9ltRG3iwiMM3krTjbnsie2P0rob1qqRNvj1gVyEBAozBd+lq2Sz13I2u7HS3p4Lh66MQxVKHYdOkv9gal9+A/B+cmGNw5UNFOVsL2wGZz0vF++FRbb2F10UeLDTyslYzB7jxK+rkQUJV0lheTTFhf4BCj6UzIQUzPofU9R5vIwGP2IHYmI3hAU2aM7P8EeZZgqDmIsq7RA3XmDli3+EHE3viIawyMyKOZZxG4CC/cwj3ZBCe5sGP1EfiSeIdMPqPojifuP7VW1a9ZVS+o0+Xxz3IyUtPfO2k2pICxT/M5iCKMaUipi8X/Yu6+XtPkl0xSDwWNpfvaZ+ljbPQ12fprMk8+a/9ZyoysrkC7e2Y8NaS/KcMQXy/Hyqa0LCdvWM9cpiyM/9oxIUp92TYDTEAneuq09YSg64GaTkE3Mr4M4qOL5ak0VNrSwSLTLRLtyTPk/R/5EnfhsXK9buLFeo/Kzssl4Bk6OnkV3T/1KWNsSTa7lxsHhoAMbGqAeFekSnOfGyci4wWzb6GjierZ7Vkzz0IGvhDQYgdKxbuK0v9a1XSJL8dIGQCpPYUt2lJb1qmBH/ZvmBW0TU14gHABxaAWocXUAtqQYBt8gJ3K1qWkAuIuTxX973R/QBYnuV+w9GTNRXC576q6D51l6DVzbs/34/3Ww/ixlgncJxnWV0IeZp550T2rn5TtxAlguqP/cwlg/MfWx8IehFz2AmHLlgNSUkLBi0qOqvMcgkP73OpgeKQQeRD4WOmz7gcIaV4ViETR5KVtJr3t5o1r9SBtbWIUkFZ2O7Gx0PNYtVPwifufZEBRW9gmWAb5JAb3HniYm+ju6yU7q/7KaUNjonrE8MzUlBXn7vhTot6wdIMuhKUtNQrfMsY8s0yrs4lIyZIUgJtI3K/ZueWABLL9Re/nXWIMeLAnnagQVjQD3KSB1/tbu6q1UYCBN28sr50GV046Y3xOG9VlXY5injJHTYM891ogdJV+K/GV8sG7pZdaoqvn6TyaSjCvsUlkHDV4Jp09Wp1NAFPcFj62jF8SwVmK09s8mHweyp8r1IPoFfMLWig6M7j0K7TP30bEa8q/iFX6zSN0xc1OfFbAtdbiAr+Jhiuecr5jcog/sCfn7fy4dbBbF0H1JfMEMeAoR7Dog5ilWvDb4zBGHDoOh4oATBCNUHp6GjPzlHLVjE8LIRFL3FpGxxn2VxiR2z82RKJ5AIKxxCfAPfgGcEwyBmlLcKoTplkld1N6HAwM0reuCNFfHsOKfNaxbQWRjJ0x6WTRTOiVT/+WQ4L01xciNwIHwM/UN1njd2aUjblbs36KJWJdI+3rFn85LtJ8v+dX2IY7BRy+yz8nzNnENKT+sEUAuuiPPaibgh9J3JhMjjYtxSoZtVVxbAfPNk8+dSyAX/KnStq8qwRXcrpq0b+L6R9uK+7p/q0gQ71YUD5f89obMLcXpa9tYT5vm1hmPk8OBhV2DBk+YFRwrUAlWoRbs1ayWMY5BXlNjDBXWpMN0qufwRm7ZHvmdPDhJPdzv16YOpKaHscT2FjM7K19ImG4CtIuelzqCqK3IlKosFYQGTNn9VuLHiPf9f6FnqI5H+vGAm9hxfZICa3qW0A5ZOqGJYYQIfgF7epmPcWaab1AqPItmNoxLiGIU4mnPq+FmLo+pFRlJgjKZBxiVmdHTIPcbEO/smWRrEs4rXqqDT1PY0BwHFUxC5MkwPAjJmHxJ9S0gpgwUrJjs6ZH4t2QIzmgukXT8eHprWCJ42sQiTYGuu7UhmMbC9/5SjFZ+CzE0Pblz1MZsWLX6qAGEkCbbhBwRS8HXiBWeueS7kwavbHJKn6wTRvzIheM3NRuCwwOHGD9dYm9cigUoKtPy3p550avDViZxcyja6OgXARPBiJ+u4RqusB4EwvZiF87nhdjj3HTHa6/YL6JH0WELZWIADsTRX0FhkMsO7s8NC2ynxpDXijSjBtKxTuG8mOCqG7wLZ70/2IUUfmSdTMcdp/KuofWUAl4FvfREg7T+l+R+CddljCXiIlNYOr3xljQf4OrRcJf/dADDrJ+Rq4lmwKm5auBR1Hys5X1FoUl/tdblXR9oxzHzeaQVVHP/o6FpmhAmC6JkWqJ/mXSjU1mMgXHHHoNM8yeRuL8nCdMd7jJrSv1kiYnnJB8kz/uHl0NB7GVX1DU9oUxMl8G2rhrvn9gw08Mw7L7gbYvpHIeg0H4/3j2tEPTEsYnVVkbnzDk111/jB9Utks9iQvmhX5Q1psCmj0wxLQsecRk6uEYyYeJZvOhBTctENANK8pKDISuQxzzXU1+gqNAlsHjQD0ZKJ2K56XNKupaUo0ulm3xW3ifuyTzmsz9dyGwCIKx9HaR8Qu2odTbef2Hx4UF427kGQQKowxc0OXxXeKNo7ZbpVX2Fev2U3QHaeR8N2LkuNbr98JHrMgPrrilnRu6Y4YtoYxex1J09kzzbKkdkZtRb/+TpRZarRMofGHX6gmV4pk1lnblQwFSLHLH0VDgYm2UbQOAC+gxxpKjm4LKXDUYujc38gpq5KWDkyFdGOlUz7iqxp7fwGtd2muJM2aG8LFzGrWqWxn0tr7gKzO+SXgd8Qi/9McbmDMnhc4kqohYfBOlNrQ9GOqZz+LwCVb0ScsGCg4RMIa+OVe5W1ErRAbIoPf/OeZgWiVeAMASkcmPxEC4eWBuFOD9/dPBlljv7ISyb2nebrsKFVGZGKkilcqGXAV53+2NInSDHirqeH9OqUbzXwy/8tHQ1sN4WS9J6ebkqnpe7m84TDl1TmrEL7WskTVDK4LHXULWvwmJOzymEb0k2xMNQCnJlFZCmQc/vXPiA/4JzrtfN0hxmXXKL4FzBMxAuEb30ezTEe19Pt6SvRX4MS8z6qwvBkuOWxSfr27g/22HxsgITmRPSqUla+pIZ7H4hBH8s6tPcfg7p8cjH3PPF3NJ7FLdG5dcWjPuOdH/qpHzav+O5eSc7O5f6HFTbeOJ1u5W3s+ize0Sf9I+YKLPe7xlxrsa09KlMnNB2ZDqMGq1Nm2skX8+ssqz+KLgQGLZhVnKaPTpRyF0cEJt80a33tUMXrZrEOU2FqID9lsI2OI7EoCpllThu3hM8p6yRc0GQDIIXBG7tlji7zacU2Ep2Ak4bvd2AkFuZ78WUTDfiSPsULQzzq6gdMGhTIrcTfHqcD+jgy1xMTEK8VFsHJA44K1zJLoNQFjGt0s/ClbMiHtvww6lj/H2Ql5sUo0sHEk+Am8tGFJ7ZPtFKR2qG8vKOOKXuc6PjXUSzSghiSQ2kPyETF5hpQWQv+RBAwjc37xC0QcVYCtMkJwSNwilk9ya2pjxxKX+iKtwl6WH6YafpBUj9EUePSzi80dduj6Z1h63iVbqR0v7/HLOuvAL7mZ4gZUI5jGxC1YdbeRXLarilVTYxqqZkoBgfzqZ16zz9xJoZ++Om3UUdT/lXGbK2ZYdwI7+E3ceTKu36gfamD7xDASMi7U+YMjiEFc8JrSQu6Ud1R2BPyS8PsViKehgpsXf+1PIr5+eumyg+JL7WsOnud4E4S2qwqN8Jv8QJ0Xh3jStHPxnySpj/MW8yh1llNzWUA5Dg7cET649IzNVRRJuZ1TFldtp6ZVia1Xh6sY1cmrU27/GImj3c+ePI2P7Tw3r+DzPAN1igtJnH2OGT12cpnoeSO49skLhJzWi1B2rZXpcoLzA4ucj5wR2jS/NmX/bbXDBCP/A1M5bLYx/h9XkumyUbTLI5T5wWeVB7zA7uZohW2Rij6I71vJvbwWyCQHUKeYj90zgGGarTu9d0sSd3vqq2B3G2EOlJBNwMDdWDFQqpQt2lqD8rOcS3MNM5DX1mZCCIyGDi5sjAZZWJQDd9uecN3ysobyuIsd4Sq6Aww5zInIq3HQ7FS4+qeh086qE+JLlE/VbDzSd+HMUfA4nTqzmfyYaqzew9PecK3+cLoEEAlq/LgJ3m85YtxRrq2MCZ8hgqI7gfjJMkIHoUpl7vR2531MU4yuV2+PlruEGFIAn3BEasieUEEN5Jqhhf931bhE3NR/v7W7a+bc4g9CVSi0Sqookmx3tPhkkHdp7/BOD412CyiYBtrjBUnogQNFjX4JrT9gfYqqeqksd+Rk";
		String datasetId = "MDS0001617";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		// 1. Decrypt
		String decryptData = pmd.decryptData( data2 );
		logger.info( "1. {}", decryptData );
		
		// 2. Parse
		pmd.parseResponseInput( decryptData );
		logger.info( "2. {}", pmd.printResponseInput(FormatEnum.TEXT, EncodingEnum.EUCKR) );
		
		// 3. Mapping
		pmd.mappingResponse();
		
		// 4. Build
		String buildData = pmd.buildResponseOutput();
		logger.info( "4. {}", buildData );
		
		pmd.clearResponseDataset();
		
		String cmp = pmd.transformResponse( data2 );
		logger.info( "output1. [{}][{}]", buildData.getBytes(EncodingEnum.MS949.getCharset()).length, buildData );
		logger.info( "output2. [{}][{}]", cmp.getBytes(EncodingEnum.MS949.getCharset()).length, cmp );
		//logger.info( "{}", StringUtil.HexDump(buildData.getBytes(EncodingEnum.MS949.getCharset())) );
		//logger.info( "{}", StringUtil.HexDump(cmp.getBytes(EncodingEnum.MS949.getCharset())) );
		assertEquals( buildData.getBytes(EncodingEnum.MS949.getCharset()).length, cmp.getBytes(EncodingEnum.MS949.getCharset()).length );
		
		logger.info( "########## RES_IN_Transform_Logic_Test()   END ##########" );
	}
	
	@Test
	public void MDS0001791_REQ_RES_Test() throws CloneNotSupportedException, MessageException
	{
		logger.info( "########## MDS0001791_REQ_RES_Test() START ##########" );
		
		String datasetId = "MDS0001791";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_IN_TXT";
		String data = dataRepo.get( dataId );
		
		pmd.parseRequestInput( data );
		
		logger.info( "{}", pmd.buildRequestOutput() );
		
		logger.info( "########## MDS0001791_REQ_RES_Test()   END ##########" );
	}
	
	@Test
	public void MDS0001791_RES_IN_Test() throws CloneNotSupportedException, MessageException
	{
		logger.info( "########## MDS0001791_RES_IN_Test() START ##########" );
		
		String datasetId = "MDS0001791";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		pmd.parseResponseInput( data );
		
		logger.info( "{}", pmd.printResponseInput(FormatEnum.XML, EncodingEnum.UTF8) );
		
		logger.info( "########## MDS0001791_RES_IN_Test()   END ##########" );
	}
	
	@Test
	public void MDS0001791_RES_RES_Test() throws CloneNotSupportedException, MessageException
	{
		logger.info( "########## MDS0001791_RES_RES_Test() START ##########" );
		
		String datasetId = "MDS0001791";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		pmd.parseResponseInput( data );
		
		String output = pmd.buildResponseOutput();
		logger.info( "{}", output );
		logger.info( "{}", StringUtil.HexDump(output.getBytes(EncodingEnum.EUCKR.getCharset())) );
		
		logger.info( "########## MDS0001791_RES_RES_Test()   END ##########" );
	}
	
	@Test
	public void MDS0001787_RES_Test2() throws Exception
	{
		logger.info( "########## MDS0001787_RES_Test START ##########" );
		
		String datasetId = "MDS0001787";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0001787_RES_Test   END ##########" );
		
		/*
		PublicMydataDataset aft = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		*/
	}
	
	
	@Test
	public void MDS0002427_RES_Test() throws Exception
	{
		logger.info( "########## MDS0002427_RES_Test START ##########" );
		
		String datasetId = "MDS0002427";
		
		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );
		
		String dataId = datasetId + "_FULL_OUT_XML";
		String data = dataRepo.get( dataId );
		
		String plainText = pmd.transformResponse( data );
		
		logger.info( "[{}]", plainText );
		logger.info( "{}", pmd.printResponseOutput(FormatEnum.TEXT, EncodingEnum.MS949) );
		
		logger.info( "########## MDS0002427_RES_Test   END ##########" );
	}
	
	@Test(dataProvider = "data-provider")
	public void OUT_REV_Test(Map.Entry<String, String> entry) throws Exception
	{
		logger.info( "########## MDS0001742_RES_REV_Test START ##########" );

		String datasetId = MessageFormat.format("{0}_REV", entry.getKey());

		PublicMydataDataset pmd = pmdm.getPublicMydataDataset( datasetId );
		pmd.setInsttCode( "P114700000000013" );
		pmd.setApiKey( "ab64ec61-a8ed-4f0d-9c8a-61e0e8dabe1a" );
		logger.info( "DATASET ID : [{}], NAME : [{}]", pmd.getDatasetId(), pmd.getDatasetName() );

		String data = entry.getValue();

		String plainText = pmd.transformRequest( data );
		//logger.info( "{}", pmd.printRequestInput(FormatEnum.TEXT, EncodingEnum.MS949) );

		byte[] arr = plainText.getBytes( pmd.getRequestInputEncoding().getCharset() );
		logger.info( "START HexDump {}", DateUtil.getDateTime("yyyy-MM-dd HH:mm:ss.SSS") );
		StringUtil.HexDump( arr, 0, arr.length );
		logger.info( "END HexDump {}", DateUtil.getDateTime("yyyy-MM-dd HH:mm:ss.SSS") );
		logger.info( "START HexDump2 {}", DateUtil.getDateTime("yyyy-MM-dd HH:mm:ss.SSS") );
		StringUtil.HexDump2( arr, 0, arr.length );
		logger.info( "END HexDump2 {}", DateUtil.getDateTime("yyyy-MM-dd HH:mm:ss.SSS") );
		//logger.info( "{}", pmd.printRequestOutput(FormatEnum.XML, EncodingEnum.MS949) );

		logger.info( "########## MDS0001742_RES_REV_Test   END ##########" );
	}

	@BeforeClass
	public void beforeClass() {
		testDataMap = new HashMap<>();
		//testDataMap.put("MDS0001617", "000000688520220818100326110000001P114700000000013MDS0001617                                        9001011000000ub20rhF3iBt5o8h/nZYTd6eZp1SQjIjrLzR5+fnFP4aZMnhEmhTVABv5Fzdrv+jSmRioWK0eibHZZoZ63n8D6Q==00Y홍길동                                                                                              B2210093                                                                                            신용대출                                                                                                                                                                                                                       Y                                                  2022012415300400546920000홍길동                                                                                              F19900101                                                                                                    L              1          020082020220903092658                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      00000                                                                                                                                                                                                         00000                              홍길동                                                                                              8111111111111거주자                                  본인                                                                                                                                                            서울특별시 중구 신창업로 1길                                                                                                                          홍길동                                                                                              거주                                    20100302전입                                    2015110320151103전입                                              00004홍길동                                                                                              본인                                    2010030220100302전입                                              홍영희                                                                                              배우자                                  2015100120151001전입                                              홍일일                                                                                              자녀                                    2018010120180101출생등록                                          홍이이                                                                                              자녀                                    2019010120190101출생등록                                          2022012400001                                  정정내역 없음                                                                                                                         정정내역 없음                                     00006        19800624출생등록                                          충청남도 서천군 망우리                                                                                                                                                                                                                                                                                      1989081919890819                                                  충청남도 서천군 망우리                                                                                                                                                                                                                                                                                      1991120119911201행정구역변경                                      충청남도 서천군 망우리                                                                                                                                                                                                                                                                                      1995010119950101행정구역변경                                      충청남도 서천군 망우리                                                                                                                                                                                                                                                                                      2000041920000419전입                                              충청남도 서천군 구구리                                                                                                                                                                                                                                                                                      2000050120000501전입                                              충청남도 서천군 망우리                                                                                                                                                                                                                                                                                      20171103[L012] 국내거소신고사실증명 301 : 입력 파라미터 오류입니다.[국내거소신고번호(앞번호)이(가) 입력되지 않았습니다.] [L045] 출입국에관한사실증명 Fault occurred while processing.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ");
        //testDataMap.put("MDS0001621", "000000436020220818100326110000001P114700000000013MDS0001621                                        9001011000000ub20rhF3iBt5o8h/nZYTd6eZp1SQjIjrLzR5+fnFP4aZMnhEmhTVABv5Fzdrv+jSmRioWK0eibHZZoZ63n8D6Q==00Y홍길동                                                                                              B2210093                                                                                            신용대출                                                                                                                                                                                                                       Y                                                  2022012415485884756026000홍길동                                                                                              F19900101                                                                                                    L              1          020082020220903093131                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ( 일반과세자 )                신용상사                                                                                            200-00-00000홍철수                                                                                              611111-1111111서울특별시 마포구 111-11                                                                                                                              2000/01/012000/01/1000001부동산업 및 임대업            00001비주거용 건물 임대업(점포, 자기땅)                20221 240000021   계속사업자                                                                                                                                                                                                                                                                                                                                                                                                  0000000000                                      00000                                                                                                                                                                                                                                                                                                                                                                                                                 0000000000                                                00000                         001020180140000     신용상사                                                                                            2000000000                홍철수                                                                                              소기업              도매 및 소매업                                                                                      20180401  20190331  20180828    [L024] 휴업사실증명 099 : 결과데이터가 없습니다.[[ITICMZ0027]휴업이력이 없어, 휴업사실증명 신청이 불가능합니다. 사업자등록번호를 확인하시기 바랍니다.] [L023] 폐업사실증명 099 : 결과데이터가 없습니다.[[ITICMZ0027]폐업사업자가 아닙니다. 폐업사실증명 신청이 불가능합니다. 사업자등록번호를 확인하시기 바랍니다.]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ");
        //testDataMap.put("MDS0001634", "000000292020220818100326110000001P114700000000013MDS0001634                                        9001011000000ub20rhF3iBt5o8h/nZYTd6eZp1SQjIjrLzR5+fnFP4aZMnhEmhTVABv5Fzdrv+jSmRioWK0eibHZZoZ63n8D6Q==00Y홍길동                                                                                              B2210093                                                                                            신용대출                                                                                                                                                                                                                       Y                                                  2022012416443396316792000홍길동                                                                                              F19900101                                                                                                    L              1          020082020220903093130                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              T700-000-0000-000   2022년 1월 24일105  마포세무서          홍길동                                                                                              811111-1111111                                                                                                                00001                                             0                             0                                            2018.01. ~ 2020.12.      정상                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ");
        testDataMap.put("MDS0001787", "000000971920220905214629649423604P114700000000013                                                                                                                                                       002업무담당자명                                                                                        업무담당자ID                                                                                        사용목적                                                                                                                                                                                                                       Y                                                                                                                                                                                                                                                                                            L              1          020082020220905214629                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              00001                                                                                                                                                                                                                                                        00002                                                                                                                                                                                                                                                                              000030020220526홍길동                                                                                                           81000000000신용상사                                                                                            17000000002022           0001201             107550         11520          107550         11520          01             0              0              0              0              02             107550         11520          107550         11520          02             0              0              0              0              03             107550         11520          107550         11520          03             0              0              0              0              04             107550         11520          107550         11520          04             0              0              0              0              05             107550         11520          107550         11520          05             0              0              0              0              06             107550         11520          107550         11520          06             0              0              0              0              07             107550         11520          107550         11520          07             0              0              0              0              08             107550         11520          107550         11520          08             0              0              0              0              09             107550         11520          107550         11520          09             0              0              0              0              10             107550         11520          107550         11520          10             0              0              0              0              11             107550         11520          107550         11520          11             0              0              0              0              12             107550         11520          107550         11520          12             0              0              0              0              69190          9860           2                             220526  홍길동                                                                                              0020220526홍길동                                                                                                           81000000000신용상사                                                                                            17000000002021           0001201             105000         10020          105000         10020          01             0              0              0              0              02             105000         10020          105000         10020          02             0              0              0              0              03             105000         10020          105000         10020          03             0              0              0              0              04             105000         10020          105000         10020          04             0              0              0              0              05             105000         10020          105000         10020          05             0              0              0              0              06             105000         10020          105000         10020          06             0              0              0              0              07             105000         10020          105000         10020          07             0              0              0              0              08             105000         10020          105000         10020          08             0              0              0              0              09             105000         10020          105000         10020          09             0              0              0              0              10             105000         10020          105000         10020          10             0              0              0              0              11             105000         10020          105000         10020          11             0              0              0              0              12             105000         10020          105000         10020          12             0              0              0              0              69100          9800           2                             220526  홍길동                                                                                              0020220526홍길동                                                                                                           81000000000신용상사                                                                                            17000000002020           0001201             100000         10000          100000         10000          01             0              0              0              0              02             100000         10000          100000         10000          02             0              0              0              0              03             100000         10000          100000         10000          03             0              0              0              0              04             100000         10000          100000         10000          04             0              0              0              0              05             100000         10000          100000         10000          05             0              0              0              0              06             100000         10000          100000         10000          06             0              0              0              0              07             100000         10000          100000         10000          07             0              0              0              0              08             100000         10000          100000         10000          08             0              0              0              0              09             100000         10000          100000         10000          09             0              0              0              0              10             100000         10000          100000         10000          10             0              0              0              0              11             100000         10000          100000         10000          11             0              0              0              0              12             100000         10000          100000         10000          12             0              0              0              0              69100          9800           2                             220526  홍길동                                                                                              [L008] 건강·장기요양보험료납부확인서(지역가입자) 099 : 결과데이터가 없습니다.[요청하신 사용자의 건강보험료 납부내역이 존재하지 않습니다.]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ");
	}

	@DataProvider(name = "data-provider", parallel = false)
	public Object[][] dataProviderMethod() {
		return new Object[][] {
			testDataMap.entrySet().toArray()
		};
	}
}
