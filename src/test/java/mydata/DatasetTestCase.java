/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : mydata
 * FILE      : DatasetTestCase.java
 * DATE      : 2022.08.05
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package mydata;

import common.enums.CharacterEnum;
import common.exception.MessageException;
import mydata.pub.PublicMydataDatasetManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public abstract class DatasetTestCase
{
	protected Logger logger = LoggerFactory.getLogger( DatasetTestCase.class );
	public static final String CONFIG_PATH = "config";
	protected PublicMydataDatasetManager pmdm = PublicMydataDatasetManager.getInstance();
	protected Map<String, String> dataRepo = new HashMap<>();
	
	public DatasetTestCase() {}
	
	@BeforeTest
	public void setupBeforeTest() throws MessageException, CloneNotSupportedException, IOException
	{
		loadDataset();
		loadDatasetData();
	}
	
	public void loadDataset() throws MessageException, CloneNotSupportedException
	{
		File dir = new File(CONFIG_PATH + "/dataset/" );
		assertTrue(dir.isDirectory());
		
		File[] list = dir.listFiles();
		if( null == list )
			return;
		
		for( File f: list )
		{
			logger.info( "FILE : [{}]", f.getPath() );
			pmdm.loadPublicMydataDataset( f.getPath() );
		}
		assertEquals( pmdm.getPublicMydataDatasetCount(), list.length );
	}
	
	private void loadDatasetData() throws IOException
	{
		File dir = new File(CONFIG_PATH + "/data" );
		assertTrue(dir.isDirectory());
		
		File[] list = dir.listFiles();
		if( null == list )
			return;
		
		for( File f: list )
		{
			//logger.info( "FILE : [{}]", f.getPath() );
			//logger.info( "DATA : [{}]", readFile(f) );
			//logger.info( "ID   : [{}]", extractMessageID(f) );
			String dataId = extractMessageID( f );
			String data   = readFile( f );
			dataRepo.put( dataId, data );
		}
		assertEquals( dataRepo.size(), list.length );
	}
	private String readFile( File f ) throws IOException
	{
		byte[] bytes = Files.readAllBytes( Paths.get(f.getPath()) );
		return new String( bytes, StandardCharsets.UTF_8 );
	}
	
	private String extractMessageID( File f )
	{
		int endPos = f.getPath().lastIndexOf(CharacterEnum.PERIOD.getChar() );
		int startPos = f.getPath().lastIndexOf(CharacterEnum.SLASH.getChar() );
		
		if( startPos < 0 || endPos < 0 )
			return null;
		
		return f.getPath().substring( startPos+1, endPos );
	}
}
