/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : mydata.pub
 * FILE      : DatasetConstable.java
 * DATE      : 2022.08.08
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package mydata.pub;

import common.enums.CharacterEnum;
import common.message.MessageConstable;

public interface DatasetConstable extends MessageConstable
{
	// XML Tag
	String TAG_DATASET = "Dataset";
	String TAG_MESSAGES = "Messages";
	String TAG_MAPPERS = "Mappers";
	String TAG_REQUEST  = "Request";
	String TAG_RESPONSE = "Response";
	String TAG_INPUT    = "Input";
	String TAG_OUTPUT   = "Output";
	
	// XML Attribute
	String ATTR_PATH = "PATH";
	String ATTR_FORMAT = "Format";
	String ATTR_ENCODING = "Encoding";
	String ATTR_IO = "IO";
	String ATTR_SRC = "SRC";
	String ATTR_TARGET = "TARGET";
	String ATTR_DST = "DST";
	String ATTR_CRYPTO = "Crypto";
	String ATTR_REFER_PATH = ATTR_REFER + CharacterEnum.UNDERSCORE.getString() + ATTR_PATH;
	
	// XML XPath
	String XPATH_DATASET = CharacterEnum.SLASH.getString() + TAG_DATASET;
	String XPATH_MAPPERS = CharacterEnum.SLASH.getString() + TAG_MAPPERS;
	
	//
	String IO_REQ = "REQ";
	String IO_RES = "RES";
	String TARGET_OUT = "OUT";
	String TARGET_HEADER = "HEADER";
	
	// HTTP GET METHOD Parameter KEY
	String HTTP_GET_PARAM_DATASET_KEY           = "hmac";
	// 마이데이터사업자용 HTTP Header Keys
	String HTTP_DAT_REQ_MHEAD_X_API_TRAN_ID     = "x-api-tran-id";
	String HTTP_DAT_REQ_MHEAD_AUTHORIZATION     = "Authorization";
	String HTTP_DAT_REQ_HMEAD_X_PUBLIC          = "x-public";
	String HTTP_DAT_REQ_MHEAD_X_API_TYPE        = "x-api-type";
	String HTTP_DAT_REQ_MHEAD_CERTIFICATES      = "certificates";
	String HTTP_DAT_REQ_MHEAD_CI                = "CI";
	// HTTP Dataset Request Header Keys
	String HTTP_DAT_REQ_HEAD_SERVICE_ID         = "service_id";
	String HTTP_DAT_REQ_HEAD_ID_NO              = "id_no";
	String HTTP_DAT_REQ_HEAD_CI                 = "ci";
	String HTTP_DAT_REQ_HEAD_DATA_USE_PURP_CD   = "data_use_purp_cd";
	String HTTP_DAT_REQ_HEAD_INQ_AGR_YN         = "inq_agr_yn";
	String HTTP_DAT_REQ_HEAD_UNTACT_YN          = "untact_yn";
	String HTTP_DAT_REQ_HEAD_CHG_NM             = "chg_nm";
	String HTTP_DAT_REQ_HEAD_CHG_ID             = "chg_id";
	String HTTP_DAT_REQ_HEAD_USE_PURP           = "use_purp";
	// HTTP Common Request/Response Common Keys
	String HTTP_COMMON_HEAD_APIKEY_HASH_CD      = "apikey_hash_cd";
	String HTTP_COMMON_HEAD_INSTT_CODE          = "instt_code";
	String HTTP_COMMON_HEAD_INSTT_TRNSMIS_NO    = "instt_trnsmis_no";
	String HTTP_COMMON_HEAD_KCREDIT_TRNSMIS_NO  = "kcredit_trnsmis_no";
	// HTTP Support Request Header Keys
	String HTTP_SUP_REQ_HEAD_MASTER_CODE        = "master_code";    // 지원-003
	String HTTP_SUP_REQ_HEAD_SE_HIGH_CODE       = "se_high_code";   // 지원-003
	String HTTP_SUP_REQ_HEAD_CODE_STEP          = "code_step";      // 지원-003
	String HTTP_SUP_REQ_HEAD_HIST_SE_CODE       = "hist_se_code";   // 지원-004
	String HTTP_SUP_REQ_HEAD_START_DE           = "start_de";       // 지원-004, 지원-005
	String HTTP_SUP_REQ_HEAD_END_DE             = "end_de";         // 지원-004, 지원-005
}
