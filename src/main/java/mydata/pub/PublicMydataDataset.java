/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : mydata.pub
 * FILE      : PublicMydataDataset.java
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package mydata.pub;

import common.enums.*;
import common.exception.MessageException;
import common.message.Message;
import common.message.MessageElement;
import common.message.MessageField;
import common.message.control.*;
import common.utility.StringUtil;
import go.mdds.sdk.util.CipherUtil;
import go.mdds.sdk.util.CryptoUtil;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PublicMydataDataset implements Cloneable, Serializable, DatasetConstable
{
	public final Logger logger = LoggerFactory.getLogger(PublicMydataDataset.class);
	protected static final long serialVersionUID = 1L;
	
	protected final String datasetId;
	protected final String datasetName;
	protected String apiKey;
	protected String apiHashCode;
	protected String insttCode;
	protected Map<String, Message> messageSet;
	protected DatasetUnit requestIn;
	protected DatasetUnit requestOut;
	protected DatasetUnit responseIn;
	protected DatasetUnit responseOut;
	protected List<MapperUnit> mapperList;
	protected Map<String, String> httpHeader;
	
	public PublicMydataDataset( String id, String name )
	{
		datasetId = id;
		datasetName = name;
		messageSet = new HashMap<>();
		mapperList = new ArrayList<>();
		httpHeader = new HashMap<>();
	}
	
	// Getter
	public String getDatasetId()
	{
		return datasetId;
	}
	
	public String getDatasetName()
	{
		return datasetName;
	}
	
	public String getApiKey()
	{
		return apiKey;
	}
	
	public String getApiHashCode()
	{
		return apiHashCode;
	}
	
	public String getInsttCode()
	{
		return insttCode;
	}
	
	// FormatEnum
	//public FormatEnum getRequestInputFormat() { return requestIn.getFormat(); }
	//public FormatEnum getRequestOutputFormat() { return requestOut.getFormat(); }
	//public FormatEnum getResponseInputFormat() { return responseIn.getFormat(); }
	//public FormatEnum getResponseOutputFormat() { return responseOut.getFormat(); }
	// EncodingEnum
	public EncodingEnum getRequestInputEncoding()
	{
		return requestIn.getEncoding();
	}
	
	public EncodingEnum getRequestOutputEncoding()
	{
		return requestOut.getEncoding();
	}
	
	public EncodingEnum getResponseInputEncoding()
	{
		return responseIn.getEncoding();
	}
	
	public EncodingEnum getResponseOutputEncoding()
	{
		return responseOut.getEncoding();
	}
	
	// Message
	public List<Map<String, String>> getRequestInputList()
	{
		return requestIn.getMessageList();
	}
	
	public List<Map<String, String>> getRequestOutputList()
	{
		return requestOut.getMessageList();
	}
	
	public List<Map<String, String>> getResponseInputList()
	{
		return responseIn.getMessageList();
	}
	
	public List<Map<String, String>> getResponseOutputList()
	{
		return responseOut.getMessageList();
	}
	
	//
	private DatasetUnit getRequestInputDatasetUnit()
	{
		return requestIn;
	}
	
	private DatasetUnit getRequestOutputDatasetUnit()
	{
		return requestOut;
	}
	
	private DatasetUnit getResponseInputDatasetUnit()
	{
		return responseIn;
	}
	
	private DatasetUnit getResponseOutputDatasetUnit()
	{
		return responseOut;
	}
	
	//
	public List<MapperUnit> getDatasetMappers()
	{
		return mapperList;
	}
	
	// Setter
	//public void addDatatsetMessage( Message m ) { msgset.put( m.getElementId(), m ); }
	public void setDatasetMessages( Map<String, Message> map )
	{
		messageSet = map;
	}
	
	public void setDatasetMappers( List<MapperUnit> mapper )
	{
		mapperList = mapper;
	}
	
	public void setRequestInputDataset( DatasetUnit du )
	{
		requestIn = du;
	}
	
	public void setRequestOutputDataset( DatasetUnit du )
	{
		requestOut = du;
	}
	
	public void setResponseInputDataset( DatasetUnit du )
	{
		responseIn = du;
	}
	
	public void setResponseOutputDataset( DatasetUnit du )
	{
		responseOut = du;
	}
	
	public void setApiKey( String key )
	{
		apiKey = key;
	}
	
	public void setApiHashCode( String code )
	{
		apiHashCode = code;
	}
	
	public void setInsttCode( String code )
	{
		insttCode = code;
	}
	
	public String encryptData( String data ) throws MessageException
	{
		logger.info("*************** encryptData() START ***************");
		String encryptedData;
		try
		{
			encryptedData = CipherUtil.encryptAria(data, getApiKey(), getInsttCode());
		}
		catch( Exception e )
		{
			logger.error(StringUtil.printStackTraceString(e));
			throw new MessageException(e);
		}
		logger.info("*************** encryptData()   END ***************");
		return encryptedData;
	}
	
	/**
	 * 신용정보원 암호화 라이브러리로 XML 데이터를 암호화하여 리턴한다.
	 *
	 * @param data XML 데이터
	 * @return 암호화된 데이터
	 * @throws MessageException
	 */
	public String encodeData( String data ) throws MessageException
	{
		logger.info("*************** encodeData() START ***************");
		String encodedData;
		try
		{
			encodedData = URLEncoder.encode(data, getRequestOutputEncoding().getString());
		}
		catch( UnsupportedEncodingException e )
		{
			logger.error(StringUtil.printStackTraceString(e));
			throw new MessageException(e);
		}
		logger.info("*************** encodeData()   END ***************");
		return encodedData;
	}
	
	/**
	 * 암호화 여부를 체크하여 Crypto 속성에 따라 암/복호화 처리를 한다.
	 *
	 * @param du   데이터세트 UNIT
	 * @param data 암/복호화 처리 대상 데이터
	 * @return 암/복호화 처리된 문자열
	 * @throws Exception
	 */
	public String checkDataCrypto( DatasetUnit du, String data ) throws MessageException
	{
		switch( du.getCrypto() )
		{
		case KCREDIT_ENCRYPT:
			return encryptData(data);
		case KCREDIT_DECRYPT:
			return decryptData(data);
		default:
			return data;
		}
	}
	
	/**
	 * 신용정보원 암호화 라이브러리로 암호화된 데이터를 복호화하여 리턴한다.
	 *
	 * @param data 암호화된 데이터
	 * @return 복호화된 데이터
	 * @throws MessageException
	 */
	public String decryptData( String data ) throws MessageException
	{
		logger.info("*************** decryptData() START ***************");
		String decryptedData;
		try
		{
			decryptedData = CipherUtil.decryptAria(data, getApiKey(), getInsttCode());
		}
		catch( Exception e )
		{
			logger.error(StringUtil.printStackTraceString(e));
			throw new MessageException(e);
		}
		logger.info("*************** decryptData()   END ***************");
		return decryptedData;
	}
	
	/**
	 * 응답 데이터를 Decode 한다.
	 *
	 * @param data Encode 된 데이터
	 * @return Decode 된 데이터
	 * @throws UnsupportedEncodingException
	 */
	public String decodeData( String data ) throws MessageException
	{
		logger.info("*************** decodeData() START ***************");
		String decodedData;
		try
		{
			decodedData = URLDecoder.decode(data, getResponseInputEncoding().getString());
		}
		catch( UnsupportedEncodingException e )
		{
			logger.error(StringUtil.printStackTraceString(e));
			throw new MessageException(e);
		}
		logger.info("*************** decodeData()   END ***************");
		return decodedData;
	}
	
	/**
	 * 공공 마이데이터 데이터세트 API 요청시 HTTP Header apikey_hash_cd Key 에 설정할 값 생성.
	 *
	 * @param data hmac=[URL Encode 된 데이터(암호화됨)]
	 * @return HTTP Header apikey_hash_cd Key 에 설정될 Hash 값
	 * @throws MessageException
	 */
	public String getHmacHash( String data ) throws MessageException
	{
		try
		{
			return CipherUtil.getHmacHashValue(data, getApiKey());
		}
		catch( Exception e )
		{
			throw new MessageException(e);
		}
	}
	
	public void clearRequestDataset() throws MessageException
	{
		MessageInitializer cleaner = new MessageInitializer();
		
		List<Map<String, String>> inList = getRequestInputList();
		for( Map<String, String> map : inList )
		{
			String msgId = map.get(ATTR_ID);
			String referPath = map.get(ATTR_REFER_PATH);
			
			logger.debug("Message ID : [{}], REFER_PATH : [{}]", msgId, referPath);
			
			Message m = messageSet.get(msgId);
			cleaner.visit(m);
		}
		
		List<Map<String, String>> outList = getRequestOutputList();
		for( Map<String, String> map : outList )
		{
			String msgId = map.get(ATTR_ID);
			String referPath = map.get(ATTR_REFER_PATH);
			
			logger.debug("Message ID : [{}], REFER_PATH : [{}]", msgId, referPath);
			
			Message m = messageSet.get(msgId);
			cleaner.visit(m);
		}
	}
	
	/**
	 * Response 관련 Dataset을 초기화 한다.
	 * MessageField는 값을 초기화
	 * MessageGroup은 각 Element 들의 값을 초기화
	 * MessageList는 Data Record 들을 초기화
	 * Message는 각 Element 들의 값을 초기화
	 *
	 * @throws MessageException
	 */
	public void clearResponseDataset() throws MessageException
	{
		MessageInitializer cleaner = new MessageInitializer();
		
		List<Map<String, String>> inList = getResponseInputList();
		for( Map<String, String> map : inList )
		{
			String msgId = map.get(ATTR_ID);
			String referPath = map.get(ATTR_REFER_PATH);
			
			logger.debug("Message ID : [{}], REFER_PATH : [{}]", msgId, referPath);
			
			Message m = messageSet.get(msgId);
			cleaner.visit(m);
		}
		
		List<Map<String, String>> outList = getResponseOutputList();
		for( Map<String, String> map : outList )
		{
			String msgId = map.get(ATTR_ID);
			String referPath = map.get(ATTR_REFER_PATH);
			
			logger.debug("Message ID : [{}], REFER_PATH : [{}]", msgId, referPath);
			
			Message m = messageSet.get(msgId);
			cleaner.visit(m);
		}
	}
	
	/**
	 * 공공 마이데이터 데이터세트 TEXT 를 XML 로 변환( transformRequest 패키징 ).
	 * @param data PlainText 데이터
	 * @return 암호화된 XML 데이터
	 * @throws Exception
	 */
	public String convertTextToXML( String data ) throws Exception
	{
		return transformRequest( data );
	}
	
	/**
	 * 공공 마이데이터 데이터세트 TEXT 를 XML 로 변환( transformResponse 패키징 ).
	 * @param data 암호화된 XML 데이터
	 * @return PlainText 데이터
	 * @throws Exception
	 */
	public String convertXMLToText( String data ) throws Exception
	{
		return transformResponse( data );
	}
	
	/**
	 * convert request input to request output
	 * @param reqData request input data
	 * @return request output data
	 */
	public String transformRequest( String reqData ) throws Exception
	{
		logger.info( "######################################## transformRequest() START ########################################" );
		
		// Input에 대한 암호화 여부를 확인한다.
		String inData = checkDataCrypto( getRequestInputDatasetUnit(), reqData );
		
		// 1. Parse Request Input
		parseRequestInput( inData );
		
		// 2. Mapping
		mappingRequest();
		
		// 3. Build Request Output
		String buildData = buildRequestOutput();
		
		// 4. Encrypt Request Output
		String outData = checkDataCrypto( getRequestOutputDatasetUnit(), buildData );
		
		// 5. URL Encode
		String encodeData = encodeData( outData );
		
		// 6. Generate value of apikey_hash_cd Key
		// hmac=encodeData
		String params = HTTP_GET_PARAM_DATASET_KEY + CharacterEnum.EQUAL.getString() + encodeData;
		
		// 7. set Hash Code
		String hashValue = getHmacHash( params );
		setApiHashCode( hashValue );
		
		logger.info( "######################################## transformRequest()   END ########################################" );
		return params;
	}
	
	/**
	 * convert response input to response output
	 * @param resData response input data
	 * @return response output data
	 */
	public String transformResponse( String resData ) throws Exception
	{
		logger.info( "######################################## transformResponse() START ########################################" );
		
		// 1. Decrypt Response Input
		//    check Crypto
		String inData = checkDataCrypto( getResponseInputDatasetUnit(), resData );
		
		// 2. Parse Response Input
		parseResponseInput( inData );
		
		// 3. Mapping
		mappingResponse();
		
		// 4. Build Response Output
		String buildData = buildResponseOutput();
		
		//
		String outData = checkDataCrypto( getResponseOutputDatasetUnit(), buildData );
		
		logger.info( "######################################## transformResponse()   END ########################################" );
		return outData;
	}
	
	/**
	 * 신용정보원으로 공공 마이데이터 데이터세트 API 요청시 HTTP Header 에 반드시 추가해야할 파라미터를 생성한다.
	 * @return 요청 HTTP Header 정보를 저장한 Map
	 */
	public Map<String, String> getRequestHTTPHeader()
	{
		logger.info( "*************** getRequestHTTPHeader() START ***************" );
		/*
		 * 공공마이데이터 데이터세트 HTTP Header 필수 항목
		 * apikey_hash_cd   : hmac을 SHA-256으로 Hashing 한 값
		 * instt_code       : 이용기관 시스템 코드
		 * service_id       : 데이터세트 ID
		 * // 아래의 항목들은 mappingRequest() 함수에서 처리됨.
		 * instt_trnsmis_no : API 이용기관 거래번호(이 함수에서는 처리하지 않음)
		 * id_no            : 주민번호
		 * ci               :
		 * data_use_purp_cd : '00'
		 * inq_agr_yn       : '1'
		 * untact_yn        : '1'(대면), '2'(비대면)
		 * chg_nm           : URLEncoder.encode() 필수
		 * chg_id           : URLEncoder.encode() 필수
		 * use_purp         : URLEncoder.encode() 필수
		 */
		if( null == httpHeader )
			httpHeader = new HashMap<>();
		
		// apikey_hash_cd
		httpHeader.put( HTTP_COMMON_HEAD_APIKEY_HASH_CD, getApiHashCode() );
		// instt_code
		httpHeader.put( HTTP_COMMON_HEAD_INSTT_CODE    , getInsttCode() );
		// service_id
		httpHeader.put( HTTP_DAT_REQ_HEAD_SERVICE_ID   , getDatasetId() );
		
		// TODO [공공-금융 API 연계 설계서 참조(4.2.2 데이터세트 API)]
		// 대면서비스의 경우 CI 확인이 어려울 때 주민번호 13자리를 SHA256으로 해쉬 하여 CI 대체 값으로 사용할 수 있음. (비대면 서비스에서는 원본 CI 사용 필수)
		// ex) 주민번호가 850101-1111112 인 경우
		// 주민번호 뒤 7 자리를 키로 주민번호를 SHA256 해쉬함. String fCI = CryptoUtil.generateSHA256 (“1111112”, “8501011111112”) ;
		String untact_yn = httpHeader.get( HTTP_DAT_REQ_HEAD_UNTACT_YN );
		String ci = httpHeader.get( HTTP_DAT_REQ_HEAD_CI );
		String id_no = httpHeader.get( HTTP_DAT_REQ_HEAD_ID_NO );
		if( untact_yn.equals("1") )
		{
			if( StringUtil.isBlank(ci.trim()) && !StringUtil.isBlank(id_no.trim()) )
			{
				String replaceCI = CryptoUtil.generateSHA256(id_no.substring(6), id_no);
				httpHeader.put(HTTP_DAT_REQ_HEAD_CI, replaceCI);
			}
		}
		// Logging : Data 출력
		httpHeader.entrySet().stream().forEach(entry-> {
			logger.info( "getRequestHTTPHeader() -> [key] : [" + entry.getKey() + "], [value] : [" + entry.getValue() +"]" );
		});
		
		logger.info( "*************** getRequestHTTPHeader()   END ***************" );
		return httpHeader;
	}
	
	private void cachingMessageElement( Map<String, MessageElement> map, String srcPath, MessageElement me )
	{
		if( map.containsKey(srcPath) )
			return;
		
		map.put( srcPath, me );
	}
	
	private MessageElement findElementInCache( Map<String, MessageElement> map, String srcPath, ElementEnum ee )
	{
		MessageElement me = map.get( srcPath );
		return me;
	}
	
	/**
	 * 요청에 관한 데이터 Mapping 을 처리한다.
	 */
	public void mappingRequest()
	{
		logger.info( "*************** mappingRequest() START ***************" );
		
		Map<String, MessageElement> cacheMap = new HashMap<>();
		DatasetUnit reqInputUnit  = getRequestInputDatasetUnit();
		DatasetUnit reqOutputUnit = getRequestOutputDatasetUnit();
		
		for( MapperUnit mu: mapperList )
		{
			if( IO_REQ.equalsIgnoreCase(mu.getInOut()) )
			{
				logger.info( "mappingRequest -> IO [{}], [{}], SRC [{}], TARGET [{}], DST [{}], MACRO [{}]", mu.getInOut(), mu.getElement(), mu.getSrcPath(), mu.getTarget(), mu.getDstPath(), mu.getMacro() );
				// 먼저 Cache 에 있는지 확인힌다.
				MessageElement in = findElementInCache( cacheMap, mu.getSrcPath(), mu.getElement() );
				// 없으면 Dataset 에서 찾는다.
				if( null == in )
				{
					logger.debug( "mappingRequest -> [{}] [{}] not in cache", mu.getElement(), mu.getSrcPath() );
					in = findDatasetElement( reqInputUnit, mu.getSrcPath() );
					if( null == in )
					{
						logger.debug( "mappingRequest -> [{}] [{}] not in dataset", mu.getElement(), mu.getSrcPath() );
						continue;
					}
				}
				else
					logger.debug( "mappingRequest -> [{}] [{}] in cache", mu.getElement(), mu.getSrcPath() );
				
				cachingMessageElement( cacheMap, mu.getSrcPath(), in );
				
				if( TARGET_OUT.equalsIgnoreCase(mu.getTarget()) )
				{
					MessageElement out = findDatasetElement( reqOutputUnit, mu.getDstPath() );
					if( null == out )
						continue;
					logger.debug( "mappingRequest -> findDatasetElement : IN [{}][{}], OUT [{}][{}]", in.getElement(), in.getElementId(), out.getElement(), out.getElementId() );
					
					switch( mu.getElement() )
					{
					case FIELD:
						logger.debug( "mappingRequest -> mapping to OUT" );
						mappingField( reqOutputUnit, (MessageField) in, (MessageField) out );
						break;
					case GROUP:
						break;
					case LIST:
						break;
					case MESSAGE:
						break;
					default:
					}
				}
				else if( TARGET_HEADER.equalsIgnoreCase(mu.getTarget()) )
				{
					logger.debug( "mappingRequest -> mapping to HEADER" );
					String value = ((MessageField)in).getStringValue( reqOutputUnit.getEncoding() );
					if( MessageMacroEnum.MACRO_URL_ENCODE == mu.getMacro() )
					{
						try
						{
							httpHeader.put( mu.getDstPath(), URLEncoder.encode(value, reqOutputUnit.getEncoding().getString()) );
						}
						catch( Exception e )
						{
							logger.error( StringUtil.printStackTraceString(e) );
						}
					}
					else
						httpHeader.put( mu.getDstPath(), value );
				}
			}
		}
		
		// Envelope.Header.commonHeader.apiKeyHashCode 값 설정
		// TODO 현재는 API 키를 해쉬 없이 넣어도 되며 행안부의 명확한 가이드가 배포되면 별도로 공지 예정[공공-금융 API 연계 설계서 참조(5.1 요청(Request) XML)]
		MessageField mf = (MessageField) findDatasetElement( reqOutputUnit, "Envelope.Header.commonHeader.apiKeyHashCode" );
		mf.setStringValue( getApiKey(), reqOutputUnit.getEncoding() );
		
		logger.info( "*************** mappingRequest()   END ***************" );
	}
	
	/**
	 * 매핑 소스의 MessageField 의 값을 대상 MessageField 에 복사한다.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @param src 매핑 소스 MessageField
	 * @param dst 매핑 대상 MessageField
	 */
	private void mappingField( DatasetUnit du, MessageField src, MessageField dst )
	{
		logger.debug( "mappingField call" );
		dst.setStringValue( src.getStringValue(du.getEncoding()), du.getEncoding() );
	}
	
	/**
	 * 응답에 관한 데이터 Mapping 을 처리한다.
	 */
	public void mappingResponse()
	{
		logger.info( "*************** mappingResponse() START ***************" );
		
		Map<String, MessageElement> cacheMap = new HashMap<>();
		DatasetUnit resInputUnit  = getResponseInputDatasetUnit();
		DatasetUnit resOutputUnit = getResponseOutputDatasetUnit();
		
		for( MapperUnit mu: mapperList )
		{
			if( IO_RES.equalsIgnoreCase(mu.getInOut()) )
			{
				logger.info( "mappingResponse -> [{}] [{}] [{}] [{}]", mu.getInOut(), mu.getElement(), mu.getSrcPath(), mu.getDstPath() );
				// 먼저 Cache 에 있는지 확인힌다.
				MessageElement in = findElementInCache( cacheMap, mu.getSrcPath(), mu.getElement() );
				// 없으면 Dataset 에서 찾는다.
				if( null == in )
				{
					logger.debug( "mappingResponse -> [{}] [{}] not in cache", mu.getElement(), mu.getSrcPath() );
					in = findDatasetElement( resInputUnit, mu.getSrcPath() );
					if( null == in )
					{
						logger.debug( "mappingResponse -> [{}] [{}] not in dataset", mu.getElement(), mu.getSrcPath() );
						continue;
					}
				}
				else
					logger.debug( "mappingResponse -> [{}] [{}] in cache", mu.getElement(), mu.getSrcPath() );
				
				cachingMessageElement( cacheMap, mu.getSrcPath(), in );
				MessageElement me = findDatasetElement( resInputUnit, mu.getSrcPath() );
				logger.debug( "mappingResponse -> findDatasetElement : [{}]", me );
				
				if( TARGET_OUT.equalsIgnoreCase(mu.getTarget()) )
				{
					MessageElement out = findDatasetElement( resOutputUnit, mu.getDstPath() );
					if( null == out )
						continue;
					logger.debug( "mappingResponse -> findDatasetElement : IN [{}][{}], OUT [{}][{}]", in.getElement(), in.getElementId(), out.getElement(), out.getElementId() );
					
					switch( mu.getElement() )
					{
					case FIELD:
						logger.debug( "mappingResponse -> mapping to OUT" );
						mappingField( resOutputUnit, (MessageField) in, (MessageField) out );
						break;
					case GROUP:
						break;
					case LIST:
						break;
					case MESSAGE:
						break;
					default:
					}
				}
				else if( TARGET_HEADER.equalsIgnoreCase(mu.getTarget()) )
				{
					logger.debug("mappingRequest -> mapping to HEADER");
					String value = ((MessageField) in).getStringValue( resOutputUnit.getEncoding() );
					if( MessageMacroEnum.MACRO_URL_ENCODE == mu.getMacro() )
					{
						try
						{
							httpHeader.put( mu.getDstPath(), URLEncoder.encode(value, resOutputUnit.getEncoding().getString()) );
						}
						catch( Exception e )
						{
							logger.error( StringUtil.printStackTraceString(e) );
						}
					}
					else
						httpHeader.put( mu.getDstPath(), value );
				}
			}
		}
		
		logger.info( "*************** mappingResponse()   END ***************" );
	}
	
	/**
	 * DatasetUnit 내에서 해당 경로의 MessageElement 가 존재하는지 검색한다.
	 * TODO 현재는 공통부만 검색이 필요해서 해당 소스로 처리가 되지만, 개별부 처리가 필요하면 관련하여 검색할 수 있도록 처리가 필요함. REFER_PATH 와 path 의 공통 경로를 삭제하고 검색을 해야할 것으로 생각됨.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @param path 검색하고자 하는 MessageElement 의 경로
	 * @return 존재하면 해당 MessageElement 리턴, 존재하지 않으면 null 리턴
	 */
	public MessageElement findDatasetElement( DatasetUnit du, String path )
	{
		for( Map<String, String> map: du.getMessageList() )
		{
			String msgId = map.get( ATTR_ID );
			String referPath = map.get( ATTR_REFER_PATH );
			
			Message m = messageSet.get( msgId );
			MessageElement me = m.findElement( path );
			if( null != me )
				return me;
		}
		
		return null;
	}
	
	// parse DATA
	public void parseRequestInput( String data ) throws MessageException
	{
		parseDataset( getRequestInputDatasetUnit(), data );
	}
	public void parseRequestOutput( String data ) throws MessageException
	{
		parseDataset( getRequestOutputDatasetUnit(), data );
	}
	public void parseResponseInput( String data ) throws MessageException
	{
		parseDataset( getResponseInputDatasetUnit(), data );
	}
	public void parseResponseOutput( String data ) throws MessageException
	{
		parseDataset( getResponseOutputDatasetUnit(), data );
	}
	
	// print DATA
	public String printRequestInput( FormatEnum fe, EncodingEnum ee ) throws MessageException
	{
		return printDataset( getRequestInputDatasetUnit(), fe, ee );
	}
	public String printRequestOutput( FormatEnum fe, EncodingEnum ee ) throws MessageException
	{
		return printDataset( getRequestOutputDatasetUnit(), fe, ee );
	}
	public String printResponseInput( FormatEnum fe, EncodingEnum ee ) throws MessageException
	{
		return printDataset( getResponseInputDatasetUnit(), fe, ee );
	}
	public String printResponseOutput( FormatEnum fe, EncodingEnum ee ) throws MessageException
	{
		return printDataset( getResponseOutputDatasetUnit(), fe, ee );
	}
	
	// build DATA
	public String buildRequestInput() throws MessageException
	{
		return buildDataset( getRequestInputDatasetUnit() );
	}
	public String buildRequestOutput() throws MessageException
	{
		return buildDataset( getRequestOutputDatasetUnit() );
	}
	public String buildResponseInput() throws MessageException
	{
		return buildDataset( getResponseInputDatasetUnit() );
	}
	public String buildResponseOutput() throws MessageException
	{
		return buildDataset( getResponseOutputDatasetUnit() );
	}
	
	/**
	 * 데이터를 메시지에 파싱하여 저장한다.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @param data 파싱할 데이터
	 * @throws MessageException
	 */
	private void parseDataset( @NotNull DatasetUnit du, String data ) throws MessageException
	{
		logger.info( "*************** parseDataset() START ***************" );
		
		MessageParser parser;
		FormatEnum fe = du.getFormat();
		switch( fe )
		{
		case XML:
			parser = new MessageXMLParser( data, du.getEncoding() );
			break;
		case TEXT:
			parser = new MessageTextParser( data, du.getEncoding() );
			break;
		case JSON:
			parser = new MessageJSONParser( data, du.getEncoding() );
			break;
		default:
			throw new MessageException( "Unsupported Format Data : [" + fe + "]" );
		}
		
		parseDataByFormat( du, parser );
		
		logger.info( "*************** parseDataset() START ***************" );
	}
	
	/**
	 * 데이터를 메시지 항목에 맞게 파싱하여 저장한다.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @param parser 파싱하고자하는 포맷의 parser
	 * @throws MessageException
	 */
	private void parseDataByFormat( @NotNull DatasetUnit du, MessageParser parser ) throws MessageException
	{
		logger.info( "*************** parseDataByFormat() START ***************" );
		
		logger.info( "FORMAT [{}], ENCODING [{}], CRYPTO [{}]", du.getFormat(), du.getEncoding(), du.getCrypto() );
		List<Map<String, String>> list = du.getMessageList();
		
		for( Map<String, String> map: list )
		{
			String msgId = map.get( ATTR_ID );
			String referPath = map.get( ATTR_REFER_PATH );
			
			logger.info( "Message ID : [{}], REFER_PATH : [{}]", msgId, referPath );
			if( !StringUtil.isBlank(referPath) )
				parser.setElementPath( referPath );
			
			Message m = messageSet.get( msgId );
			m.accept( parser );
		}
		
		logger.info( "*************** parseDataByFormat()   END ***************" );
	}
	
	/**
	 * 메시지 데이터를 원하는 포맷(XML/JSON/TEXT)으로 출력한다.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @param fe FormatEnum(XML/JSON/TEXT)
	 * @param ee EncodingEnum(ASCII/UTF-8/EUC-KR/MS949)
	 * @return 원하는 포맷으로 생성된 출력용 메시지 데이터.
	 * @throws MessageException
	 */
	private String printDataset( @NotNull DatasetUnit du, FormatEnum fe, EncodingEnum ee ) throws MessageException
	{
		switch( fe )
		{
		case XML:
		case JSON:
			return printDataByFormat( du, fe, ee );
		case TEXT:
			MessageTextPrinter printer = new MessageTextPrinter( ee );
			return printTextFormat( du, printer );
		default:
			throw new MessageException( "Unsupported Format Data : [" + fe + "]" );
		}
	}
	
	/**
	 * 원하는 포맷(XML/JSON)으로 메시지 데이터를 출력하는 공통함수.
	 * XML 과 JSON 포맷은 Builder를 이용하여 메시지 데이터를 출력한다.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @param fe FormatEnum(XML/JSON)
	 * @param ee EncodingEnum(ASCII/UTF-8/EUC-KR/MS949)
	 * @return 원하는 포맷으로 build() 된 데이터 문자열
	 * @throws MessageException
	 */
	public String printDataByFormat( @NotNull DatasetUnit du, FormatEnum fe, EncodingEnum ee ) throws MessageException
	{
		MessageBuilder builder;
		if( FormatEnum.XML == fe )
			builder = new MessageXMLBuilder( ee );
		else
			builder = new MessageJSONBuilder( ee );
			
		buildDataByFormat( du, builder );
		
		return builder.build();
	}
	
	/**
	 * 일반적으로 PlainText Format 에서 메시지 항목들을 확인하기 위하여 출력하는 형태로 출력한다.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @param printer 일반적으로 EUC-KR/MS949/UTF-8 Encoding 을 파라미터로 받아서 해당 Encoding 에 맞게 항목들을 출력함.
	 * @return 메시지 항목들을 Text 형식으로 출력한 문자열
	 * @throws MessageException
	 */
	public String printTextFormat( @NotNull DatasetUnit du, MessageTextPrinter printer ) throws MessageException
	{
		List<Map<String, String>> list = du.getMessageList();
		
		for( Map<String, String> map: list )
		{
			String msgId = map.get( ATTR_ID );
			String referPath = map.get( ATTR_REFER_PATH );
			
			logger.info( "printTextDataFormat() -> Message ID : [{}], PATH : [{}]", msgId, referPath );
			
			Message m = messageSet.get( msgId );
			m.accept( printer );
		}
		
		return printer.print();
	}
	
	/**
	 * Public Mydata Dataset 데이터를 원하는 포맷(XML/JSON/TEXT)으로 build() 한다.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @return 원하는 포맷(XML/JSON/TEXT)으로 생성된 데이터
	 * @throws MessageException
	 */
	private String buildDataset( @NotNull DatasetUnit du ) throws MessageException
	{
		MessageBuilder builder;
		FormatEnum fe = du.getFormat();
		switch( fe )
		{
		case XML:
			builder = new MessageXMLBuilder( du.getEncoding() );
			buildDataByFormat( du, builder );
			break;
		case TEXT:
			builder = new MessageTextBuilder( du.getEncoding() );
			buildDataByFormat( du, builder );
			break;
		case JSON:
			builder = new MessageJSONBuilder( du.getEncoding() );
			buildDataByFormat( du, builder );
			break;
		default:
			throw new MessageException( "Unsupported Format Data : [" + fe + "]" );
		}
		
		return builder.build();
	}
	
	/**
	 * Public Mydata Dataset 데이터를 원하는 MessageBuilder 를 통하여 데이터를 생성하는 공통함수.
	 * @param du Dataset Unit( Request IN/OUT, Response IN/OUT )
	 * @param builder 원하는 포맷으로 데이터를 build()하는 Abstract Class
	 * @throws MessageException
	 */
	private void buildDataByFormat( @NotNull DatasetUnit du, MessageBuilder builder ) throws MessageException
	{
		logger.info( "*************** buildDataByFormat() START ***************" );
		
		logger.info( "FORMAT [{}], ENCODING [{}], CRYPTO [{}]", du.getFormat(), du.getEncoding(), du.getCrypto() );
		
		List<Map<String, String>> list = du.getMessageList();
		
		for( Map<String, String> map: list )
		{
			String msgId = map.get( ATTR_ID );
			String referPath = map.get( ATTR_REFER_PATH );
			
			logger.info( "Message ID : [{}], REFER_PATH : [{}]", msgId, referPath );
			if( !StringUtil.isBlank(referPath) )
				builder.setElementPath( referPath );
			
			Message m = messageSet.get( msgId );
			m.accept( builder );
		}
		
		logger.info( "*************** buildDataByFormat()   END ***************" );
	}
	
	/**
	 * PublicMydataDataset 객체를 복제한다.
	 * @return 새로 생성된 PublicMydataDataset 객체
	 * @throws CloneNotSupportedException
	 */
	public PublicMydataDataset clone() throws CloneNotSupportedException
	{
		try
		{
			// Serializing
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream out  = new ObjectOutputStream( baos );
			out.writeObject( this );
			out.flush();
			
			// Deserializing
			ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() );
			ObjectInputStream in   = new ObjectInputStream( bais );
			return (PublicMydataDataset) in.readObject();
		}
		catch( IOException ie )
		{
			throw new CloneNotSupportedException( "Serialization/Deserialization failed. : " + ie );
		}
		catch( ClassNotFoundException ce )
		{
			throw new CloneNotSupportedException("Deserialization failed. : " + ce);
		}
	}
}
