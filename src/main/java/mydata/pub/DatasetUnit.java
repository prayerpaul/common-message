/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : mydata.pub
 * FILE      : DatasetUnit.java
 * DATE      : 2022.08.03
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package mydata.pub;

import common.enums.CryptographyEnum;
import common.enums.EncodingEnum;
import common.enums.FormatEnum;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DatasetUnit implements Serializable
{
	protected static final long serialVersionUID = 1L;
	protected FormatEnum   fe;
	protected EncodingEnum ee;
	protected CryptographyEnum ce;
	protected List<Map<String, String>> msgList;
	
	DatasetUnit( List<Map<String, String>> list, FormatEnum fe, EncodingEnum ee, CryptographyEnum ce )
	{
		this.msgList  = list;
		this.fe = fe;
		this.ee = ee;
		this.ce = ce;
	}
	
	public List<Map<String, String>> getMessageList() { return this.msgList; }
	public FormatEnum  getFormat() { return this.fe; }
	public EncodingEnum  getEncoding() { return this.ee; }
	public CryptographyEnum getCrypto() { return this.ce; }
}
