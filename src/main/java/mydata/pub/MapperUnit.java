/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : mydata.pub
 * FILE      : MapperUnit.java
 * DATE      : 2022.08.08
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package mydata.pub;

import common.enums.ElementEnum;
import common.enums.MessageMacroEnum;

import java.io.Serializable;

public class MapperUnit implements Serializable
{
	protected static final long serialVersionUID = 1L;

	private final ElementEnum ee;
	private final String      ioType;
	private final String      targetType;
	private final String      srcPath;
	private final String      dstPath;
	private final MessageMacroEnum mme;
	
	public MapperUnit( String io, ElementEnum ee, String target, String src, String dst )
	{
		this( io, ee, target, src, dst, MessageMacroEnum.NULL );
	}
	public MapperUnit( String io, ElementEnum ee, String target, String src, String dst, MessageMacroEnum mme )
	{
		this.ioType = io;
		this.ee = ee;
		this.targetType = target;
		this.srcPath = src;
		this.dstPath = dst;
		this.mme = mme;
	}
	
	// Getter
	public String getInOut() { return ioType; }
	public ElementEnum getElement() { return ee; }
	public String getTarget() { return targetType; }
	public String getSrcPath() { return srcPath; }
	public String getDstPath() { return dstPath; }
	public MessageMacroEnum getMacro() { return mme; }
	
	// Setter
}
