/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : mydata.pub
 * FILE      : PublicMydataDatasetManager.java
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package mydata.pub;

import common.enums.*;
import common.exception.MessageException;
import common.message.Message;
import common.message.MessageManager;
import common.utility.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PublicMydataDatasetManager implements DatasetConstable
{
	protected Logger logger = LoggerFactory.getLogger( PublicMydataDatasetManager.class );
	
	protected MessageManager msgManager;
	protected Map<String, PublicMydataDataset> datasetRepo;
	
	protected PublicMydataDatasetManager()
	{
		super();
		datasetRepo = new ConcurrentHashMap<>();
		msgManager  = MessageManager.getInstance();
	}
	private static class SingletonHelper
	{
		private static final PublicMydataDatasetManager INSTANCE = new PublicMydataDatasetManager();
	}
	
	public static PublicMydataDatasetManager getInstance()
	{
		return PublicMydataDatasetManager.SingletonHelper.INSTANCE;
	}
	
	public PublicMydataDataset getPublicMydataDataset( String id ) throws CloneNotSupportedException
	{
		PublicMydataDataset pmd = datasetRepo.get( id );
		return pmd.clone();
	}
	
	public int getPublicMydataDatasetCount() { return datasetRepo.size(); }
	
	public void loadPublicMydataDataset( String path ) throws MessageException, CloneNotSupportedException
	{
		parsePublicMydataDatasetXML( path );
	}
	
	public void parsePublicMydataDatasetXML( String filePath ) throws MessageException, CloneNotSupportedException
	{
		try
		{
			InputSource is = new InputSource( Files.newBufferedReader(Paths.get(filePath), EncodingEnum.UTF8.getCharset()) );		// non-blocking I/O and auto-closable
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			
			// XXE Attack Mitigation
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			
			Document document = dbf.newDocumentBuilder().parse(is);
			
			XPath xPath = XPathFactory.newInstance().newXPath();
			
			Node datasetNode = (Node)xPath.evaluate(XPATH_DATASET, document, XPathConstants.NODE );
			String datasetId = datasetNode.getAttributes().getNamedItem(ATTR_ID).getTextContent();
			String datasetNm = datasetNode.getAttributes().getNamedItem(ATTR_NAME).getTextContent();
			logger.debug( "parseDatasetXML() -> DATASET ID : [{}], NAME :  [{}]", datasetId, datasetNm );
			
			PublicMydataDataset pmd = new PublicMydataDataset( datasetId, datasetNm );
			
			// /Dataset/Messages/*
			String msgPath = XPATH_DATASET + CharacterEnum.SLASH.getString() + TAG_MESSAGES + CharacterEnum.SLASH.getString() + CharacterEnum.ASTERISK.getString();
			logger.debug( "parseDatasetXML() -> Messages XPATH : [{}]", msgPath );
			NodeList nodes = (NodeList)xPath.evaluate( msgPath, document, XPathConstants.NODESET );
			pmd.setDatasetMessages( parseMessageNodeList(nodes) );
			
			// /Dataset/Mappers
			String mappersPath = XPATH_DATASET + CharacterEnum.SLASH.getString() + TAG_MAPPERS;
			Node mappersNode = (Node)xPath.evaluate( mappersPath, document, XPathConstants.NODE );
			List<MapperUnit> list = new ArrayList<>();
			try
			{
				String mappersFile = mappersNode.getAttributes().getNamedItem(ATTR_PATH).getTextContent();
				logger.debug( "MAPPERS PATH : [{}]", mappersFile );
				parseMapperFile( list, mappersFile );
			}
			catch( Exception e ) { logger.error( StringUtil.printStackTraceString(e) ); }
			
			// /Dataset/Mappers/*
			String mapperPath = XPATH_DATASET + CharacterEnum.SLASH.getString() + TAG_MAPPERS + CharacterEnum.SLASH.getString() + CharacterEnum.ASTERISK.getString();
			logger.debug( "parseDatasetXML() -> Mappers XPATH : [{}]", mapperPath );
			nodes = (NodeList)xPath.evaluate( mapperPath, document, XPathConstants.NODESET );
			parseMapperNodeList( list, nodes );
			pmd.setDatasetMappers( list );
			
			// /Dataset/Request/*
			String reqPath = XPATH_DATASET + CharacterEnum.SLASH.getString() + TAG_REQUEST + CharacterEnum.SLASH.getString() + CharacterEnum.ASTERISK.getString();
			logger.debug( "parseDatasetXML() -> Request XPATH : [{}]", reqPath );
			nodes = (NodeList)xPath.evaluate( reqPath, document, XPathConstants.NODESET );
			parseDatasetNodeList( TAG_REQUEST, pmd, nodes );
			
			// /Dataset/Response/*
			String resPath = XPATH_DATASET + CharacterEnum.SLASH.getString() + TAG_RESPONSE + CharacterEnum.SLASH.getString() + CharacterEnum.ASTERISK.getString();
			logger.debug( "parseDatasetXML() -> Response XPATH : [{}]", resPath );
			nodes = (NodeList)xPath.evaluate( resPath, document, XPathConstants.NODESET );
			parseDatasetNodeList( TAG_RESPONSE, pmd, nodes );
			
			datasetRepo.put( pmd.getDatasetId(), pmd );
		}
		catch( XPathExpressionException | SAXException | IOException | ParserConfigurationException e )
		{
			throw new MessageException( e );
		}
	}
	
	public void  parseMapperFile( List<MapperUnit> list, String path ) throws MessageException
	{
		try
		{
			InputSource is = new InputSource( Files.newBufferedReader(Paths.get(path), EncodingEnum.UTF8.getCharset()) );		// non-blocking I/O and auto-closable
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			
			// XXE Attack Mitigation
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			
			Document document = dbf.newDocumentBuilder().parse(is);
			
			XPath xPath = XPathFactory.newInstance().newXPath();
			
			String mapperPath = XPATH_MAPPERS + CharacterEnum.SLASH.getString() + CharacterEnum.ASTERISK.getString();
			NodeList nodeList = (NodeList)xPath.evaluate( mapperPath, document, XPathConstants.NODESET );
			
			for( int i=0; null != nodeList && i < nodeList.getLength(); ++i )
			{
				Node node = nodeList.item(i);     // no need to cast
				if( node.getNodeType() != Node.ELEMENT_NODE ) continue;
				
				String nodeTag = node.getNodeName();
				String io = node.getAttributes().getNamedItem(ATTR_IO).getTextContent();
				String type = node.getAttributes().getNamedItem(ATTR_TYPE).getTextContent();
				String src = node.getAttributes().getNamedItem(ATTR_SRC).getTextContent();
				String dst = node.getAttributes().getNamedItem(ATTR_DST).getTextContent();
				String target;
				try { target = node.getAttributes().getNamedItem(ATTR_TARGET).getTextContent(); } catch( Exception e ) { target = ""; }
				String macro;
				try { macro = node.getAttributes().getNamedItem(ATTR_MACRO).getTextContent();   } catch( Exception e ) { macro = ""; }
				logger.debug( "parseMapperFile() -> [{}] [{}] : [{}], [{}], [{}], [{}], [{}]", nodeTag, io, type, src, dst, target, macro );
				
				ElementEnum ee = ElementEnum.getEnum( type );
				MessageMacroEnum mme = MessageMacroEnum.getEnum( macro );
				
				MapperUnit mu = new MapperUnit( io, ee, target, src, dst, mme );
				list.add( mu );
			}
		}
		catch( XPathExpressionException | SAXException | IOException | ParserConfigurationException e )
		{
			throw new MessageException( e );
		}
	}
	
	protected Map<String, Message> parseMessageNodeList( NodeList nodeList ) throws MessageException, CloneNotSupportedException
	{
		Map<String, Message> msgList = new HashMap<>();
		for( int i=0; null != nodeList && i < nodeList.getLength(); ++i )
		{
			Node node = nodeList.item( i );     // no need to cast
			if( node.getNodeType() != Node.ELEMENT_NODE )  continue;
			
			logger.debug( "parseMessageNodeList() -> [{}] : [{}], [{}]", node.getNodeName()
					                        , node.getAttributes().getNamedItem(ATTR_ID).getTextContent()
					                        , node.getAttributes().getNamedItem(ATTR_PATH).getTextContent()
					                        );
			String msgId = node.getAttributes().getNamedItem(ATTR_ID).getTextContent();
			String fpath = node.getAttributes().getNamedItem(ATTR_PATH).getTextContent();
			
			//
			if( !msgManager.containMessage(msgId) )
				msgManager.load( fpath );
			
			Message m = msgManager.getMessage( msgId );
			msgList.put( msgId, m );
			logger.debug( "parseMessageNodeList() -> Message {} is saved", m.getElementId() );
		}
		
		return msgList;
	}
	
	public void parseMapperNodeList( List<MapperUnit> list, NodeList nodeList )
	{
		for( int i=0; null != nodeList && i < nodeList.getLength(); ++i )
		{
			Node node = nodeList.item(i);     // no need to cast
			if( node.getNodeType() != Node.ELEMENT_NODE ) continue;
			
			String nodeTag = node.getNodeName();
			String io = node.getAttributes().getNamedItem(ATTR_IO).getTextContent();
			String type = node.getAttributes().getNamedItem(ATTR_TYPE).getTextContent();
			String src = node.getAttributes().getNamedItem(ATTR_SRC).getTextContent();
			String dst = node.getAttributes().getNamedItem(ATTR_DST).getTextContent();
			String target;
			try { target = node.getAttributes().getNamedItem(ATTR_TARGET).getTextContent(); } catch( Exception e ) { target = ""; }
			String macro;
			try { macro = node.getAttributes().getNamedItem(ATTR_MACRO).getTextContent();   } catch( Exception e ) { macro = ""; }
			logger.debug( "parseMapperNodeList() -> [{}] [{}] : [{}], [{}], [{}], [{}], [{}]", nodeTag, io, type, src, dst, target, macro );
			
			ElementEnum ee = ElementEnum.getEnum( type );
			MessageMacroEnum mme = MessageMacroEnum.getEnum( macro );
			
			MapperUnit mu = new MapperUnit( io, ee, target, src, dst, mme );
			list.add( mu );
		}
	}
	
	public void parseDatasetNodeList( String parentTag, PublicMydataDataset pmd, NodeList nodeList )
	{
		for( int i=0; null != nodeList && i < nodeList.getLength(); ++i )
		{
			Node node = nodeList.item( i );     // no need to cast
			if( node.getNodeType() != Node.ELEMENT_NODE )  continue;
			
			String nodeTag = node.getNodeName();
			String format = node.getAttributes().getNamedItem(ATTR_FORMAT).getTextContent();
			String encoding = node.getAttributes().getNamedItem(ATTR_ENCODING).getTextContent();
			String crypto;
			try { crypto = node.getAttributes().getNamedItem(ATTR_CRYPTO).getTextContent(); } catch( Exception e ) { crypto = CharacterEnum.BLANK.getString(); }
			logger.debug( "parseDatasetNodeList() -> [{}] : [{}], [{}], [{}]", parentTag, nodeTag, format, encoding );
			
			if( node.hasChildNodes() )
			{
				logger.debug( "parseDatasetNodeList() [{}] Message -> {} has children", parentTag, node.getNodeName() );
				List<Map<String, String>> list = parseSubNodeList( node.getChildNodes() );
				if( list.size() > 0 )
				{
					FormatEnum       fe = FormatEnum.getEnum( format );
					EncodingEnum     ee = EncodingEnum.getEnum( encoding );
					CryptographyEnum ce = CryptographyEnum.getEnum( crypto );
					DatasetUnit du = new DatasetUnit( list, fe, ee, ce );
					
					if( TAG_REQUEST.equals(parentTag) )
					{
						if( TAG_INPUT.equalsIgnoreCase(nodeTag) )
						{
							pmd.setRequestInputDataset( du );
						}
						else if( TAG_OUTPUT.equalsIgnoreCase(nodeTag) )
						{
							pmd.setRequestOutputDataset( du );
						}
					}
					else if( TAG_RESPONSE.equals(parentTag) )
					{
						if( TAG_INPUT.equalsIgnoreCase(nodeTag) )
						{
							pmd.setResponseInputDataset( du );
						}
						else if( TAG_OUTPUT.equalsIgnoreCase(nodeTag) )
						{
							pmd.setResponseOutputDataset( du );
						}
					}
				}
				for( Map<String, String> arr: list )
				{
					logger.debug( "parseDatasetNodeList() [{}] -> ID : [{}], REFER_PATH : [{}]", parentTag, arr.get(ATTR_ID), arr.get(ATTR_REFER_PATH) );
				}
			}
		}
	}
	
	public List<Map<String, String>> parseSubNodeList( NodeList nodeList )
	{
		List<Map<String, String>> list = new ArrayList<>();
		
		for( int i=0; null != nodeList && i < nodeList.getLength(); ++i )
		{
			Node node = nodeList.item( i );     // no need to cast
			if( node.getNodeType() != Node.ELEMENT_NODE )  continue;
			
			String msgId = node.getAttributes().getNamedItem(ATTR_ID).getTextContent();
			//String refer = node.getAttributes().getNamedItem(ATTR_REFER).getTextContent();
			
			String referPath;
			try { referPath = node.getAttributes().getNamedItem(ATTR_REFER_PATH).getTextContent(); } catch( Exception e ) { referPath = ""; }
			logger.debug( "parseSubNodeList() -> [{}] : [{}], [{}], Parent [{}]", node.getNodeName(), msgId, referPath, node.getParentNode().getNodeName() );
			
			Map<String, String> map = new HashMap<>();
			map.put( ATTR_ID, msgId );
			map.put( ATTR_REFER_PATH, referPath );
			
			list.add( map );
		}
		
		return list;
	}
}
