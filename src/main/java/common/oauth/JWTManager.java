/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.oauth
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.oauth;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import common.enums.CharacterEnum;
import common.enums.EncodingEnum;
import common.exception.MessageException;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author prayerpaul
 *
 */
public class JWTManager
{
    protected Logger logger = LoggerFactory.getLogger( JWTManager.class );
    protected Map<String, SignInfo> algRepo;
    
    protected static final String XPATH_SIGNATURE = "/Signature";
    
    protected static final String ATTR_ID         = "ID";
    protected static final String ATTR_PUBLIC     = "PUBLIC";
    protected static final String ATTR_PRIVATE    = "PRIVATE";
    protected static final String ATTR_SECRET_KEY = "SECRET_KEY";
    protected static final String ATTR_TYPE       = "TYPE";
    
    protected JWTManager()
    {
        algRepo = new ConcurrentHashMap<>();
    }

    private static class SingletonHelper
    {
        private static final JWTManager INSTANCE = new JWTManager();
    }

    public static JWTManager getInstance()
    {
        return SingletonHelper.INSTANCE;
    }
    
    public SignInfo getSignInfo( String key ) { return algRepo.get( key ); }
    
    public void printSignatureAlgorithm()
    {
        for( Map.Entry<String, SignInfo> entry : algRepo.entrySet() )
        {
            logger.debug( "ID : [{}], ALG : [{}], PUB : [{}], PRI : [{}]", entry.getKey(), entry.getValue().getSignatureAlgorithm(), entry.getValue().getPublicKey(), entry.getValue().getPrivateKey()  );
        }
    }
    
    public void parseXML( String path ) throws MessageException
    {
        try 
        {
            InputSource is = new InputSource( Files.newBufferedReader(Paths.get(path), EncodingEnum.UTF8.getCharset()) );       // non-blocking I/O and auto-closable
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            
            // XXE Attack Mitigation
            dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            
            Document document = dbf.newDocumentBuilder().parse(is);
    
            XPath xPath = XPathFactory.newInstance().newXPath();
    
            // XPATH_SIGNATURE
            String inPath = XPATH_SIGNATURE + "/*";
            NodeList nodes = (NodeList)xPath.evaluate( inPath, document, XPathConstants.NODESET );
            
            logger.debug( "{}", nodes.getLength() );
            for( int i=0; i < nodes.getLength(); ++i )
            {
                Node node = nodes.item( i );  // no need to cast Node
                if( node.getNodeType() != Node.ELEMENT_NODE )  continue;
                
                registerSignatureAlgorithm( node );
            }
        }
        catch( XPathExpressionException | SAXException | IOException | ParserConfigurationException e )
        {
            throw new MessageException( e );
        }
    }
    
    private void registerSignatureAlgorithm( Node node ) throws MessageException
    {
        logger.debug("INPUT : [{}][{}]", node.getNodeName(), node.getAttributes().getNamedItem(ATTR_ID).getTextContent() );
        
        String algId = node.getAttributes().getNamedItem(ATTR_ID).getTextContent().trim();
        String algType = node.getAttributes().getNamedItem(ATTR_TYPE).getTextContent().trim();
        
        SignInfo si;
        SignatureAlgorithm sa = SignatureAlgorithm.valueOf( algType );
        // 대칭키인경우는 getSecretKey()
        // 비대칭키인경우는 getPublicKey()
        switch( sa )
        {
        // 대칭 암호화 알고리즘
        case HS256:
        case HS384:
        case HS512:
            String secKey = node.getAttributes().getNamedItem(ATTR_SECRET_KEY).getTextContent().trim();
            si = new SignInfo( sa, algId, secKey );
            break;
        // 비대칭 암호화 알고리즘
        case RS256:
        case RS384:
        case RS512:
        case PS256:
        case PS384:
        case PS512:
        case ES256:
        case ES384:
        case ES512:
            String pubFile = node.getAttributes().getNamedItem(ATTR_PUBLIC).getTextContent().trim();
            String pubKey = readPEMFile( pubFile );
            String priFile = node.getAttributes().getNamedItem(ATTR_PRIVATE).getTextContent().trim();
            String priKey = readPEMFile( priFile );
            
            si = new SignInfo( sa, algId, pubKey, priKey );
            break;
        default:
            throw new MessageException( "Unsupported SignatureAlgorithm : [" + sa + "]" );
        }
        logger.debug( "[{}], [{}], [{}], [{}]", si.getSignatureAlgorithm(), si.getKeyId(), si.getSecretKey(), si.getPrivateKey() );
        algRepo.put( algId, si);
    }
    
    private String readPEMFile( String path )
    {
        String key = null;
//        String path = file;
//        //String path = System.getProperty( "jwt.key.path" );
//        //path = path.replaceAll( "\\$\\{app.cfgbase\\}", System.getProperty("app.cfgbase") );
//        //path = path + "/" + file;
        try 
        {
        	File root = new File("");
        	
        	path = root.getAbsolutePath() + "/" + path;
        	
        	File f = new File( path );
        	if( !f.exists() )
        	{
        		logger.error( "file not found : [{}]", path );
        		return null;
        	}
        	
            String keyPEM = new String( Files.readAllBytes(Paths.get(f.getAbsolutePath())), StandardCharsets.UTF_8 );
            
            key = keyPEM.replace( "-----BEGIN PUBLIC KEY-----", CharacterEnum.BLANK.getString() )
                        .replace( "-----BEGIN PRIVATE KEY-----", CharacterEnum.BLANK.getString() )
                        .replaceAll( System.lineSeparator(), CharacterEnum.BLANK.getString() )
                        .replaceAll( "\n", CharacterEnum.BLANK.getString() )
                        .replace( "-----END PUBLIC KEY-----", CharacterEnum.BLANK.getString() )
                        .replace( "-----END PRIVATE KEY-----", CharacterEnum.BLANK.getString() );
        }
        catch( IOException ioe )
        {
            logger.error( "readPEMFile() error : ", ioe );
        }
        
        return key;
    }
}
