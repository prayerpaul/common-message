/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.oauth
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.oauth;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

import javax.crypto.SecretKey;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.enums.TokenPurposeEnum;
import common.exception.TokenException;
import common.utility.StringUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

/**
 * @author prayerpaul
 *
 */
public class JWTUtil
{
    private static Logger logger = LoggerFactory.getLogger( JWTUtil.class );
    
    public  static final String BOUNDARY = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd      = new SecureRandom();
    //private static SignatureAlgorithm algorithm;
    //private static String             publicKey;
    //private static String             privateKey; // secret Key
    //private static String       secret_key; // secret key or private key
    
    // HEADER
    public static final String JWT_HEADER_ALG_KEY = "alg";
    public static final String JWT_HEADER_TYP_KEY = "typ";
    public static final String JWT_HEADER_KID_KEY = "kid";
    public static final String JWT_HEADER_KTY_KEY = "kty";
    public static final String JWT_HEADER_USE_KEY = "use";
    public static final String JWT_HEADER_X5C_KEY = "x5c";
    // PAYLOAD
    public static final String JWT_PAYLOAD_SCOPE_KEY  = "scope";
    public static final String JWT_COOCON_ADMIN_SCOPE = "admin";
    
    // KEY LENGTH
    public static final int HS256_ES256_KEY_BITS_LEN = 256;
    public static final int HS384_ES384_KEY_BITS_LEN = 384;
    public static final int HS512_ES512_KEY_BITS_LEN = 512;
    public static final int RS256_PS256_KEY_BITS_LEN = 2048;
    public static final int RS384_PS384_KEY_BITS_LEN = 3072;
    public static final int RS512_PS512_KEY_BITS_LEN = 4096;

    // TTL : TIME TO LIVE
    public static final long ONE_SECOND_TO_MS = 1000L;
    public static final long ONE_SECOND_TTL   = 1;
    public static final long ONE_MINUTE_TTL   = ONE_SECOND_TTL * 60;
    public static final long ONE_HOUR_TTL     = ONE_MINUTE_TTL * 60;
    public static final long ONE_DAY_TTL      = ONE_HOUR_TTL * 24;
    public static final long SEVEN_DAYS_TTL   = ONE_DAY_TTL * 7;
    public static final long THIRTY_DAYS_TTL  = ONE_DAY_TTL * 30;
    public static final long NINETY_DAYS_TTL  = ONE_DAY_TTL * 90;
    public static final long ONE_YEAR_TTL     = ONE_DAY_TTL * 365;
    
    private JWTUtil() {}
    
    
    /**
     * 비대칭키 암호화(Asymmetric Key Encryption) 방식의 PublicKey 객체를 생성한다.
     * @param alg 비대칭키 암호화 알고리즘 Enum
     * @param pubKey 공개키 문자열
     * @return PublicKey 객체
     */
    public static PublicKey generatePublicKey( SignatureAlgorithm alg, String pubKey ) throws TokenException
    {
        KeyFactory factory = null;
        PublicKey key      = null;
        byte[]    keyBytes = Base64.getDecoder().decode( pubKey );
        
        try
        {
            if( alg.isRsa() )
            {
                factory = KeyFactory.getInstance("RSA");
                X509EncodedKeySpec ukeySpec = new X509EncodedKeySpec(keyBytes);
                key = factory.generatePublic(ukeySpec);
            }
            else if( alg.isEllipticCurve() )
            {
                factory = KeyFactory.getInstance( "EC" );
                X509EncodedKeySpec ukeySpec = new X509EncodedKeySpec( keyBytes );
                key = factory.generatePublic( ukeySpec );
            }
            else if( alg.isHmac() )
            {
                throw new TokenException( "HMAC is not Asymmetric Key Encryption Method" );
            }
        }
        catch( NoSuchAlgorithmException | InvalidKeySpecException e )
        {
            throw new TokenException(e);
        }
    
        return key;
    }
    
    
    /**
     * 비대칭키 암호화(Asymmetric Key Encryption) 방식의 PrivateKey 객체를 생성한다.
     * PrivateKey는 PKCS8EncodedKeySpec을 사용한다.
     * @param alg 비대칭키 암호화 알고리즘 Enum
     * @param priKey 비공개키 문자열
     * @return PrivateKey 객체
     */
    public static PrivateKey generatePrivateKey( SignatureAlgorithm alg, String priKey ) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        KeyFactory factory = null;
        PrivateKey key     = null;
        byte[]    keyBytes = Base64.getDecoder().decode( priKey );
        
        if( alg.isRsa() )
        {
            factory = KeyFactory.getInstance( "RSA" );
            PKCS8EncodedKeySpec ukeySpec = new PKCS8EncodedKeySpec( keyBytes );
            key = factory.generatePrivate( ukeySpec );
        }
        else if( alg.isEllipticCurve() )
        {
            factory = KeyFactory.getInstance( "EC" );
            PKCS8EncodedKeySpec ukeySpec = new PKCS8EncodedKeySpec( keyBytes );
            key = factory.generatePrivate( ukeySpec );
        }
        else if( alg.isHmac() )
        {
            throw new NoSuchAlgorithmException("HMAC is not Asymmetric Key Encryption Method");
        }
        
        return key;
    }
    
    public static boolean verifyKeyLength( SignatureAlgorithm alg, String key )
    {
        // 1 Byte == 8 Bits
        return alg.getMinKeyLength() <= (key.length() * 8);
    }
    
    public static String generateToken( SignInfo sa, TokenPurposeEnum tpe, Map<String, Object> params ) throws TokenException, NoSuchAlgorithmException, InvalidKeySpecException
    {
        Map<String, Object> header = createTokenHeader( sa );
        
        Map<String, Object> payload = createTokenPayload( tpe, params );
        //Jwts.builder().setHeader( header ).setClaims( null ).sign
        logger.debug( "private key : [{}]", sa.getPrivateKey() );
        
        if( sa.getSignatureAlgorithm().isHmac() )
        {
            SecretKey key = Keys.hmacShaKeyFor( sa.getSecretKey().getBytes(StandardCharsets.UTF_8) );
            return Jwts.builder().setHeader( header ).setClaims( payload ).signWith( key ).compact();
        }
        else if( sa.getSignatureAlgorithm().isRsa() || sa.getSignatureAlgorithm().isEllipticCurve() )
        {
            PrivateKey key = generatePrivateKey( sa.getSignatureAlgorithm(), sa.getPrivateKey() );
            return Jwts.builder().setHeader( header ).setClaims( payload ).signWith( key ).compact();
        }
        
        return null;
    }
    
    public static Map<String, Object> createTokenHeader( SignInfo sa ) throws TokenException
    {
        Map<String, Object> header = new HashMap<>();
        SignatureAlgorithm alg = sa.getSignatureAlgorithm();
        
        header.put( JWT_HEADER_TYP_KEY, Header.JWT_TYPE );
        header.put( JWT_HEADER_KID_KEY, sa.getKeyId()   );
        
        if( alg.isHmac() || alg.isRsa() || alg.isEllipticCurve() )
        {
            header.put( JWT_HEADER_ALG_KEY, alg );
        }
        else
        {
            throw new TokenException( "Unsupported SignatureAlgorithm : [" + alg + "]" );
        }
        
        return header;
    }
    
    public static Map<String, Object> createTokenPayload( TokenPurposeEnum tpe, Map<String, Object> params ) throws TokenException
    {
        //Map<String, Object> payload = new HashMap<String, Object>();
        long ttl = 0L;
        
        // iat
        Date now = new Date();
        params.put( Claims.ISSUED_AT, now );
        
        // iss
        String iss = (String)params.get( Claims.ISSUER );
        if( StringUtil.isBlank(iss) )
            throw new TokenException( "Invalid request : " + Claims.ISSUER + " is null" );
        //payload.put( Claims.ISSUER, iss );
        // aud
        String aud = (String)params.get( Claims.AUDIENCE );
        if( StringUtil.isBlank(aud) )
            throw new TokenException( "Invalid request : " + Claims.AUDIENCE + " is null" );
        //payload.put( Claims.AUDIENCE, aud );
        // scope
        String scope = (String)params.get( JWT_PAYLOAD_SCOPE_KEY );
        if( TokenPurposeEnum.COOCON_ADMIN != tpe && StringUtil.isBlank(scope) )
            throw new TokenException( "Invalid request : scope is null" );
        //payload.put( JWT_PAYLOAD_SCOPE_KEY, scope );
        
        String jti;
        switch( tpe )
        {
        case FINANCE_MYDATA7: // 통합인증 1차
            ttl = SEVEN_DAYS_TTL;
            // jti
            jti = (String)params.get( Claims.ID );
            if( StringUtil.isBlank(jti) )
                throw new TokenException( "Invalid request : jti is null" );
            //payload.put( Claims.ID, jti );
            //payload.put( JWT_PAYLOAD_SCOPE_KEY, params.get(JWT_PAYLOAD_SCOPE_KEY) );
            break;
        case FINANCE_MYDATA90: // 금융 Mydata AccessToken
            ttl = NINETY_DAYS_TTL;
            // jti
            jti = (String)params.get( Claims.ID );
            if( StringUtil.isBlank(jti) )
                throw new TokenException( "Invalid request : jti is null" );
            //payload.put( Claims.ID, jti );
            break;
        case FINANCE_MYDATA365: // 금융 Mydata RefreshToken  
        case FINANCE_SUPPORT:   // 지원API AccessToken
            ttl = ONE_YEAR_TTL;
            // jti
            jti = (String)params.get( Claims.ID );
            if( StringUtil.isBlank(jti) )
                throw new TokenException( "Invalid request : jti is null" );
            //payload.put( Claims.ID, jti );
            break;
        case COOCON_ADMIN:
            ttl = ONE_YEAR_TTL;
            // scope 설정
            params.put( JWT_PAYLOAD_SCOPE_KEY, JWT_COOCON_ADMIN_SCOPE );
            // jti 설정 : 1회용 Token의 의미
            params.put( Claims.ID, UUID.randomUUID().toString() );
            break;
        default:
            throw new TokenException( "Unsupported TokenPurposeEnum : [" + tpe + "]" );
        }

        //Date nbf = new Date( now.getTime() - (ONE_SECOND_TTL * 1 * ONE_SECOND_TO_MS) );
        Date nbf = new Date( now.getTime() );
        Date exp = new Date( now.getTime() + (ttl * ONE_SECOND_TO_MS) );
        params.put( Claims.EXPIRATION, exp );
        params.put( Claims.NOT_BEFORE, nbf );
        
        return params;
    }
    
    /**
     * 항상 JwtParser의 parseClaimsJws 메서드를 호출하여 Token을 검증할 것.
     * @param token JWT 토큰
     * @return 토큰을 파싱하여 만들어진 Jws<Claims>
     * @throws TokenException
     */
    public static Jws<Claims> parseToken( String token ) throws TokenException
    {
        SignInfo si = getSignatureAlgorithmInfo( token );
        logger.debug( "TOKEN : [{}]", token );
        logger.debug( "SECRET KEY : [{}]", si.getSecretKey() );
        
        Jws<Claims> jws = null;
        
        try
        {
            SignatureAlgorithm alg = si.getSignatureAlgorithm();
            if( alg.isHmac() )
            {
                SecretKey key = Keys.hmacShaKeyFor( si.getSecretKey().getBytes(StandardCharsets.UTF_8) );
                jws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws( token );
            }
            else if( alg.isRsa() || alg.isEllipticCurve() )
            {
                PublicKey key = generatePublicKey( alg, si.getPublicKey() );
                jws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws( token );
            }
        }
        catch( JwtException ex )
        {
            logger.error( "parseToken() occurs Exception : {}", ex );
            throw new TokenException( ex );
        }
        
        return jws;
    }
    
    private static SignInfo getSignatureAlgorithmInfo( String token )
    {
        String[] tokArr = token.split( "\\." );
        logger.debug( "ARRAY LENGTH : {}", tokArr.length );
        String header = tokArr[0];
        
        logger.debug( "JWT HEADER1 : [{}]", header );
        String decHeader = new String( Base64.getDecoder().decode(header) );
        logger.debug( "JWT HEADER2 : [{}]", decHeader );
        
        JSONParser parser = new JSONParser();
        JSONObject obj = null;
        try
        {
            obj = (JSONObject)parser.parse( decHeader );
        }
        catch( ParseException e )
        {
            logger.error( "getSignatureAlgorithmInfo() occurs Exception : {}", e );
            return null;
        }
        
        //String alg = (String)obj.get( JWT_HEADER_ALG_KEY );
        String kid = (String)obj.get( JWT_HEADER_KID_KEY );
        logger.debug( "kid : [{}", kid );
    
        return JWTManager.getInstance().getSignInfo( kid );
    }
    
    public static Jws<Claims> validateToken( String token ) throws TokenException
    {
        Jws<Claims> jws = parseToken( token );
        
        return jws;
    }
    
    /**
     * JWT에 필요한 Client ID를 생성한다.
     * @return
     */
    public static String generateClientId()
    {
        return generateClientId( HS256_ES256_KEY_BITS_LEN );
    }
    public static String generateClientId( int len )
    {
        rnd.setSeed( System.currentTimeMillis() );
        return getRandomString( len );
    }
    public static String generateClientId( int len, String seed )
    {
        rnd.setSeed( seed.getBytes() );
        return getRandomString( len );
    }
    
    /**
     * JWT에 필요한 Client SECRET를 생성한다.
     * @return
     */
    public static String generateClientSecret()
    {
        return generateClientSecret( HS256_ES256_KEY_BITS_LEN );
    }
    public static String generateClientSecret( int len )
    {
        rnd.setSeed( System.currentTimeMillis() );
        return getRandomString( len );
    }
    public static String generateClientSecret( int len, String seed )
    {
        rnd.setSeed( seed.getBytes() );
        return getRandomString( len );
    }

    
    /**
     * Random Alpha + numeric 문자열을 생성한다.
     * @param length
     * @return
     */
    public static String getRandomString( int length )
    {
        StringBuilder sb = new StringBuilder( length );
        for( int i = 0; i < length; i++ )
            sb.append( BOUNDARY.charAt(rnd.nextInt(BOUNDARY.length())) );
        
        return sb.toString();
    }
    
    /**
     * Generates a new key pair( private, public )
     * @return 
     * @throws NoSuchAlgorithmException
     */
    public static KeyPair generateRSAKeyPair() throws TokenException
    {
        return generateAsymmetricKeyPair( SignatureAlgorithm.RS256, RS256_PS256_KEY_BITS_LEN );
    }
    public static KeyPair generateECDSAKeyPair() throws TokenException
    {
        return generateAsymmetricKeyPair( SignatureAlgorithm.ES256, HS256_ES256_KEY_BITS_LEN );
    }
    public static KeyPair generateAsymmetricKeyPair( SignatureAlgorithm sa, int bits ) throws TokenException
    {
        Security.addProvider( new org.bouncycastle.jce.provider.BouncyCastleProvider() );
    
        KeyPairGenerator generator = null;
        try
        {
            if( sa.isRsa() )
            {
                generator = KeyPairGenerator.getInstance("RSA");
                generator.initialize(bits);
            }
            else if( sa.isEllipticCurve() )
            {
                generator = KeyPairGenerator.getInstance("EC");
                generator.initialize(bits);
            }
            else
            {
                throw new TokenException("HMAC is not Asymmetric Key Encryption Method");
            }
        }
        catch( NoSuchAlgorithmException e )
        {
            throw new TokenException( e );
        }
        
        return generator.generateKeyPair();
    }
    
    /**
     * 
     * @param pubPath
     * @param priPath
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static void saveRSAKeyFile( String pubPath, String priPath ) throws TokenException
    {
        try
        {
            KeyPair keyPair = generateRSAKeyPair();
    
            Key privKey = keyPair.getPrivate();
            Key pubKey = keyPair.getPublic();
    
            // 2022.05.11 swcho.rnd : change to NIO function and do not use restricted function.
            StringBuilder sb = new StringBuilder();
            sb.append("-----BEGIN PUBLIC KEY-----\n");
            sb.append(Base64.getMimeEncoder().encodeToString(pubKey.getEncoded()));
            sb.append("\n-----END PUBLIC KEY-----\n");
            Files.write(Paths.get(pubPath), sb.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE_NEW);
    
            sb = new StringBuilder();    // It would be better setLength(0) ?
            sb.append("-----BEGIN PRIVATE KEY-----\n");
            sb.append(Base64.getMimeEncoder().encodeToString(privKey.getEncoded()));
            sb.append("\n-----END PRIVATE KEY-----\n");
            Files.write(Paths.get(priPath), sb.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE_NEW);
        }
        catch( IOException e )
        {
            throw new TokenException( e );
        }
    }

    
    /**
     * 
     * @param pubPath
     * @param priPath
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static void saveECDSAKeyFile( String pubPath, String priPath ) throws TokenException
    {
        try
        {
            KeyPair keyPair = generateECDSAKeyPair();

            Key privKey = keyPair.getPrivate();
            Key pubKey = keyPair.getPublic();
    
            // 2022.05.11 swcho.rnd : change to NIO function and do not use restricted function.
            StringBuilder sb = new StringBuilder();
            sb.append("-----BEGIN PUBLIC KEY-----\n");
            sb.append(Base64.getMimeEncoder().encodeToString(pubKey.getEncoded()));
            sb.append("\n-----END PUBLIC KEY-----\n");
            Files.write(Paths.get(pubPath), sb.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE_NEW);
    
            sb = new StringBuilder();    // It would be better setLength(0) ?
            sb.append("-----BEGIN PRIVATE KEY-----\n");
            sb.append(Base64.getMimeEncoder().encodeToString(privKey.getEncoded()));
            sb.append("\n-----END PRIVATE KEY-----\n");
            Files.write(Paths.get(priPath), sb.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE_NEW);
        }
        catch( IOException e )
        {
            throw new TokenException( e );
        }
    }
}
