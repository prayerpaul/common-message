/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.oauth
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.oauth;

import java.util.Base64;

import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author prayerpaul
 *
 */
public class SignInfo
{
    private String             keyId;
    private SignatureAlgorithm sa;
    private String             secKey;
    private String             priKey;
    private String             pubKey;
    
    public SignInfo( String sa, String id, String key )
    {
        this( SignatureAlgorithm.valueOf(sa), id, key );
    }
    
    public SignInfo( SignatureAlgorithm sa, String id, String key )
    {
        this.sa     = sa;
        this.keyId  = id;
        this.secKey = key;
    }
    
    public SignInfo( String sa, String id, String pub, String priv )
    {
        this( SignatureAlgorithm.valueOf(sa), id, pub, priv );
    }
    
    public SignInfo( SignatureAlgorithm sa, String id, String pub, String priv )
    {
        this.sa     = sa;
        this.keyId  = id;
        this.priKey = priv;
        this.pubKey = pub;
    }
    
    public SignatureAlgorithm getSignatureAlgorithm() { return this.sa; }
    public String             getKeyId() { return this.keyId; }
    public String             getPublicKey() { return this.pubKey; }
    public String             getPrivateKey() { return this.priKey; }
    public String             getSecretKey() { return this.secKey; }
    public byte[]             getPublicKey2Byte() { return Base64.getDecoder().decode( this.pubKey ); }
    public byte[]             getPrivateKey2Byte() { return Base64.getDecoder().decode( this.priKey ); }
}
