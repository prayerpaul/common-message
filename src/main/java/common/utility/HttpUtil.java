/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.utility
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.utility;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.enums.CharacterEnum;
import common.enums.EncodingEnum;
import common.enums.HttpMethodEnum;

public class HttpUtil
{
    private static Logger logger = LoggerFactory.getLogger( HttpUtil.class );
    public static final int DEFAULT_CONNECTION_TIMOUT_TTL = 60000;
    public static final int DEFAULT_READ_TIMOUT_TTL       = 60000;
    
    
    private HttpUtil() {}
    /**
     * 
     * @param method
     * @param url
     * @param headParam
     * @param body
     * @return
     */
    public static HttpURLConnection request( HttpMethodEnum method, String url, Map<String, String> headParam, String body )
    {
        OutputStreamWriter writer  = null;
        HttpURLConnection  conn    = null;
        
        if( StringUtil.isBlank(url) ) return null;
        
        try 
        {
            // Create HttpURLConnection
            URL httpUrl = new URL( url );
            conn = (HttpURLConnection)httpUrl.openConnection();
            
            // Set HTTP Header
            if( HttpMethodEnum.POST == method )
            {
                conn.setDoOutput( true );
                conn.setUseCaches( false );
            }
            
            conn.setDoInput( true );
            conn.setRequestMethod( method.getString() );
            conn.setConnectTimeout( DEFAULT_CONNECTION_TIMOUT_TTL );
            conn.setReadTimeout( DEFAULT_READ_TIMOUT_TTL );
            
            // set HTTP Header
            Iterator<Entry<String, String>> entries = headParam.entrySet().iterator();
            while( entries.hasNext() )
            {
                Entry<String, String> entry = entries.next();
                String                    key   = entry.getKey();
                String                    value = entry.getValue();
                conn.setRequestProperty( key, value );
            }
            
            logger.debug( getHttpHeader(conn) );
            // 
            if( HttpMethodEnum.POST == method )
            {
                conn.setRequestProperty( "Content-Length", String.valueOf(body.length()) );
                writer = new OutputStreamWriter( conn.getOutputStream() );
                writer.write( body );
                writer.flush();
            }
        }
        catch( IOException ioe )
        {
            logger.error( "request() occurs Exception : [{}]", StringUtil.printStackTraceString(ioe) );
        }
        finally
        {
            if( null != writer )
            {
                try { writer.close(); }
                catch( IOException e ) { logger.error( "request() occurs Exception : [{}]", StringUtil.printStackTraceString(e) ); }
            }
        }
        
        return conn;
    }
    
    /**
     * 
     * @param method
     * @param url
     * @param headParam
     * @param body
     * @return
     */
    public static String requestFully( HttpMethodEnum method, String url, Map<String, String> headParam, String body )
    {
        if( StringUtil.isBlank(url) ) return null;
        
        String             rspBody = null;
        OutputStreamWriter writer  = null;
        HttpURLConnection  conn    = null;
        try 
        {
            // Create HttpURLConnection
            URL httpUrl = new URL( url );
            conn = (HttpURLConnection)httpUrl.openConnection();
            
            // Set HTTP Header
            if( HttpMethodEnum.POST == method )
            {
                conn.setDoOutput( true );
                conn.setUseCaches( false );
            }
            
            conn.setDoInput( true );
            conn.setRequestMethod( method.getString() );
            conn.setConnectTimeout( DEFAULT_CONNECTION_TIMOUT_TTL );
            conn.setReadTimeout( DEFAULT_READ_TIMOUT_TTL );
            
            // set HTTP Header
            Iterator<Entry<String, String>> entries = headParam.entrySet().iterator();
            while( entries.hasNext() )
            {
                Entry<String, String> entry = entries.next();
                String                    key   = entry.getKey();
                String                    value = entry.getValue();
                conn.setRequestProperty( key, value );
            }
            
            // 
            if( HttpMethodEnum.POST == method )
            {
                conn.setRequestProperty( "Content-Length", String.valueOf(body.length()) );
                writer = new OutputStreamWriter( conn.getOutputStream() );
                writer.write( body );
                writer.flush();
            }
            
            // Response
            logger.debug( "HTTP RESPONSE HEADER : [{}]", getResponseHeader(conn) );
            rspBody = getResponseBody( conn );
        }
        catch( IOException ioe )
        {
            logger.error( "request() occurs Exception : [{}]", StringUtil.printStackTraceString(ioe) );
        }
        finally
        {
            if( null != writer )
            {
                try { writer.close(); }
                catch( IOException e ) { logger.error( "request() occurs Exception : [{}]", StringUtil.printStackTraceString(e) ); }
            }
            if( null != conn )
            {
                conn.disconnect();
            }
        }
        
        return rspBody;
    }
    
    /**
     * 
     * @param method
     * @param url
     * @param headParam
     * @param bodyParam
     * @return
     */
    public static String request( HttpMethodEnum method, String url, Map<String, String> headParam, Map<String, String> bodyParam )
    {
        String body = null;
        try
        {
            makeParam2String( bodyParam );
        }
        catch( UnsupportedEncodingException uee )
        {
            logger.error( "makeParam2String() occurs Exception : [{}]", StringUtil.printStackTraceString(uee) );
            return body;
        }
        return requestFully( method, url, headParam, body );
    }
    
    /**
     * 
     * @param params
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String makeParam2String( Map<String, String> params ) throws UnsupportedEncodingException
    {
        StringBuilder sb = new StringBuilder();
        
        // key1=value1&key2=value2&
        for( Entry<String, String> entry : params.entrySet() )
        {
            sb.append( URLEncoder.encode(entry.getKey(), EncodingEnum.UTF8.getString()) );
            sb.append( CharacterEnum.EQUAL.getString() );
            sb.append( URLEncoder.encode(entry.getValue(), EncodingEnum.UTF8.getString()) );
            sb.append( CharacterEnum.AMPERSAND.getString() );
        }
        
        String paramStr = sb.toString();
        // key1=value1&key2=value2& ==> key1=value1&key2=value2
        return paramStr.length() > 0 ? paramStr.substring(0, paramStr.length()-1) : paramStr;
    }
    
    /**
     * 
     * @param conn
     * @return
     */
    public static String getResponseHeader( HttpURLConnection conn )
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.append(conn.getResponseCode()).append(CharacterEnum.SPACE.getString());
            sb.append(conn.getResponseMessage()).append(CharacterEnum.LINE_FEED.getString());
        }
        catch( IOException e )
        {
            return sb.toString();
        }
        
        Map<String, List<String>> headers = conn.getHeaderFields();
        for( Entry<String, List<String>> entry : headers.entrySet() )
        {
            if( null != entry.getKey() )
                sb.append( entry.getKey()).append(CharacterEnum.COLON.getString()).append(CharacterEnum.SPACE.getString() );
            
            List<String> values = entry.getValue();
            Iterator<String> iter = values.iterator();
            if( iter.hasNext() )
            {
                sb.append( iter.next() );
                while( iter.hasNext() )
                    sb.append( CharacterEnum.COMMA.getString()).append(CharacterEnum.SPACE.getString() ).append( iter.next() );
            }
            sb.append( CharacterEnum.LINE_FEED.getString() );
        }
        
        return sb.toString();
    }
    
    /**
     * 
     * @param conn
     * @return
     */
    public static String getHttpHeader( HttpURLConnection conn )
    {
        StringBuilder sb = new StringBuilder();
        
        Map<String, List<String>> headers = conn.getHeaderFields();
        for( Entry<String, List<String>> entry : headers.entrySet() )
        {
            if( null != entry.getKey() )
                sb.append( entry.getKey() + CharacterEnum.COLON.getString() + CharacterEnum.SPACE.getString() );
            
            List<String> values = entry.getValue();
            Iterator<String> iter = values.iterator();
            if( iter.hasNext() )
            {
                sb.append( iter.next() );
                while( iter.hasNext() )
                    sb.append( CharacterEnum.COMMA.getString() + CharacterEnum.SPACE.getString() + iter.next() );
            }
            sb.append( CharacterEnum.LINE_FEED.getString() );
        }
        
        return sb.toString();
    }
    
    public static String getResponseBody( HttpURLConnection conn )
    {
        return getResponseBody( conn, EncodingEnum.UTF8 );
    }
    /**
     * 
     * @param conn
     * @return
     */
    public static String getResponseBody( HttpURLConnection conn, EncodingEnum ee )
    {
        StringBuilder sb = new StringBuilder();
        
        BufferedReader br = null;
        try
        {
            int status = conn.getResponseCode();
            Reader streamReader = null;
            if( status >= 200 && status <= 299 )
                streamReader = new InputStreamReader( conn.getInputStream(), ee.getCharset() );
            else
                streamReader = new InputStreamReader( conn.getErrorStream(), ee.getCharset() );
    
            br = new BufferedReader( streamReader );
            String line = null;
            while( (line = br.readLine()) != null )
            {
                sb.append( line );
            }
        }
        catch( IOException ioe )
        {
            logger.error( "getResponseBody() occurs Exception : [{}]", StringUtil.printStackTraceString(ioe) );
            return null;
        }
        finally
        {
            if( null != br )
            {
                try { br.close(); }
                catch( IOException e ) { logger.error( "BufferedReader close() occurs Exception : [{}]", StringUtil.printStackTraceString(e) ); }
            }
        }
        
        return sb.toString();
    }
    
    /**
     * 
     * @param conn
     * @return
     */
    public static JSONObject getResponseBody2Json( HttpURLConnection conn )
    {
        JSONObject jsonObj = null;
        
        BufferedReader br = null;
        try
        {
            
            int status = conn.getResponseCode();
            Reader streamReader = null;
            if( status > 299 )
                streamReader = new InputStreamReader( conn.getErrorStream(), EncodingEnum.UTF8.getString() );
            else
                streamReader = new InputStreamReader( conn.getInputStream(), EncodingEnum.UTF8.getString() );
    
            br = new BufferedReader( streamReader );
            jsonObj = (JSONObject)JSONValue.parse( br );
        }
        catch( IOException ioe )
        {
            logger.error( "getResponseBody2Json() occurs Exception : [{}]", ioe.getMessage() );
            return null;
        }
        finally
        {
            if( null != br )
            {
                try { br.close(); }
                catch( IOException e ) { logger.error( "BufferedReader close() occurs Exception : [{}]", StringUtil.printStackTraceString(e) ); }
            }
        }
        
        return jsonObj;
    }
    
    /**
     * 
     * @param in
     * @param size
     * @return
     * @throws IOException
     */
    public static byte[] readBytesFromStream( InputStream in, int size ) throws IOException
    {
        if( size == 0 )
            return new byte[0];
        
        byte[]                buff        = null;
        int                   nDataLength = size;
        ByteArrayOutputStream out         = new ByteArrayOutputStream();
        int                   nReadSize;
        if( size > 0 )
        {
            nReadSize = Math.min( 1024, size );
            
            int nTmp = 0;
            for( buff = new byte[nReadSize]; nDataLength > 0; nDataLength -= nTmp )
            {
                nReadSize = Math.min( 1024, nDataLength );

                try
                {
                    nTmp = in.read( buff, 0, nReadSize );
                }
                catch( SocketException var8 )
                {
                    nTmp = -1;
                    var8.printStackTrace();
                }
                catch( IOException var9 )
                {
                    throw makeIOException( size, out.toByteArray() );
                }

                if( nTmp == -1 )
                {
                    throw makeIOException( size, out.toByteArray() );
                }

                out.write( buff, 0, nTmp );
            }
        }
        else // size < 0 like readFully()
        {
            buff = new byte[1024];

            boolean bReadStream;
            for( bReadStream = false; (nReadSize = in.read( buff )) != -1; out.write( buff, 0, nReadSize ) )
            {
                if( !bReadStream )
                {
                    bReadStream = true;
                }
            }

            if( !bReadStream )
            {
                throw new IOException( "InputStream error -1 (read size:0)" );
            }
        }

        return out.toByteArray();
    }
    
    /**
     * 
     * @param size
     * @param read
     * @return
     */
    protected static IOException makeIOException( int size, byte[] read )
    {
        int           nReadSize = read != null ? read.length : 0;
        String        strMsg    = "[]";
        StringBuilder sb    = new StringBuilder();
        if( nReadSize > 0 )
        {
            sb.append( "Read Length [" ).append( nReadSize ).append( "]" );
            sb.append( "[" );
            int nMax = Math.min( 20, read.length );
            int i    = 0;
            while( i < nMax )
            {
                if( i != 0 )
                {
                    sb.append( " " );
                }
                sb.append( Integer.toHexString( read[i] & 255 ) );
                ++i;
            }
            sb.append( "]" );
        }
    
        return new IOException( "channel read error init size (" + size + ") read size(" + nReadSize + ")" + strMsg );
    }
}
