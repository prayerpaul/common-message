/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.utility
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.Constable;
import common.enums.CharacterEnum;
import common.enums.EncodingEnum;
import common.enums.PaddingEnum;
import common.exception.MessageException;
import common.oauth.JWTUtil;

/**
 * @author prayerpaul
 *
 */
public class StringUtil
{
	private static Logger logger = LoggerFactory.getLogger(StringUtil.class);
    
    static final String[] HANGUL_SUNG = {
            "가", "간", "갈", "감", "강", "개", "견", "경", "계", "고", "곡", "공", "곽", "교", "구", "국", "군", "궁", "궉", "권", "근", "금", "기", "길",
            "김", "나", "난", "남", "낭", "내", "노", "뇌", "누", "단", "담", "당", "대", "도", "돈", "동", "두", "마", "만", "매", "맹", "명", "모", "목",
            "묘", "묵", "문", "미", "민", "박", "반", "방", "배", "백", "범", "변", "복", "봉", "부", "비", "빈", "빙", "사", "삼", "상", "서", "석", "선",
            "설", "섭", "성", "소", "손", "송", "수", "순", "승", "시", "신", "심", "십", "아", "안", "애", "야", "양", "어", "엄", "여", "연", "염", "엽",
            "영", "예", "오", "옥", "온", "옹", "왕", "요", "용", "우", "운", "원", "위", "유", "육", "윤", "은", "음", "이", "인", "임", "자", "장", "저",
            "전", "점", "정", "제", "조", "종", "좌", "주", "준", "즙", "증", "지", "진", "차", "창", "채", "천", "초", "최", "추", "춘", "탁", "탄", "태",
            "판", "팽", "편", "평", "포", "표", "풍", "피", "필", "하", "학", "한", "함", "해", "허", "현", "형", "호", "홍", "화", "환", "황", "후", "흥",
            "강전", "남궁", "독고", "동방", "망절", "사공", "서문", "선우", "소봉", "어금", "장곡", "제갈", "황보"
    };
    
    static final char HANGUL_SYLLABLES_BEGIN = 0xAC00;
    static final char HANGUL_SYLLABLES_END   = 0xD7A3;
    
    static final char HANGUL_COMPATIBILITY_BEGIN = 0x3130;
    static final char HANGUL_COMPATIBILITY_END   = 0x318E;
    
    // ㄱ      ㄲ      ㄴ      ㄷ      ㄸ      ㄹ      ㅁ      ㅂ      ㅃ      ㅅ      ㅆ      ㅇ      ㅈ      ㅉ      ㅊ      ㅋ      ㅌ      ㅍ      ㅎ
    static final char[] HANGUL_CHOSUNG   = { 0x3131, 0x3132, 0x3134, 0x3137, 0x3138, 0x3139, 0x3141, 0x3142, 0x3143, 0x3145, 0x3146, 0x3147, 0x3148, 0x3149, 0x314A, 0x314B, 0x314C, 0x314D, 0x314E };
                                            // ㅏ      ㅐ      ㅑ      ㅒ      ㅓ      ㅔ      ㅕ      ㅖ      ㅗ      ㅘ      ㅙ      ㅚ      ㅛ      ㅜ      ㅝ      ㅞ      ㅟ      ㅠ      ㅡ      ㅢ      ㅣ
    static final char[] HANGUL_JUNGSUNG  = { 0x314F, 0x3150, 0x3151, 0x3152, 0x3153, 0x3154, 0x3155, 0x3156, 0x3157, 0x3158, 0x3159, 0x315A, 0x315B, 0x315C, 0x315D, 0x315E, 0x315F, 0x3160, 0x3161, 0x3162, 0x3163 };
                                            //         ㄱ      ㄲ      ㄳ      ㄴ      ㄵ      ㄶ      ㄷ      ㄹ      ㄺ      ㄻ      ㄼ      ㄽ      ㄾ      ㄿ      ㅀ      ㅁ      ㅂ      ㅄ      ㅅ      ㅆ      ㅇ      ㅈ      ㅊ      ㅋ      ㅌ      ㅍ      ㅎ
    static final char[] HANGUL_JONGSUNG  = { 0x0000, 0x3131, 0x3132, 0x3133, 0x3134, 0x3135, 0x3136, 0x3137, 0x3139, 0x313A, 0x313B, 0x313C, 0x313D, 0x313E, 0x313F, 0x3140, 0x3141, 0x3142, 0x3144, 0x3145, 0x3146, 0x3147, 0x3148, 0x314A, 0x314B, 0x314C, 0x314D, 0x314E };
    
    
    /**
     * 생성자, 외부에서 객체를 인스턴스화 할 수 없도록 설정
     */
    private StringUtil() {}
    
    /**
     * 대상 문자열(strTarget)이 전각문자로 구성되어 있는지 확인한다.
     *
     * @param strTarget 전각여부를 확인할 문자
     * @return 전각문자만으로 구성된 문자열일 경우 true, 아니면 false
     */
    public static boolean isFullChar( String strTarget )
    {
        byte[] byteArray;
        byteArray = strTarget.getBytes();

        for( int i=0; i < byteArray.length; ++i )
        {
            if( (byteArray[i] >= (byte)0x81 && byteArray[i] <= (byte)0x9f)
            ||  (byteArray[i] >= (byte)0xe0 && byteArray[i] <= (byte)0xef) )
            {
                if( (byteArray[i+1] >= (byte)0x40 && byteArray[i+1] <= (byte)0x7e)
                ||  (byteArray[i+1] >= (byte)0x80 && byteArray[i+1] <= (byte)0xfc) )
                {
                    i++;
                }
                else
                    return false;
            }
            else
                return false;

        }

        return true;
    }
    
    /**
     * 대상 문자열(strTarget)이 반각문자로 구성되어 있는지 확인한다.
     *
     * @param strTarget 반각여부를 확인할 문자
     * @return 반각문자만으로 구성된 문자열일 경우 true, 아니면 false
     */
    public static boolean isHalfChar( String strTarget )
    {
        byte[] byteArray;
        byteArray = strTarget.getBytes();

        for( int i=0; i < byteArray.length; ++i )
        {
            if( (byteArray[i] >= (byte)0x81 && byteArray[i] <= (byte)0x9f)
            ||  (byteArray[i] >= (byte)0xe0 && byteArray[i] <= (byte)0xef) )
            {
                if( (byteArray[i+1] >= (byte)0x40 && byteArray[i+1] <= (byte)0x7e)
                ||  (byteArray[i+1] >= (byte)0x80 && byteArray[i+1] <= (byte)0xfc) )
                {
                    return false;
                }
            }
        }

        return true;
    }
    
    /**
     * 문자열이 null 이거나 길이가 0인지 체크한다.
     * @param str
     * @return
     */
    public static boolean isBlank( String str )
    {
        return ( null == str || 0 == str.length() );
    }
    
    /**
     * 문자열이 정수형이나 실수형인지 체크한다.
     * ^([-+]?[0-9]+) : 맨 앞에 '-', '+' 기호가 없거나 있고, 숫자가 1개이상 발생함.
     * (.[0-9]*)?$ : 마지막에 '.' + 숫자가 0개 이상인 패턴이 없거나 발생함.
     * @param str 정수형이나 실수형 문자열
     * @return boolean true : 문자열이 숫자형, false : 문자열이 숫자형이 아님.
     */
    public static boolean isNumeric( String str )
    {
        /*
         * 숫자 Regular Expression 패턴
         */
        final String NUMERIC_PATTERN  = "^([-+]?[0-9]+)(.[0-9]*)?$";
        
        return str.matches( NUMERIC_PATTERN );
    }
    
    /**
     * 문자열이 정수형인지 체크한다.
     * ^([0-9]+) : 숫자가 1개이상 발생함.
     * @param str 정수형 문자열
     * @return boolean true : 문자열이 정수형, false : 문자열이 정수형이 아님.
     */
    public static boolean isNumber( String str )
    {
        final String NUMBER_PATTERN   = "^[0-9]+$";
        
        return str.matches( NUMBER_PATTERN );
    }
    
    /**
     * 문자열이 금액 표현 형식인지 체크한다.
     * ^[0-9]+ : 맨 앞에 0에서 9라는 숫자가 하나 이상
     * | : 이거나
     * ^[0-9]{1,3}(,[0-9]{3})* : 맨 앞에 0에서 9라는 숫자가 1에서 3개까지 이고 ,와 숫자 3개로 이루어진 패턴이 0이상 발생
     * (.[0-9]{1,3})?$ : 마지막에 '.' + 숫자가 1자리에서 3자리로 이루어진 패턴이 0이거나 1번 발생
     * @param str 금액을 표현하는 문자열
     * @return boolean true : 문자열이 금액을 표현함, false : 문자열이 금액을 표현하지 않음.
     */
    public static boolean isCurrency( String str )
    {
        final String CURRENCY_PATTERN = "^(-?[0-9]+|-?[0-9]{1,3}(,[0-9]{3})*)(.[0-9]{1,3})?$";
        
        return str.matches( CURRENCY_PATTERN );
    }
    
    /**
     * str 문자열에 tok에 해당하는 문자열이 포함되어 있는지 검증한다.
     * ex)
     * str = "cn=손승길(동아약국)(sonseungkil)0004028700475583,ou=sonseungkil,ou=KMB,ou=corporation,o=yessign,c=kr";
     * tok = "손승길";
     * StrUtil.isMatch( str, tok );
     * @param str 검증하고자 하는 문자열
     * @param tok 검증대상 token
     * @return true tok가 포함되어 있는 str문자열, false tok가 포함되어 있지 않은 str문자열
     */
    public static boolean isPatternMatch( String str, String tok )
    {
        Pattern ptn = Pattern.compile( tok );
        Matcher mtc = ptn.matcher( str );

        return mtc.matches();
    }
    
    /**
     * 반각문자로 변경한다
     *
     * @param src     변경할값
     * @return String 변경된값
     */
    public static String toHalfChar( String src )
    {
        StringBuilder sb = new StringBuilder();
        char c;
        int nSrcLength = src.length();
        for( int i = 0; i < nSrcLength; i++ )
        {
            c = src.charAt( i );
            // 영문이거나 특수 문자 일경우.
            // 0xFF01('！'), 0xFF5E('˜')
            if( c >= 0xFF01 && c <= 0xFF5E )
            {
                c -= 0xFEE0;
            }
            // 공백일경우
            else if( c == 0x3000 )
            {
                c = 0x20;
            }

            // 문자열 버퍼에 변환된 문자를 쌓는다
            sb.append( c );
        }

        return sb.toString();
    }
    
    /**
     * 전각문자로 변경한다.
     *
     * @param src     변경할값
     * @return String 변경된값
     */
    public static String toFullChar( String src )
    {
        // 입력된 스트링이 null 이면 null 을 리턴
        if( null == src )
            return null;

        // 변환된 문자들을 쌓아놓을 StringBuffer 를 마련한다
        StringBuilder strBuf = new StringBuilder();
        char c;
        int nSrcLength = src.length();
        for( int i = 0; i < nSrcLength; i++ )
        {
            c = src.charAt( i );
            // 영문이거나 특수 문자 일경우.
            // 0x21('!'), 0x7E('~')
            if( c >= 0x21 && c <= 0x7E )
            {
                c += 0xFEE0;
            }
            // 공백일경우
            else if( c == 0x20 )
            {
                c = 0x3000;
            }

            // 문자열 버퍼에 변환된 문자를 쌓는다
            strBuf.append( c );
        }
        return strBuf.toString();
    }
    
    /**
     * 문자열 좌측의 공백을 제거하는 메소드. 반각/전각에 해당하는 공백 제거
     * 0x20(반각), 0x3000(전각)
     * ex)
     * ltrim("    abcdef") ==> "abcdef"
     *
     * @param str 대상 문자열
     * @return trimed string with white space removed from the front.
     */
    public static String ltrim( String str )
    {
        int len = str.length();
        int idx = 0;
        while( (idx < len) && (str.charAt(idx) == 0x20 || str.charAt(idx) == 0x3000) )
        {
            idx++;
        }
        return str.substring( idx, len );
    }

    /**
     * 문자열 우측의 공백을 제거하는 메소드. 반각/전각에 해당하는 공백 제거
     * 0x20(반각), 0x3000(전각)
     * ex)
     * rtrim("abcdef     ") ==> "abcdef"
     *
     * @param str 대상 문자열
     * @return trimed string with white space removed from the end.
     */
    public static String rtrim( String str )
    {
        int len = str.length();
        while( (0 < len) && (str.charAt(len - 1) == 0x20 || str.charAt(len - 1) == 0x3000) )
        {
            len--;
        }
        return str.substring( 0, len );
    }

    public static String trim( String str )
    {
        return rtrim(ltrim(str));
    }

    
    /**
     * substring을 byte단위로 처리한다.
     * @param source
     * @param fromIdx
     * @param toIdx
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String substringByte( String source, int fromIdx, int toIdx ) throws UnsupportedEncodingException
    {
        EncodingEnum ee = EncodingEnum.getEnum( Constable.FILE_ENCODING );
        return substringByte( source, fromIdx, toIdx, ee );
    }
    
    /**
     * substring을 byte단위로 처리한다.
     * @param source
     * @param fromIdx
     * @param toIdx
     * @return
     * @throws UnsupportedEncodingException 
     */
    public static String substringByte( String source, int fromIdx, int toIdx, EncodingEnum ee ) throws UnsupportedEncodingException
    {
        int    begin = 0;
        int    end;
        byte[] src   = source.getBytes( ee.getString() );

        if( src.length >= toIdx )
            end = toIdx;
        else
            end = src.length;

        if( fromIdx >= 0 )
            begin = fromIdx;
    
        byte[] temp = new byte[ end - begin ];
        System.arraycopy( src, begin, temp, 0, end );

        return new String( temp, ee.getString() );
    }
    
    /**
     * 한글이 깨지지 않도록 substring 해준다.
     * @param str
     * @param byteLength
     * @return
     */
    public static String substringForEuckr( String str, int byteLength )
    {
        // String 을 byte 길이 만큼 자르기.
        int retLength = 0;
        int tempSize = 0;
        int asc;
        if( isBlank(str) || "null".equals(str) )
        {
            return str;
        }

        int length = str.length();

        for( int i = 1; i <= length; i++ )
        {
            asc = str.charAt( i - 1 );
            if( asc > 127 )
            {
                if( byteLength >= tempSize + 2 )
                {
                    tempSize += 2;
                    retLength++;
                }
                else
                {
                    return str.substring( 0, retLength );
                }
            }
            else
            {
                if( byteLength > tempSize )
                {
                    tempSize++;
                    retLength++;
                }
            }
        }

        return str.substring( 0, retLength );
    }

    /**
     * String으로 printStackTrace() Exception Message를 리턴한다.
     * @param e Exception
     * @return Exception메시지가 String으로 변환된 문자열
     */
    public static String printStackTraceString( Exception e )
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream           ps   = new PrintStream( baos );
        e.printStackTrace( ps );
        return baos.toString();
    }
    
    /*
    public static byte[] copyByte( byte[] src, int start, int length )
    {
        int len = 0;
        
        //System.out.println( "[" + src.length + "] : [" + start + "], [" + length + "]" );
        if( src.length - start >= length )
            len = length;
        else
            len = src.length - start;
        
        byte[] temp = new byte[ len ];
        System.arraycopy( src, start, temp, 0, len );
        
        return temp;
    }
    */
    
    /**
     * byte 길이를 리턴한다.
     * @param str
     * @return
     * @throws UnsupportedEncodingException
     */
    public static int getByteLength( String str ) throws UnsupportedEncodingException
    {
        EncodingEnum ee = EncodingEnum.getEnum( Constable.FILE_ENCODING );
        return getByteLength( str, ee );
    }
    
    /**
     * byte 길이를 리턴한다.
     *
     * @param str  String target
     * @return int bytelength
     * @throws UnsupportedEncodingException 
     */
    public static int getByteLength( String str, EncodingEnum ee ) throws UnsupportedEncodingException
    {
        if( null == str || str.length() == 0 )
            return 0;

        byte[] byteArray = str.getBytes( ee.getString() );
        
        return byteArray.length;
    }
    
    /**
     * 
     * @param src
     * @param length
     * @param pad
     * @return
     * @throws MessageException
     */
    public static String lpad( String src, int length, char pad ) throws MessageException
    {
        try
        {
            return pad( PaddingEnum.LPAD, src, length, pad );
        }
        catch( UnsupportedEncodingException e )
        {
            throw new MessageException( e );
        }
    }
    
    /**
     * 
     * @param src
     * @param length
     * @param pad
     * @return
     * @throws MessageException
     */
    public static String rpad( String src, int length, char pad ) throws MessageException
    {
        try
        {
            return pad( PaddingEnum.RPAD, src, length, pad );
        }
        catch( UnsupportedEncodingException e )
        {
            throw new MessageException( e );
        }
    }
    
    /**
     * 
     * @param pe
     * @param src
     * @param length
     * @param pad
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String pad( PaddingEnum pe, String src, int length, char pad ) throws UnsupportedEncodingException
    {
    	StringBuilder sb = new StringBuilder();
    	if( isBlank(src) )
    	{
    		for( int i=0; i < length; ++i )
			{
    			sb.append( pad );
			}
    	}
    	else
    	{
    		int byteLen = getByteLength( src );
    		if( byteLen > length )
    		{
    			sb.append( substringByte(src, 0, length) );
    		}
    		else if( byteLen == length )
    		{
    			sb.append( src );
    		}
    		else
    		{
    			if( pe == PaddingEnum.RPAD )
    				sb.append( src );
    			int padLen = length - byteLen;
    			for( int i=0; i < padLen; ++i )
    			{
    				sb.append( pad );
    			}
    			if( pe == PaddingEnum.LPAD )
    				sb.append( src );
    		}
    	}
    	
    	return sb.toString();
    }
    
    /**
     * 문자열을 좌측 정렬한다. 이때 문자열뒤에 줄임표는 넣지 않는다.<br>
     * @param source 원본 문자열
     * @param length 정렬이 이루어질 길이
     * @return 정렬이 이루어진 문자열
     *
     * <code>
     * String source = "ABCDEFG";<br>
     * String result = StringUtil.alignLeft(source, 10);<br>
     * </code> <code>result</code>는 <code>"ABCDEFG   "</code> 을 가지게 된다.
     * @throws UnsupportedEncodingException 
     */
    public static String alignLeft( String source, int length ) throws UnsupportedEncodingException
    {
        return alignLeft( source, length, false );
    }

    /**
     * 문자열을 좌측부터 원하는만큼 자른다.(원한다면 끝에 ...을 붙인다.)<br>
     * @param source 원본 문자열
     * @param length 정렬이 이루어질 길이
     * @param isEllipsis 마지막에 줄임표("...")의 여부
     * @return 정렬이 이루어진 문자열
     *
     * <code>
     * String source = "ABCDEFG";<br>
     * String result = StringUtil.alignLeft(source, 5, true);<br>
     * </code> <code>result</code>는 <code>"AB..."</code> 을 가지게 된다.
     * @throws UnsupportedEncodingException 
     */
    public static String alignLeft( String source, int length, boolean isEllipsis ) throws UnsupportedEncodingException
    {
        int srcLength = source.getBytes().length;

        if( srcLength <= length )
        {
            StringBuilder temp = new StringBuilder( source );
            for( int i = 0, n=(length - srcLength); i < n; ++i )
            {
                temp.append( CharacterEnum.SPACE.getString() );
            }

            return temp.toString();
        }
        else
        {
            if( isEllipsis )
            {
                StringBuilder temp = new StringBuilder( length );
                temp.append(source, 0, length - 3);
                temp.append( "..." );
                return temp.toString();
            }
            else
            {
                return substringByte( source, 0, length );
            }
        }
    }

    /**
     * 문자열을 우측 정렬한다. 이때 문자열뒤에 줄임표는 넣지 않는다.<br>
     * @param source 원본 문자열
     * @param length 정렬이 이루어질 길이
     * @return 정렬이 이루어진 문자열
     *
     * <code>
     * String source = "ABCDEFG";<br>
     * String result = StringUtil.alignRight(source, 10);<br>
     * </code> <code>result</code>는 <code>"   ABCDEFG"</code> 을 가지게 된다.
     * @throws UnsupportedEncodingException 
     */
    public static String alignRight( String source, int length ) throws UnsupportedEncodingException
    {
        return alignRight( source, length, false );
    }

    /**
     * 문자열을 우측 정렬한다.(원한다면 끝에 ...을 붙인다.)<br>
     * @param source 원본 문자열
     * @param length 정렬이 이루어질 길이
     * @param isEllipsis 마지막에 줄임표("...")의 여부
     * @return 정렬이 이루어진 문자열
     *
     * <code>
     * String source = "ABCDEFG";<br>
     * String result = StringUtil.alignRight(source, 5, true);<br>
     * </code> <code>result</code>는 <code>"AB..."</code> 을 가지게 된다.
     * @throws UnsupportedEncodingException 
     */
    public static String alignRight( String source, int length, boolean isEllipsis ) throws UnsupportedEncodingException
    {
        int srcLength = source.getBytes().length;

        if( srcLength <= length )
        {
            StringBuilder temp = new StringBuilder( length );
            for( int i = 0, n=(length-srcLength); i < n; i++ )
            {
                temp.append( CharacterEnum.SPACE.getString() );
            }

            temp.append( source );
            return temp.toString();
        }
        else
        {
            if( isEllipsis )
            {
                StringBuilder temp = new StringBuilder( length );
                temp.append( source.substring(0, length - 3) );
                temp.append( "..." );
                return temp.toString();
            }
            else
            {
                return substringByte( source, 0, length );
            }
        }
    }

    /**
     * 문자열을 중앙 정렬한다. 이때 문자열뒤에 줄임표는 넣지 않는다. 만약 공백이 홀수로 남는다면 오른쪽에 들어 간다.<br>
     * @param source 원본 문자열
     * @param length 정렬이 이루어질 길이
     * @return 정렬이 이루어진 문자열
     *
     * <code>
     * String source = "ABCDEFG";<br>
     * String result = StringUtil.alignCenter(source, 10);<br>
     * </code> <code>result</code>는 <code>" ABCDEFG "</code> 을 가지게 된다.
     * @throws UnsupportedEncodingException 
     */
    public static String alignCenter( String source, int length ) throws UnsupportedEncodingException
    {
        return alignCenter( source, length, CharacterEnum.SPACE.getString(), false );
    }

    public static String alignCenter( String source, int length, String padding ) throws UnsupportedEncodingException
    {
        return alignCenter( source, length, padding, false );
    }

    /**
     * 문자열을 중앙 정렬한다. 만약 공백이 홀수로 남는다면 오른쪽에 들어 간다.<br>
     * @param source 원본 문자열
     * @param length 정렬이 이루어질 길이
     * @param isEllipsis 마지막에 줄임표("...")의 여부
     * @return 정렬이 이루어진 문자열
     *
     * <code>
     * String source = "ABCDEFG";<br>
     * String result = StringUtil.alignCenter(source, 5,true);<br>
     * </code> <code>result</code>는 <code>"AB..."</code> 을 가지게 된다.
     * @throws UnsupportedEncodingException 
     */
    public static String alignCenter( String source, int length, String padding, boolean isEllipsis ) throws UnsupportedEncodingException
    {
        int srcLength = source.getBytes().length;

        if( srcLength <= length )
        //if( source.length() <= length )
        {
            StringBuilder temp = new StringBuilder( length );
            int leftMargin = (length - srcLength) / 2;
            //int leftMargin = (int) (length - source.length()) / 2;

            int rightMargin;
            if( (leftMargin * 2) == (length - srcLength) )
            //if( (leftMargin * 2) == (length - source.length()) )
            {
                rightMargin = leftMargin;
            }
            else
            {
                rightMargin = leftMargin + 1;
            }

            for( int i = 0; i < leftMargin; i++ )
            {
                temp.append( padding );
            }

            temp.append( source );

            for( int i = 0; i < rightMargin; i++ )
            {
                temp.append( padding );
            }

            return temp.toString();
        }
        else
        {
            if( isEllipsis )
            {

                StringBuffer temp = new StringBuffer( length );
                temp.append( source.substring(0, length - 3) );
                temp.append( "..." );
                return temp.toString();
            }
            else
            {
                return substringByte( source, 0, length );
                //return source.substring( 0, length );
            }
        }
    }
    
    /**
     * byte array를 byte단위 문자열로 리턴하여 디버깅 할 수 있도록 한다.
     * @param array
     * @return
     * @throws MessageException
     */
    public static String HexDump( byte[] array ) throws MessageException
    {
        return HexDump( array, 0, array.length );
    }
    /**
     * 
     * @param array
     * @return
     * @throws IOException 
     */
    public static String HexDump( byte[] array, int offset, int length ) throws MessageException
    {
        final int HEXA_WIDTH = 16;
        final String DIVIDER   = "|---------------------------------------------------------------------------------|\n";
        final String GRADATION = "|   OFFSET   | 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F | 0123456789ABCDEF |\n";
        
        StringBuilder builder = new StringBuilder();
        builder.append( "\n" + DIVIDER   );
        builder.append( GRADATION );
        builder.append( DIVIDER   );

        for( int rowOffset = offset, n=offset+length; rowOffset < n ; rowOffset += HEXA_WIDTH )
        {
            try { makeHexDumpRecord( builder, array, rowOffset ); } catch (UnsupportedEncodingException e) { throw new MessageException(e); }
        }
        
        builder.append( DIVIDER );

        return builder.toString();
    }
    
    /**
     * HexDump 각 행 데이터를 생성한다.
     * @param sb
     * @param array
     * @param offset
     * @throws IOException
     */
    public static void makeHexDumpRecord( StringBuilder sb, byte[] array, int offset ) throws UnsupportedEncodingException
    {
        final int    HEXA_WIDTH   = 16;
        final String HEXA_FORMAT  = "| %010d | %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X | %16s |\n";
        final String BLANK_FORMAT = "   ";
              
        List<Object> args = new ArrayList<Object>();
        args.add( offset );
        
        //
        if( offset + HEXA_WIDTH <= array.length )
        {
            for( int i=0; i < HEXA_WIDTH; ++i )
            {
                args.add( array[offset + i] );
            }
            args.add( new String(array, offset, HEXA_WIDTH, EncodingEnum.ASCII.getString()).replaceAll("[^\\x20-\\x7E]", CharacterEnum.PERIOD.getString()) );
            sb.append( String.format(HEXA_FORMAT, args.toArray()) );
        }
        else
        {
            int asciiWidth = array.length - offset;

            String tmp_format = "| %010d | ";
            for( int i=0; i < HEXA_WIDTH; ++i )
            {
                if( offset + i < array.length )
                {
                    tmp_format += "%02X ";
                    args.add( array[offset + i] );
                }
                else
                    tmp_format += BLANK_FORMAT;
            }
            tmp_format += "| %s%" + (HEXA_WIDTH-asciiWidth) + "s |\n";
            args.add( new String(array, offset, asciiWidth, EncodingEnum.ASCII.getString()).replaceAll("[^\\x20-\\x7E]", CharacterEnum.PERIOD.getString()) );
            args.add( CharacterEnum.BLANK.getString() );
            sb.append( String.format(tmp_format, args.toArray()) );
        }
   }

    /**
     * 
     * @param array
     * @return
     * @throws MessageException
     */
    public static String HexDump2( byte[] array, int offset, int length ) throws MessageException
    {
        final int HEXA_WIDTH = 16;
        final String DIVIDER   = "|---------------------------------------------------------------------------------|\n";
        final String GRADATION = "|   OFFSET   | 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F | 0123456789ABCDEF |\n";
        
        StringBuilder builder = new StringBuilder();
        builder.append( DIVIDER   );
        builder.append( GRADATION );
        builder.append( DIVIDER   );

        for( int rowOffset = offset, n=offset+length; rowOffset < n ; rowOffset += HEXA_WIDTH )
        {
            builder.append( String.format("| %010d | ", rowOffset) );
            
            for( int index = 0; index < HEXA_WIDTH; ++index )
            {
                if( rowOffset + index < array.length )
                {
                    builder.append( String.format("%02X ", array[rowOffset + index]) );
                } 
                else
                {
                    builder.append( StringUtil.rpad(CharacterEnum.SPACE.getString(), 3, CharacterEnum.SPACE.getChar()) );
                }
            }

            if( rowOffset < array.length )
            {
                int asciiWidth = Math.min( HEXA_WIDTH, array.length - rowOffset );
                builder.append(String.format("%s ", CharacterEnum.VERTICAL_BAR.getString()));
                try
                {
                    builder.append( new String(array, rowOffset, asciiWidth, EncodingEnum.ASCII.getString()).replaceAll("[^\\x20-\\x7E]", CharacterEnum.PERIOD.getString()) );
                    //builder.append(new String(array, rowOffset, asciiWidth, "UTF-8").replaceAll("\r\n", " ").replaceAll("\n", " "));
                }
                catch (UnsupportedEncodingException ignored) {}
            }
            if( rowOffset + HEXA_WIDTH <= array.length )
            {
                builder.append( CharacterEnum.SPACE.getString() + CharacterEnum.VERTICAL_BAR.getString() );
            }
            else
            {
                builder.append( StringUtil.lpad(CharacterEnum.SPACE.getString() + CharacterEnum.VERTICAL_BAR.getString(), (rowOffset + HEXA_WIDTH - array.length)+2, CharacterEnum.TILDE.getChar()) );
            }

            builder.append( CharacterEnum.LINE_FEED.getString() );
        }
        
        builder.append( DIVIDER );

        return builder.toString();
    }
    
    /**
     * 문자열에서 숫자만 남겨두고 제거한다.
     * @param str 원본 문자열
     * @return 숫자만 남은 수정된 문자열
     */
    public static String removeExceptDigit( String str )
    {
        return str.replaceAll( "\\D", "" );
    }
    /**
     * 숫자만으로 이루어진 문자열에서 앞에 '0'을 제거한다.
     * ex) "000012340000" -> "12340000"
     * @param str 숫자로 이루어진 문자열
     * @return 앞에 '0'을 제거한 문자열
     */
    public static String removeFirstZero( String str )
    {
        if( isNumber(str) )
            return str;

        return str.replaceFirst( "^([-+0]?[0]+)", "" );
    }

    public static String removeSpace( String str )
    {
        return removeChar( str, CharacterEnum.SPACE.getChar() );
    }
    public static String removeZero( String str )
    {
        return removeChar( str, CharacterEnum.ZERO.getChar() );
    }
    /**
     * 문자열에서 특정 문자를 제거하여 리턴한다.
     * @param str 원본 문자열
     * @param c   제거하고자 하는 문자.
     * @return 특정 문자가 제거된 문자열.
     */
    public static String removeChar( String str, char c )
    {
        StringBuilder sb = new StringBuilder();

        for( int i=0, n=str.length(); i < n; ++i )
        {
            char cur = str.charAt( i );
            if( cur != c )
            {
                sb.append( cur );
            }
        }

        return sb.toString();
    }
    
    /**
     * 문자+숫자 조합의 Random 문자열을 생성하여 리턴한다.
     * @param length 생성할 문자열의 길이
     * @return 문자+숫자 조합의 Randowm 문자열
     */
    public static String getRandomAlphaNum( int length )
    {
        SecureRandom sr = new SecureRandom();
        StringBuilder sb = new StringBuilder( length );
        for( int i = 0; i < length; i++ )
            sb.append( JWTUtil.BOUNDARY.charAt(sr.nextInt(JWTUtil.BOUNDARY.length())) );
        
        return sb.toString();
    }
    
    /**
     * 문자 padding
     * @param v 패딩전 문자열
     * @param length
     * @param padding 삽입문자
     * @param aligntype L, R 좌우측 패딩 선택값
     * @return v에 패딩을 추가한 문자열
     */
    public static String setPadding(String v, int length, char padding, char aligntype) {
		String value = v == null ? "null" : v;
		if (value.getBytes().length >= length) {
			return value;
		} else {
			StringBuffer sbValue = new StringBuffer(length);
			int nValueLength = value.getBytes().length;
			int nCenter = length / 2;
			int nCount = 0;
			int nMaxCount = 0;
			if (aligntype == 'L') {
				sbValue.append(value);
				nCount = nValueLength;
			} else {
				if (aligntype == 'R') {
					for (nMaxCount = length - nValueLength; nCount < nMaxCount; ++nCount) {
						sbValue.append(padding);
					}

					sbValue.append(value);
					nCount += nValueLength;
				} else {
					for (nMaxCount = nCenter - nValueLength / 2; nCount < nMaxCount; ++nCount) {
						sbValue.append(padding);
					}

					sbValue.append(value);
					nCount += nValueLength;
				}
			}

			while (nCount < length) {
				sbValue.append(padding);
				++nCount;
			}

			return sbValue.toString();
		}
	}
    
    /**
     * 숫자로 이루어진 문자열을 원하는 길이만큼 패딩하여 리턴한다.
     * @param numberStr 숫자값의 문자열
     * @param total_length 패딩을 포함한 전체 길이
     * @param real_length 패딩을 포함한 소수점 길이
     * @param padding 패딩하고자하는 character
     * @return 패딩된 숫자 문자열
     * @throws MessageException
     */
    public static String getNumberValueByLength( String numberStr, int total_length, int real_length, char padding ) throws MessageException
    {
        if( !StringUtil.isBlank(numberStr) && !StringUtil.isNumeric(numberStr) )
            return numberStr;
        
        // 실수
        if( real_length > 0 )
        {
            int periodPos = numberStr.indexOf( CharacterEnum.PERIOD.getChar() );
            if( periodPos > 0 )
            {
                String naturalPart = numberStr.substring( 0, periodPos );
                String realPart = numberStr.substring(periodPos + 1 );
                return String.format( "%s.%s", StringUtil.lpad(naturalPart, total_length - real_length - 1, padding), StringUtil.rpad(realPart, real_length, padding) );
            }
            else
            {
                return String.format( "%s.%s", StringUtil.lpad(numberStr, total_length - real_length - 1, padding), StringUtil.rpad(CharacterEnum.BLANK.getString(), real_length, padding) );
            }
        }
        // 정수
        else
        {
            int periodPos = numberStr.indexOf( CharacterEnum.PERIOD.getChar() );
            if( periodPos > 0 )
            {
                String naturalPart = numberStr.substring( 0, periodPos );
                return StringUtil.lpad( naturalPart, total_length, padding );
            }
            else
            {
                return StringUtil.lpad( numberStr, total_length, padding );
            }
        }
    }
    
    public static String randomHangulName(int len )
    {
        Random rand = new SecureRandom();
        
        String name = HANGUL_SUNG[rand.nextInt(HANGUL_SUNG.length)];
        
        if( len < 2 )
            return "";
        
        for( int i=0, n=len-name.length(); i < n; ++i )
        {
            name += assembleHangul( rand.nextInt(HANGUL_CHOSUNG.length), rand.nextInt(HANGUL_JUNGSUNG.length), rand.nextInt(HANGUL_JONGSUNG.length) );
        }
        
        return name;
    }
    
    public static String randomHangul( int len )
    {
        Random rand = new SecureRandom();
        
        String name = "";
        for( int i=0; i < len; ++i )
        {
            name += assembleHangul( rand.nextInt(HANGUL_CHOSUNG.length), rand.nextInt(HANGUL_JUNGSUNG.length), rand.nextInt(HANGUL_JONGSUNG.length) );
        }
        
        return name;
    }
    
    public static String assembleHangul( int first, int middle, int last )
    {
        char sum = (char)((first * HANGUL_JUNGSUNG.length + middle) * HANGUL_JONGSUNG.length + last + HANGUL_SYLLABLES_BEGIN);
        return String.valueOf(sum);
    }
}
