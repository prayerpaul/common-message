/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.utility
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author prayerpaul
 *
 */
/**
 * 날짜관련 함수들을 모아놓은 클래스
 * DateUtil
 * @author PAUL LEE
 */
public class DateUtil
{
    public enum DATE_STND { DATE, MONTH, YEAR }

    private static final String DEFAULT_DATE_FORMAT = "yyyyMMdd";
    private static final String DEFAULT_TIME_FORMAT = "HHmmss";
    private static final String DEFAULT_MILISEC_FORMAT = "yyyyMMddHHmmss.SSS";

    private DateUtil() {}

    /**
     * 파라미터로 입력된 날짜 문자열 포맷의 형태로 현재 시간에 대한 문자열을 리턴한다.
     * @param format 'yyyy-MM-dd HH:mm:ss.SSS', 'yyyyMMdd', 'HHmmss', ...
     * @return 입력받은 format 형태의 문자열
     */
    public static synchronized String getDateTime( String format )
    {
        SimpleDateFormat formatter = new SimpleDateFormat( format, java.util.Locale.KOREA );
        return formatter.format( new Date() );
    }

    /**
     * 현재의 날짜를 리턴한다.
     * @return yyyyMMdd 포맷의 날짜 문자열
     */
    public static String getDate()
    {
        return getDateTime( DEFAULT_DATE_FORMAT );
    }

    /**
     * 현재 날짜의 시분초를 리턴한다.
     * @return HHmmss 포맷의 날짜 문자열
     */
    public static String getTime()
    {
        return getDateTime( DEFAULT_TIME_FORMAT );
    }
    
    /**
     * 현재의 날짜의 년월일시분초를 리턴한다.
     * @return yyyyMMddHHmmss 포맷의 날짜 문자열
     */
    public static String getDateTime()
    {
        return getDateTime( DEFAULT_DATE_FORMAT + DEFAULT_TIME_FORMAT );
    }
    
    /**
     * 현재의 날짜의 년월일시분초+밀리세컨드를 리턴한다.
     * @return yyyyMMddHHmmssSSS 포맷의 날짜 문자열
     */
    public static String getDateTimeMilisec()
    {
        return getDateTime( DEFAULT_MILISEC_FORMAT );
    }

    /**
     * 두 날짜 사이의 일수를 리턴한다.
     * @param fromDt 시작일자(yyyyMMdd)
     * @param toDt   종료일자(yyyyMMdd)
     * @return 두 날짜 사이의 일수
     * @throws Exception
     */
    public static int getDayCount( String fromDt, String toDt ) throws Exception
    {
        SimpleDateFormat sdf = new SimpleDateFormat( DEFAULT_DATE_FORMAT );

        return (int) ((sdf.parse(toDt).getTime() - sdf.parse(fromDt).getTime())/1000 / 60 / 60 / 24);
    }

    /**
     * 오늘을 기준으로 days 만큼 가감한 날짜의 "yyyyMMdd"포맷의 문자열을 리턴한다.
     * @param days 가감할 숫자
     * @return yyyyMMdd 형식의 날짜 문자열
     * @throws ParseException
     */
    public static String getCountDate( int days ) throws ParseException
    {
        return getCountDate( getDate(), days, DATE_STND.DATE );
    }

    /**
     * 기준 일자에서 days 만큼 가감한 날짜의 "yyyyMMdd"포맷의 문자열을 리턴한다.
     * @param stndDt 기준 일자(yyyyMMdd)
     * @param days 가감할 숫자
     * @return yyyyMMdd 형식의 날짜 문자열
     * @throws ParseException
     */
    public static String getCountDate( String stndDt, int days ) throws ParseException
    {
        return getCountDate( stndDt, days, DATE_STND.DATE );
    }

    /**
     * 오늘을 기준으로 months 만큼 가감한 일자를 리턴한다.
     * @param months 가감할 월수
     * @return yyyyMMdd 형식의 날짜 문자열
     * @throws ParseException
     */
    public static String getCountMonth( int months ) throws ParseException
    {
        return getCountDate( getDate(), months, DATE_STND.MONTH );
    }
    /**
     * 기준 일자에 months 만큼 가감한 일자를 리턴한다.
     * @param stndDt 기준일자
     * @param months 가감할 월수
     * @return yyyyMMdd 형식의 날짜 문자열
     * @throws ParseException
     */
    public static String getCountMonth( String stndDt, int months ) throws ParseException
    {
        return getCountDate( stndDt, months, DATE_STND.MONTH );
    }

    /**
     * 당일에 years 만큼 가감한 일자를 리턴한다.
     * @param years 가감할 년수
     * @return yyyyMMdd 형식의 날짜 문자열
     * @throws ParseException
     */
    public static String getCountYear( int years ) throws ParseException
    {
        return getCountDate( getDate(), years, DATE_STND.YEAR );
    }
    /**
     * 기준 일자에 years 만큼 가감한 일자를 리턴한다.
     * @param stndDt 기준일자
     * @param years 가감할 년수
     * @return yyyyMMdd 형식의 날짜 문자열
     * @throws ParseException
     */
    public static String getCountYear( String stndDt, int years ) throws ParseException
    {
        return getCountDate( stndDt, years, DATE_STND.YEAR );
    }

    /**
     * 기준일자의 tp에 해당하는 count를 가감하여 날짜 문자열을 리턴한다.
     * @param stndDt 기준일자
     * @param count 가감할 정수형 숫자
     * @return yyyyMMdd 형식의 날짜 문자열
     * @throws ParseException
     */
    public static String getCountDate( String stndDt, int count, DATE_STND stnd ) throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat( DEFAULT_DATE_FORMAT );
        Calendar c = Calendar.getInstance();
        Date dt = null;

        dt = sdf.parse( stndDt );
        c.setTime( dt );
        switch( stnd )
        {
        case DATE:
            c.add( Calendar.DAY_OF_YEAR, count );
            break;
        case MONTH:
            c.add( Calendar.MONTH, count );
            break;
        case YEAR:
            c.add( Calendar.YEAR, count );
            break;
        default:
            throw new ParseException( "Unknown DATE_STND", stnd.ordinal() );
        }

        return sdf.format( c.getTime() );
    }

    /**
     * 윤년인지 검증한다.
     * @param date
     * @return
     */
    public static boolean isLeapYear( String date )
    {
        if( date.length() != 4 && date.length() != 6 && date.length() != 8 )
            return false;

        String year = date.substring( 0, 4 );
        return isLeapYear( Integer.parseInt(year) );
    }
    public static boolean isLeapYear( int year )
    {
        if( ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) )
            return true;

        return false;
    }

    public static String getDateByString ( Date date )
    {
        return getDateByString( date, DEFAULT_DATE_FORMAT );
    }
    public static synchronized String getDateByString( Date date, String format )
    {
        SimpleDateFormat sdf = new SimpleDateFormat( format );
        return sdf.format( date );
    }
    
    /**
     * 
     * @param date
     * @param oldFormat
     * @param newFormat
     * @return
     * @throws ParseException
     */
    public static String changeDateFormat( String date, String oldFormat, String newFormat ) throws ParseException
    {
        SimpleDateFormat oldSdf = new SimpleDateFormat( oldFormat );
        SimpleDateFormat newSdf = new SimpleDateFormat( newFormat );

        Date dt = oldSdf.parse( date );

        return newSdf.format( dt );
    }
}
