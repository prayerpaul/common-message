/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.utility
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.enums.CharacterEnum;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author prayerpaul
 *
 */
public class ByteUtil
{
    private static final Logger logger = LoggerFactory.getLogger( ByteUtil.class );
    /**
     * 생성자, 외부에서 객체를 인스턴스화 할 수 없도록 설정
     */
    private ByteUtil() {}
    
    /**
     * byte array 를 복사한다.
     * @param in 소스 byte array
     * @param out 대상 byte array
     */
    public static void assignBytes( byte[] in, byte[] out )
    {
        int len = in.length;

        if( len <= out.length )
        {
            System.arraycopy( in, 0, out, 0, len );
            for( int i = len; i < out.length; i++ )
                out[i] = 32;
        }
        else
        {
            System.arraycopy( in, 0, out, 0, out.length );
        }
    }
   
    /**
     * byte array 를 원하는 위치에서 길이만큼 복사해서 리턴한다.
     * @param src 원본 byte array
     * @param start 시작위치
     * @param length 복사할 byte 길이
     * @return source byte array 에서 복사된 새로운 byte array
     */
    public static byte[] copyByte( byte[] src, int start, int length )
    {
        int len = Math.min(src.length - start, length);
    
        // System.out.println( "[" + src.length + "] : [" + start + "], [" + length + "]" );
    
        logger.trace( "start[{}], len[{}], length[{}]", start, len, length );
        byte[] temp = new byte[ len ];
        System.arraycopy( src, start, temp, 0, len );
        
        return temp;
    }
    
    /**
     * left padding
     * @param buffer padding 대상인 byte array
     * @param ch padding 처리할 Character 의 byte
     * @param totalLength padding 처리후의 총 길이
     * @return padding 처리된 새로운 byte array
     */
    public static byte[] lpadBytes( byte[] buffer, byte ch, int totalLength )
    {
        if( totalLength <= buffer.length )
            return ByteUtil.substringByte( buffer, 0, totalLength );
        
        return concatByteArrays( paddingByte(ch, totalLength-buffer.length), buffer );
    }
    
    /**
     * right padding
     * @param buffer padding 대상인 byte array
     * @param ch padding 처리할 Character 의 byte
     * @param totalLength padding 처리후의 총 길이
     * @return padding 처리된 새로운 byte array
     */
    public static byte[] rpadBytes( byte[] buffer, byte ch, int totalLength )
    {
        if( totalLength <= buffer.length )
            return ByteUtil.substringByte( buffer, 0, totalLength );
        
        return concatByteArrays( buffer, paddingByte(ch, totalLength-buffer.length) );
    }
    
    /**
     * 주어진 길이만큼 Character 를 채운 byte array 를 리턴한다.
     * @param ch padding 처리할 Character 의 byte
     * @param paddingLength padding 처리할 길이
     * @return ch로 전체 채워진 byte array
     */
    public static byte[] paddingByte( byte ch, int paddingLength )
    {
        byte[] padding = new byte[paddingLength];
        Arrays.fill( padding, ch );

        return padding;
    }
    
    /**
     * 여러개의 byte array 연결해서 새로운 byte array 를 리턴한다.
     * @param buffers 연결할 대상 byte array 들
     * @return
     *  성공 : 연결할 대상들을 모두 연결한 새로운 byte array
     *  실패 : null
     */
    public static byte[] concatByteArrays( byte[]... buffers )
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        try
        {
            for( byte[] bytes : buffers )
            {
                baos.write(bytes);
            }
        }
        catch( IOException e )
        {
            return null;
        }
        
        return baos.toByteArray();
    }
    
    /**
     * 공백문자를 제거한 byte array 를 리턴한다.
     * @param buff 공백을 제거할 대상 byte array
     * @return 공백이 제거된 byte array
     */
    public static byte[] trimByte( byte[] buff )
    {
        if( buff == null || buff.length < 1 )
        {
            return buff;
        }
        int nStartIndex = -1;
        int nEndIndex   = -1;
        int i           = 0;
        
        while( i < buff.length )
        {
            if( buff[i] != 32 && buff[i] != 10 && buff[i] != 13 )
            {
                nStartIndex = i;
                break;
            }
            ++i;
        }
        
        if( nStartIndex == -1 )
        {
            return new byte[0];
        }
        i = buff.length - 1;
        
        while( i >= 0 )
        {
            if( buff[i] != 32 && buff[i] != 13 && buff[i] != 10 )
            {
                nEndIndex = i + 1;
                break;
            }
            --i;
        }
        byte[] result = new byte[nEndIndex - nStartIndex];
        System.arraycopy( buff, nStartIndex, result, 0, result.length );
        return result;
    }
    
    /**
     * Byte Array 에서 새로운 문자열로 대체한다.
     * @param src 변환대상 byte array
     * @param newByte  대체되는 Byte
     * @param oldBytes 대체대상이 되는 Bytes
     * @return oldBytes 들이 newByte 로 변경된 byte array
     */
    public static byte[] replaceBytes( byte[] src, byte newByte, byte... oldBytes )
    {
    	for( int i=0, n=src.length; i < n; ++i )
    	{
            for( byte oldByte : oldBytes )
            {
                if( src[i] == oldByte )
                    src[i] = newByte;
            }
    	}
    	
    	return src;
    }
    
    /**
     * source byte array를 target byte array에 원하는 길이 만큼 replace 한다.
     * @param src 교체할  byte array
     * @param dst 변경 대상 byte array
     * @param offset 시작 위치
     * @return src byte array 값이 적용된 dst byte array
     */
    public static byte[] replaceByteArray( byte[] src, byte[] dst, int offset )
    {
        System.arraycopy( src, 0, dst, 0, src.length );
        return dst;
    }
    
    public static byte[] substringByte( byte[] array, int start )
    {
        return substringByte( array, start, array.length );
    }
    
    /**
     * byte 단위 substring
     * @param array substring 대상 byte array
     * @param start 시작 위치
     * @param end   종료 위치
     * @return byte 단위로 substring 된 byte array
     */
    public static byte[] substringByte( byte[] array, int start, int end )
    {
        if( end <= start )
            return null;
        
        int length = (end - start);
        
        byte[] newArray = new byte[ length ];
        System.arraycopy( array, start, newArray, 0, length );
        return newArray;
    }
    
    
    public static byte[] getStringByteByLength( byte[] data, int length, byte padding )
    {
        if( length == data.length )
            return data;
        else if( length > data.length )
            return rpadBytes(data, padding, length );
        else
            return substringByte( data, 0, length );
    }
    
    public static byte[] getNumericByteByLength( byte[] data, int length, int pointLength, byte padding )
    {
        int naturalLength, realLength, index = 0;
        
        if( length <= pointLength )
            return null;
            
        // Double
        if( pointLength > 0 )
        {
            naturalLength = length - pointLength -1;
            index = ByteUtil.indexOf( data, CharacterEnum.PERIOD.getByte() );
            
            if( index > 0 )
            {
                byte[] nPart = ByteUtil.lpadBytes( ByteUtil.substringByte(data, 0, index), padding, naturalLength );
                byte[] rPart = ByteUtil.rpadBytes( ByteUtil.substringByte(data, index), padding, pointLength+1 );
                return ByteUtil.concatByteArrays( nPart, rPart );
            }
            else
            {
                byte[] nPart = ByteUtil.lpadBytes( data, padding, naturalLength );
                byte[] period = CharacterEnum.PERIOD.getString().getBytes();
                byte[] rPart = ByteUtil.rpadBytes( CharacterEnum.ZERO.getString().getBytes(), padding, pointLength );
                return ByteUtil.concatByteArrays( nPart, period, rPart );
            }
        }
        // Long
        else
        {
            naturalLength = length;
            return ByteUtil.lpadBytes( data, padding, naturalLength );
        }
        
        
    }
    
    public static int indexOf( byte[] data, byte ch )
    {
        return indexOf( data, ch, 0 );
    }
    public static int indexOf( byte[] data, byte ch, int fromIndex )
    {
        int index = -1;
        
        for( int i=fromIndex, n=data.length; i < n; ++i )
        {
            if( data[i] == ch )
            {
                index = i;
                break;
            }
        }
        
        return index;
    }
}
