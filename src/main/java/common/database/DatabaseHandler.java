/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.database
 * FILE      : DatabaseHandler.java
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.database;

import common.utility.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseHandler
{
	  Logger logger = LoggerFactory.getLogger( DatabaseHandler.class );
	
	private SQLUnit           sql;
	private Connection conn;
	private PreparedStatement ptmt;
	private ResultSet rs;
	private int               fetchCnt;
	
	public DatabaseHandler( SQLUnit unit )
	{
		this.sql = unit;
	}
	
	/* Getter */
	public SQLUnit           getSQLUnit()           { return this.sql; }
	public Connection        getConnection()        { return this.conn; }
	public PreparedStatement getPreparedStatement() { return this.ptmt; }
	public ResultSet         getResultSet()         { return this.rs; }
	public int               getFetchCount()        { return this.fetchCnt; }
	
	/* Setter */
	public void setSQLUnit          ( SQLUnit           unit ) { this.sql      = unit; }
	public void setConnection       ( Connection        conn ) { this.conn     = conn; }
	public void setPreparedStatement( PreparedStatement ptmt ) { this.ptmt     = ptmt; }
	public void setResultSet        ( ResultSet         rs   ) { this.rs       = rs;   }
	public void setFetchCount       ( int               cnt  ) { this.fetchCnt = cnt;  }
	
	/**
	 * Check Connection
	 * @return true : Connection is not null, false : Connection is null
	 */
	public boolean checkConnection()
	{
		return null != conn;
	}
	/**
	 * Check PreparedStatement
	 * @return true : PreparedStatement is not null, false : PreparedStatement is null
	 */
	public boolean checkPreparedStatement()
	{
		return null != ptmt;
	}
	public void commit()
	{
		if( !checkConnection() )
			return;
		
		try
		{
			conn.commit();
		}
		catch( SQLException e )
		{
			logger.error( "SQLException occur : [{}]", StringUtil.printStackTraceString(e) );
		}
	}
	
	public void rollback()
	{
		if( !checkConnection() )
			return;
		
		try
		{
			conn.rollback();
		}
		catch( SQLException e )
		{
			logger.error( "SQLException occur : [{}]", StringUtil.printStackTraceString(e) );
		}
	}
	
	public PreparedStatement setupPreparedStatement( Map<String, String> params ) throws Exception
	{
		if( !checkConnection() )
			throw new SQLException( "connection is null" );
		
		
		List<String> bind = sql.getBindList();
		if( null == params && bind.size() > 0 )
			throw new SQLException( "params is null" );
		
		try
		{
			verifyInputParams( bind, params );
			
			logger.debug( sql.printQuery() );
			ptmt = conn.prepareStatement( sql.getBindQuery() );
			
			bindInputParams( bind, params );
		}
		catch( SQLException e )
		{
			logger.error( "SQLException occurs : [{}]", StringUtil.printStackTraceString(e) );
			throw e;
		}
		catch( Exception e )
		{
			logger.error( "Exception occurs : [{}]", StringUtil.printStackTraceString(e) );
			throw e;
		}
		
		return ptmt;
	}
	
	public List<Map<String, Object>> selectList( Map<String, String> params ) throws Exception
	{
		List<Map<String, Object>> rows = new ArrayList<>();
		
		try
		{
			setupPreparedStatement( params );
			
			rs = ptmt.executeQuery();
			rows = doResultSetToList( rs );
		}
		catch( Exception e )
		{
			logger.error( "Exception occurs : [{}]", StringUtil.printStackTraceString(e) );
			throw e;
		}
		finally
		{
			if( null != rs ) rs.close();
			if( null != ptmt ) ptmt.close();
		}
		
		return rows;
	}
	
	public Map<String, Object> selectRow( Map<String, String> params ) throws Exception
	{
		Map<String, Object> row = new HashMap<>();
		
		try
		{
			setupPreparedStatement( params );
			
			rs = ptmt.executeQuery();
			if( rs.next() )
				row = doResultSetToMap( rs );
		}
		catch( Exception e )
		{
			logger.error( "Exception occurs : [{}]", StringUtil.printStackTraceString(e) );
			throw e;
		}
		finally
		{
			if( null != rs ) rs.close();
			if( null != ptmt ) ptmt.close();
		}
		
		return row;
	}
	
	public List<Map<String, Object>> doResultSetToList( ResultSet rs ) throws SQLException
	{
		List<Map<String, Object>> rows = new ArrayList<>();
		while( rs.next() )
		{
			rows.add( doResultSetToMap(rs) );
		}
		return rows;
	}
	public Map<String, Object> doResultSetToMap( ResultSet rs ) throws SQLException
	{
		Map<String, Object> row = null;
		ResultSetMetaData rsmd = rs.getMetaData();
		int cols = rsmd.getColumnCount();
		
		if( cols > 0 )
		{
			row = new HashMap<>();
			for( int i=1, n=rsmd.getColumnCount(); i <= n; ++i )
			{
				row.put( rsmd.getColumnLabel(i).toUpperCase(), rs.getObject(i) );
			}
		}
		
		return row;
	}
	
	public List<Map<String, Object>> fetchSelect() throws SQLException
	{
		List<Map<String, Object>> rows = new ArrayList<>();
		
		int fetchCnt = 0;
		while( rs.next() )
		{
			Map<String, Object> row = doResultSetToMap( rs );
			if( null != row )
			{
				rows.add( row );
				fetchCnt++;
			}
			
			if( fetchCnt >= getFetchCount() )
				break;
		}
		
		return rows;
	}
	
	public boolean finishSelect()
	{
		return finishBatch();
	}
	
	/**
	 * 대량의 DML(INSERT/UPDATE/DELETE)을 처리하기 위해 PrepareStatement를 생성한다.
	 *
	 * @return true  : PrepareStatement 생성 완료
	 *         false : PrepareStatement 생성 오류
	 */
	public boolean startBatch()
	{
		if( !checkConnection() )
			return false;
		
		try
		{
			ptmt = conn.prepareStatement( sql.getBindQuery() );
			logger.debug( sql.printQuery() );
		}
		catch( SQLException e )
		{
			logger.error( "SQLException occurs : [{}]", StringUtil.printStackTraceString(e) );
			return false;
		}
		
		return true;
	}
	
	/**
	 * 대량의 DML(INSERT/UPDATE/DELETE)을 처리하기 위해 데이터를 추가한다.
	 * @param params 바인딩 변수에 해당하는 key, value Map
	 * @return
	 */
	public boolean addBatch( Map<String, String> params )
	{
		if( !checkPreparedStatement() )
			return false;
		
		List<String> bind = sql.getBindList();
		try
		{
			bindInputParams( bind, params );
		}
		catch( Exception e )
		{
			logger.error( "Exception occurs : [{}]", StringUtil.printStackTraceString(e) );
			return false;
		}
		
		logger.debug( "addBatch()   end" );
		return true;
	}
	
	/**
	 * 바인딩 변수를 파라미터 Map에 포함되어 있는지 검증한다.
	 * Case Sensitive -> Case Insensitive로 변경함.
	 * @param bind 바인딩 변수 목록
	 * @param params 바인딩 변수에 해당하는 key, value Map
	 * @throws Exception
	 */
	public void verifyInputParams( List<String> bind, Map<String, String> params ) throws Exception
	{
		for( String bindKey : bind )
		{
			boolean isMatch = false;
			for( Map.Entry<String, String> entry : params.entrySet() )
			{
				if( bindKey.equalsIgnoreCase(entry.getKey()) )
				{
					isMatch = true;
					break;
				}
			}
			
			if( !isMatch )
				throw new Exception( "Not Found Binding Variable : [" + bindKey + "]" );
		}
	}
	/**
	 * PrepareStatement에 바인딩 변수에 해당하는 값을 설정한다.
	 * Case Sensitive -> Case Insensitive로 변경함.
	 * @param bind 바인딩 변수 목록
	 * @param params 바인딩 변수에 해당하는 key, value Map
	 * @throws Exception
	 */
	public void bindInputParams( List<String> bind, Map<String, String> params ) throws Exception
	{
		int i = 1;
		for( String bindKey : bind )
		{
			logger.debug( "BIND : {} ", bindKey );
			
			boolean isMatch   = false;
			String  bindValue = null;
			for( Map.Entry<String, String> entry : params.entrySet() )
			{
				if( bindKey.equalsIgnoreCase(entry.getKey()) )
				{
					isMatch   = true;
					bindValue = entry.getValue();
					logger.debug( String.format("find [%s][%s]", bindKey, bindValue) );
					break;
				}
			}
			
			logger.debug( String.format("2 [%s][%s]", bindKey, bindValue) );
			
			if( !isMatch )
				throw new Exception( "Not Found Binding Variable : [" + bindKey + "]" );
			
			logger.debug( String.format("3 [%s][%s]", bindKey, bindValue) );
			
			//logger.debug( String.format("[%02d][%s] : [%s]\n", i, bindKey, bindValue) );
			ptmt.setString( i, bindValue );
			logger.debug( String.format("4 [%s][%s]", bindKey, bindValue) );
			++i;
		}
	}
	
	/**
	 * 대량의 트랜잭션을 실제로 DB에 실행한다.
	 * @return
	 * @throws Exception
	 */
	public int[] executeBatch() throws Exception
	{
		if( !checkPreparedStatement() )
			throw new Exception( "PrepareStatement is null" );
		
		int[] result = ptmt.executeBatch();
		logger.debug( "executeBatch() count : [{}]", result.length );
		
		return result;
	}
	
	/**
	 * 대량의 트랜잭션 종료 처리를 한다.
	 * @return
	 */
	public boolean finishBatch()
	{
		try
		{
			if( null != rs )
				rs.close();
			if( null != ptmt )
				ptmt.close();
		}
		catch( SQLException e )
		{
			logger.error( "SQLException occurs : [{}]", StringUtil.printStackTraceString(e) );
			return false;
		}
		
		logger.debug( "finishBatch()   end!!!" );
		return true;
	}
}
