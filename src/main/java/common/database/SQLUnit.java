/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.database
 * FILE      : SQLUnit.java
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.database;

import common.enums.DBVendorEnum;

import java.io.*;
import java.util.List;

public class SQLUnit
{
	private static final long serialVersionUID = 1L;
	
	private String       fileId;
	private DBVendorEnum dbe;
	private String       sqlId;
	private String       query;
	private List<String> input;
	
	public SQLUnit( String file, String id, String query, List<String> bindList )
	{
		this.fileId = file;
		this.sqlId  = id;
		this.query  = query;
		this.input  = bindList;
	}
	
	/* Getter */
	public String       getFileId()   { return this.fileId; }
	public String       getSqlId()    { return this.sqlId; }
	public String       getQuery()    { return this.query; }
	public String       getBindQuery(){ return getQuery().replaceAll( SQLManager.getBindPattern(), "?" ); }
	public List<String> getBindList() { return this.input; }
	public DBVendorEnum getDBVendor() { return this.dbe; }
	
	/* Setter */
	public void setFieldId ( String       id   ) { this.fileId = id; }
	public void setSqlId   ( String       id   ) { this.sqlId  = id; }
	public void setQuery   ( String       sql  ) { this.query  = sql; }
	public void setBindList( List<String> list ) { this.input  = list; }
	public void setDBVendor( DBVendorEnum dbe  ) { this.dbe    = dbe; }
	
	public String printQuery()
	{
		StringBuilder sb = new StringBuilder();
		sb.append( String.format("SQL [%s][%s]\n", fileId, sqlId) );
		sb.append( query );
		
		return sb.toString();
	}
	
	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		try
		{
			SQLUnit clone = (SQLUnit) super.clone();
			// Serializing
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream( baos );
			out.writeObject( clone );
			out.flush();
			
			// Deserializing
			ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() );
			ObjectInputStream in = new ObjectInputStream( bais );
			return in.readObject();
		}
		catch( IOException ie )
		{
			throw new CloneNotSupportedException( "Serialization/Deserialization failed." );
		}
		catch( ClassNotFoundException cnfe )
		{
			throw new CloneNotSupportedException( "Deserialization failed." );
		}
		catch( CloneNotSupportedException cnse )
		{
			throw cnse;
		}
	}
}
