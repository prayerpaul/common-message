/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.database
 * FILE      : SQLManager.java
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.database;

import common.Constable;
import common.enums.EncodingEnum;
import common.enums.ManagerStateEnum;
import common.exception.MessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.sql.DataSource;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SQLManager
{
	private static Logger logger = LoggerFactory.getLogger( SQLManager.class );
	
	private DataSource datasource;
	
	public static final int     DEFAULT_FETCH_CNT     = 1000;
	public static final String  DEFAULT_BIND_PATTERN  = "(:{1})([A-Za-z_0-9]{1,})";
	public static final String  DEFAULT_XPATH         = "//SQL";
	
	private static Pattern pattern;
	private Map<String, Map<String, SQLUnit>> sqlRepo;
	private Map<String, ManagerStateEnum>     stateRepo;
	
	protected SQLManager()
	{
		sqlRepo   = new ConcurrentHashMap<>();
		stateRepo = new ConcurrentHashMap<>();
		pattern = Pattern.compile( DEFAULT_BIND_PATTERN );
	}
	
	private static class SingletonHelper
	{
		private static final SQLManager INSTANCE = new SQLManager();
	}
	
	public static SQLManager getInstance()
	{
		return SingletonHelper.INSTANCE;
	}
	
	public static String getBindPattern() { return pattern.pattern(); }
	public static void setBindPattern( String pat )
	{
		if( DEFAULT_BIND_PATTERN.equals(pat) )
			return;
		
		pattern = Pattern.compile( pat );
	}
	public void setDataSource( DataSource ds ) { this.datasource = ds; }
	
	public Map<String, SQLUnit> getSqlRepo( String fileId ) { return sqlRepo.get( fileId ); }
	public SQLUnit getSQLUnit( String fileId, String sqlId ) throws CloneNotSupportedException { return (SQLUnit)sqlRepo.get(fileId).get(sqlId).clone(); }
	
	public DatabaseHandler getDatabaseHandler( String fileId, String sqlId ) throws SQLException
	{
		SQLUnit unit = sqlRepo.get( fileId ).get( sqlId );
		DatabaseHandler dh = new DatabaseHandler( unit );
		dh.setConnection( datasource.getConnection() );
		
		return dh;
	}
	
	public void load( String filePath ) throws MessageException
	{
		if( null == filePath )
			throw new MessageException( "SQL File Path is null" );
		
		String fileId = filePath.substring( filePath.lastIndexOf(Constable.FILE_SEPERATOR)+1 );
		load( fileId, filePath );
	}
	
	public void load( String fileId, String filePath ) throws MessageException
	{
		parseSQLFile( fileId, filePath );
	}
	
	public void load( String[] files ) throws MessageException
	{
		for( int i=0, n=files.length; i < n; ++i )
		{
			load( files[i] );
		}
	}
	
	public void load( List<String> files ) throws MessageException
	{
		for( String file: files )
		{
			load( file );
		}
	}
	
	public void reload( String filePath ) throws MessageException
	{
		String fileId = filePath.substring( filePath.lastIndexOf(Constable.FILE_SEPERATOR)+1 );
		reload( fileId, filePath );
	}
	
	public synchronized void reload( String fileId, String filePath ) throws MessageException
	{
		ManagerStateEnum mse = stateRepo.get( fileId );
		if( ManagerStateEnum.UPDATING == mse ) return;
		
		stateRepo.put( fileId, ManagerStateEnum.UPDATING );
		sqlRepo.remove( fileId );
		
		load( fileId, filePath );
	}
	
	public void parseSQLFile( String fileId, String filePath ) throws MessageException
	{
		try
		{
			InputSource is  = new InputSource( Files.newBufferedReader( Paths.get( filePath ), EncodingEnum.UTF8.getCharset() ) ); // non-blocking I/O and auto-closable
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			
			// XXE Attack Mitigation (2022.04.01 swcho.rnd)
			dbf.setFeature( "http://apache.org/xml/features/disallow-doctype-decl", true );
			dbf.setFeature( "http://xml.org/sax/features/external-general-entities", false );
			dbf.setFeature( "http://xml.org/sax/features/external-parameter-entities", false );
			dbf.setAttribute( XMLConstants.ACCESS_EXTERNAL_DTD, "" );
			dbf.setAttribute( XMLConstants.ACCESS_EXTERNAL_SCHEMA, "" );
			
			Document document = dbf.newDocumentBuilder().parse( is );
			
			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList sqlList = (NodeList)xPath.evaluate( DEFAULT_XPATH, document, XPathConstants.NODESET );
			
			Map<String, SQLUnit> sqlMap = null;
			if( sqlList.getLength() > 0 )
				sqlMap = new HashMap<>();
			
			for( int i=0, n=sqlList.getLength(); i < n; ++i )
			{
				String sqlId = sqlList.item( i ).getAttributes().getNamedItem( "ID" ).getTextContent();
				String sql   = sqlList.item( i ).getTextContent();
				
				SQLUnit unit = new SQLUnit( fileId, sqlId, sql, extractBindList(sql) );
				
				sqlMap.put( sqlId, unit );
				logger.debug( "FILE ID : [{}], SQL ID : [{}], BIND : [{}]", fileId, sqlId, unit.getBindList().size() );
			}
			
			sqlRepo.put( fileId, sqlMap );
			stateRepo.put( fileId, ManagerStateEnum.WORKING );
			
		}
		catch( XPathExpressionException | SAXException | IOException | ParserConfigurationException e )
		{
			throw new MessageException( e );
		}
	}
	
	public List<String> extractBindList( String sql )
	{
		Matcher matcher = pattern.matcher( sql );
		
		List<String> bindList = new ArrayList<>();
		while( matcher.find() )
		{
			//logger.debug( "[{}] : [{}]\n", matcher.groupCount(), matcher.group(1) );
			bindList.add( matcher.group(matcher.groupCount()) );
		}
		
		return bindList;
	}
}
