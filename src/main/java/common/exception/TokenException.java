/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.exception
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.exception;

public class TokenException extends Exception
{
	private static final long serialVersionUID = -2443441760336553070L;
	
	public TokenException( String msg ) { super(msg); }
	
	public TokenException( Throwable t ) {
		super(t.toString());
	}
	
	public TokenException( Exception e ) {
		super(e.toString());
	}
}
