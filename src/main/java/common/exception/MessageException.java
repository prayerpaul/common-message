/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.exception
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.exception;

public class MessageException extends Exception
{
	private static final long serialVersionUID = -2443441760336553070L;
	
	public MessageException( String msg ) { super(msg); }
	
	public MessageException( Throwable t ) {
		super(t.toString());
	}
	
	public MessageException( Exception e ) {
		super(e.toString());
	}
}
