/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.exception
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.exception;

public class ResponseException extends Exception
{
	private String resp_cd;
	
	public ResponseException( String cd, String msg )
	{
		super( msg );
		this.resp_cd = cd;
	}
	
	public ResponseException( Throwable t, String cd  )
	{
		super(t.toString());
		this.resp_cd = cd;
	}
	
	public ResponseException( Exception e, String cd )
	{
		super(e.toString());
		this.resp_cd = cd;
	}
	
	public String getResponseCode() { return resp_cd; }
	public String getResponseMessage() { return super.getMessage(); }
}
