/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.exception
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.exception;

public class HttpResponseException extends ResponseException
{
	private int status;
	
	public HttpResponseException( int status, String cd, String msg )
	{
		super( cd, msg );
		this.status = status;
	}
	
	public HttpResponseException( Throwable t, int status, String cd )
	{
		super( t, cd );
		this.status = status;
	}
	
	public HttpResponseException( Exception e, int status, String cd  )
	{
		super( e, cd );
		this.status = status;
	}
	
	public int getHttpResponseStatus() { return status; }
}
