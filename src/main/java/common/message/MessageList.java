/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import common.enums.CharacterEnum;
import common.enums.ElementEnum;
import common.exception.MessageException;
import common.message.control.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class MessageList extends MessageElement
{
	protected static final long serialVersionUID = 7229016916324470207L;
	private final Logger logger = LoggerFactory.getLogger( MessageList.class );
	
	protected List<MessageElement>        layoutList;
	protected List<List<MessageElement>>  dataRecList;
	
	public MessageList( String id, String name )
	{
		super( id, name, ElementEnum.LIST );
		this.layoutList  = new ArrayList<>();
		this.dataRecList = new ArrayList<>();
	}
	
	// Getter
	public int getLayoutElementCount() { return this.layoutList.size(); }
	public int getDataRecordCount()    { return this.dataRecList.size(); }
	public List<MessageElement>       getLayout() { return this.layoutList; }
	public List<List<MessageElement>> getDataList() { return this.dataRecList; }
	public List<MessageElement>       getDataRecord( int index ) { return this.dataRecList.get( index ); }
	// Setter
	public void setLayoutList( List<MessageElement> list )
	{
		this.layoutList.clear();
		this.layoutList.addAll( list );
	}
	public void appendDataRecord( List<MessageElement> rec )
	{
		this.dataRecList.add( rec );
	}
	public void setDataList( List<List<MessageElement>> list )
	{
		this.dataRecList.clear();
		this.dataRecList.addAll( list );
	}
	
	public void appendLayoutElement( int index, MessageElement me ) { this.layoutList.add( index, me ); }
	public void appendLayoutElement( MessageElement me ) { this.layoutList.add( me ); }
	
	public void clearData() { this.dataRecList.clear(); }
	@Override
	public void accept( Visitor visitor ) throws MessageException
	{
		visitor.visit( this );
	}
	
	/**
	 * (non-Javadoc)
	 * @see common.message.MessageElement#findElement(String)
	 *
	 * ex) 아래의 케이스를 다 만족시켜야 한다.
	 * case 1) ListId
	 * case 2) ListId[index].FieldId
	 * case 3) ListId[index].SubGroupId
	 * case 4) ListId[index].SubGroupId.fieldId
	 * case 5) ListId[index].SubListId
	 * case 6) ListId[index].SubListId[index].FieldId
	 * case 7) ListId[index].SubListId[index].GroupId
	 * case 8) ListId[index].SubListId[index].SSubListId
	 *
	 * 예외사항) MessageElement를 리턴하기 때문에 아래의 케이스는 처리하지 않음.
	 * ListId[index]
	 * ListId.ListId[index]
	 */
	@Override
	public MessageElement findElement( String elementId )
	{
		//logger.info( "[{}] findElement : [{}]", this.getElementId(), elementId );
		int pos = elementId.indexOf( CharacterEnum.PERIOD.getString() );
		boolean hasChild = pos > 0;
		
		String myDepthId = hasChild ? elementId.substring(0, pos) : elementId;
		int myDepthIndex = extractListIndex( myDepthId );
		//logger.info( "find record index : [{}]", myDepthIndex );
		
		// id가 일치하지 않으면 SKIP!!!
		if( !myDepthId.equals(getElementId()) && !myDepthId.startsWith(getElementId()) )
			return null;
		// 예외사항) ListId[index] 경우, null 리턴
		else if( myDepthId.startsWith(getElementId()) && !hasChild && myDepthIndex >= 0 )
			return null;
		// case 1) id가 일치하고 hasChild가 없고, RECORD index가 존재하지 않으면 자신을 리턴
		else if( getElementId().equals(myDepthId) && !hasChild && myDepthIndex < 0 )
		{
			//logger.info( "[{}] return this", this.getElementId() );
			return this;
		}
		
		//logger.info( "find record count : [{}]", getDataRecordCount() );
		if( getDataRecordCount() > 0 )
		{
			// 다음 Depth가 시작이 되도록 substring
			String subElementId = elementId.substring(pos + 1);
			MessageElement elem;
			List<MessageElement> list = this.getDataRecord(myDepthIndex);
			for( MessageElement me : list )
			{
				// case 2,3,4,5,6,7,8)
				elem = me.findElement(subElementId);
				if( null != elem )
					return elem;
			}
		}
		
		return null;
	}
	
	/*
	@Override
	public MessageElement replaceElement( String elementId, MessageElement nme )
	{
		int pos = elementId.indexOf( CharacterEnum.PERIOD.getString() );
		boolean hasChild = pos > 0;
		
		String myDepthId = hasChild ? elementId.substring(0, pos) : elementId;
		logger.info( "MessageList [{}] : myDepthId [{}]", this.getElementId(), myDepthId );
		if( !getElementId().equals(myDepthId) )
			return null;
		
		// 다음 Depth가 시작이 되도록 substring
		String subElementId = elementId.substring( pos+1 );
		logger.info( "MessageList [{}] : subElementId [{}], [{}]", this.getElementId(), subElementId, nme.getElementId() );
		List<MessageElement> list = this.getLayout();
		for( int i=0, n=list.size(); i < n; i++ )
		{
			MessageElement me = list.get(i);
			logger.info( "[{}] : [{}], [{}]", i, me.getElement(), me.getElementId() );
			if( me.getElement() == nme.getElement() && me.getElementId().equals(subElementId) )
			{
				logger.info( "MessageList match MessageElement!!! [{}]", me.getElementId() );
				list.remove( i );
				list.add( i, nme );
				return nme;
			}
			else
			{
				MessageElement m = me.replaceElement( subElementId, nme );
				if( null != m )
					return m;
			}
		}
		
		return null;
	}
	*/
	
	protected int extractListIndex( String value )
	{
		int start = value.indexOf( CharacterEnum.LEFT_BRACKET.getString() );
		if( start < 0 )
			return -1;
		int end = value.indexOf( CharacterEnum.RIGHT_BRACKET.getString() );
		if( end < 0 )
			return -1;
		
		String index = value.substring( start + 1, end );
		if( index.length() <= 0 )
			return -1;
		return Integer.parseInt( index );
	}
	
	/**
	 * (non-Javadoc)
	 * @see Object#clone()
	 */
	@Override
	public MessageList clone() throws CloneNotSupportedException
	{
		MessageList clone = (MessageList)super.clone();
		// copy MessageList Layout
		clone.setLayoutList( cloneMessageElementList(this.getLayout()) );
		
		// copy MessageList DataList
		List<List<MessageElement>> lol = this.getDataList();
		for( List<MessageElement> l: lol )
		{
			clone.appendDataRecord( cloneMessageElementList(l) );
		}
		
		return clone;
	}
}
