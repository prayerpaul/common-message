/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import common.enums.CharacterEnum;
import common.enums.ElementEnum;
import common.exception.MessageException;
import common.message.control.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MessageGroup extends MessageElement
{
	protected static final long serialVersionUID = -1928127044636942500L;
	private final Logger logger = LoggerFactory.getLogger( MessageGroup.class );
	protected List<MessageElement> grpEleList;
	
	public MessageGroup( String id, String name )
	{
		super( id, name, ElementEnum.GROUP );
		this.grpEleList = new ArrayList<>();
	}
	
	// Getter
	public int                  getElementCount() { return this.grpEleList.size(); }
	public MessageElement       getElement( int index )
	{
		return this.grpEleList.get( index );
	}
	public List<MessageElement> getElementList() { return this.grpEleList; }
	// Setter
	public void setElementList( List<MessageElement> list )
	{
		this.grpEleList.clear();
		this.grpEleList.addAll( list );
	}
	
	public void appendElement( int index, MessageElement me ) { this.grpEleList.add( index, me ); }
	public void appendElement( MessageElement me ) { this.grpEleList.add( me ); }
	
	/**
	 * (non-Javadoc)
	 * @see common.message.MessageElement#accept(Visitor)
	 */
	@Override
	public void accept( Visitor visitor ) throws MessageException
	{
		visitor.visit( this );
	}
	
	/**
	 * (non-Javadoc)
	 * @see common.message.MessageElement#findElement(String)
	 *
	 * ex) 아래의 케이스를 다 만족시켜야 한다.
	 * case 1) GroupId
	 * case 2) GroupId.FieldId
	 * case 3) GroupId.SubGroupId
	 * case 4) GroupId.SubGroupId.FieldId
	 * case 5) GroupId.ListId
	 * case 6) GroupId.ListId[index].FieldId
	 * case 7) GroupId.ListId[index].GroupId
	 * case 8) GroupId.ListId[index].SubListId
	 * 예외사항) MessageElement를 리턴하지 않기 때문에 아래의 케이스는 처리하지 않음.
	 * GroupId.ListId[index]
	 */
	@Override
	public MessageElement findElement( String elementId )
	{
		int pos = elementId.indexOf( CharacterEnum.PERIOD.getString() );
		boolean hasChild = pos > 0;
		
		String myDepthId = hasChild ? elementId.substring(0, pos) : elementId;
		//logger.info( "MessageGroup findElement -> [{}]", myDepthId );
		// id가 일치하지 않으면 SKIP!!!
		if( !getElementId().equals(myDepthId) )
			return null;
		// case 1) id가 일치하고 hasChild가 없으면, 자신을 리턴
		else if( getElementId().equals(myDepthId) && !hasChild )
			return this;
		
		// 다음 Depth가 시작이 되도록 substring
		String subElementId = elementId.substring( pos+1 );
		//MessageElement elem;
		for( MessageElement me: this.getElementList() )
		{
			// case 2,3,4,5,6,7,8)
			MessageElement element = me.findElement( subElementId );
			if( null != element )
				return element;
		}
		
		return null;
	}
	
	/*
	@Override
	public MessageElement replaceElement( String elementId, MessageElement nme )
	{
		int pos = elementId.indexOf( CharacterEnum.PERIOD.getString() );
		boolean hasChild = pos > 0;
		
		String myDepthId = hasChild ? elementId.substring(0, pos) : elementId;
		logger.info( "MessageGroup [{}] : myDepthId [{}]", this.getElementId(), myDepthId );
		if( !getElementId().equals(myDepthId) )
			return null;
		
		// 다음 Depth가 시작이 되도록 substring
		String subElementId = elementId.substring( pos+1 );
		logger.info( "MessageGroup [{}] : subElementId [{}], [{}]", this.getElementId(), subElementId, nme.getElementId() );
		List<MessageElement> list = this.getElementList();
		for( int i=0, n=list.size(); i < n; i++ )
		{
			MessageElement me = list.get(i);
			logger.info( "[{}] : [{}], [{}]", i, me.getElement(), me.getElementId() );
			if( me.getElement() == nme.getElement() && me.getElementId().equals(subElementId) )
			{
				logger.info( "MessageGroup match MessageElement!!! [{}]", me.getElementId() );
				list.remove( i );
				list.add( i, nme );
				return nme;
			}
			else
			{
				MessageElement m = me.replaceElement( subElementId, nme );
				if( null != m )
					return m;
			}
		}
		
		return null;
	}
	*/
	
	/**
	 * (non-Javadoc)
	 * @see Object#clone()
	 */
	@Override
	public MessageGroup clone() throws CloneNotSupportedException
	{
		try
		{
			MessageGroup clone = (MessageGroup)super.clone();
			// Serializing
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream out  = new ObjectOutputStream( baos );
			out.writeObject( clone );
			out.flush();
			
			// Deserializing
			ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() );
			ObjectInputStream in   = new ObjectInputStream( bais );
			return (MessageGroup)in.readObject();
			//clone.setElement( cloneMessageElementList(this.getElementList()) );
			//return clone;
		}
		catch( IOException ie )
		{
			throw new CloneNotSupportedException( "Serialization/Deserialization failed. : " + ie );
		}
		catch( ClassNotFoundException ce )
		{
			throw new CloneNotSupportedException( "Deserialization failed. : " + ce );
		}
	}
}
