/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message
 * FILE      : MessageArray.java
 * DATE      : 2022.09.06
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import common.enums.ArrayEnum;
import common.enums.ElementEnum;
import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.control.Visitor;
import common.utility.ByteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class MessageArray extends MessageElement
{
	protected static final long serialVersionUID = 7229016916324470207L;
	private final Logger logger = LoggerFactory.getLogger( MessageArray.class );
	
	private ArrayEnum type;
	private int length;
	private int point;
	private List<Object> dataList;
	
	public MessageArray( String id, String name )
	{
		super( id, name, ElementEnum.ARRAY );
		setLength( 0 );
		setPointLength( 0 );
		this.dataList  = new ArrayList<>();
	}
	
	public int getLength() { return this.length; }
	public int getPointLength()  { return this.point; }
	public ArrayEnum getArrayType() { return this.type; }
	public int getDataCount() { return this.dataList.size(); }
	public Object getDataIndex( int i ) { return this.dataList.get( i ); }
	public List<Object> getData() { return this.dataList; }
	// Setter
	public void setArrayType( ArrayEnum fe ) { this.type = fe; }
	public void setLength( int length ) { this.length = length; }
	public void setPointLength( int point ) { this.point = point; }
	public void setDataList( List<Object> list )
	{
		this.dataList.clear();
		this.dataList.addAll( list );
	}
	public void addData( Object obj ) { this.dataList.add( obj ); }
	
	public void clearData() { this.dataList.clear(); }
	@Override
	public void accept( Visitor visitor ) throws MessageException
	{
		visitor.visit( this );
	}
	
	/**
	 * (non-Javadoc)
	 * @see MessageElement#findElement(String)
	 */
	@Override
	public MessageElement findElement( String elementId )
	{
		if( elementId.equals(getElementId()) )
			return this;
		return null;
	}
	
	public String getStringValueByLength( int index, byte ch, EncodingEnum ee )
	{
		byte[] bytes = getByteValueByLength( index, ch, ee );
		if( null == bytes )
			return null;
		
		String value = new String( bytes, ee.getCharset() );
		logger.debug( "getStringValueByLength() -> [{}] : [{}] [{}]", getElementId(), ee, value );
		return value;
	}
	
	/**
	 * MessageArray 길이만큼의 byte array 데이터를 리턴한다.
	 *
	 * @param ch 데이터가 길이보다 작을 때, padding할 character
	 * @param ee EncodingEnum
	 * @return MessageField의 길이만큼의 byte array
	 * <p>
	 * ex) getByteValueByLength( '0', EncodingEnum.EUCKR );
	 */
	public byte[] getByteValueByLength( int index, byte ch, EncodingEnum ee )
	{
		byte[] array = getByteValue( getDataIndex(index), getArrayType(), ee );
		
		if( ArrayEnum.STRING == getArrayType() )
			return ByteUtil.getStringByteByLength( array, getLength(), ch );
		else
			return ByteUtil.getNumericByteByLength( array, getLength(), getPointLength(), ch );
	}
	
	public byte[] getByteValue( Object data, ArrayEnum fe, EncodingEnum ee )
	{
		if( ArrayEnum.STRING ==  fe )
			return ((String)data).getBytes( ee.getCharset() );
		else
			return String.valueOf( data ).getBytes( ee.getCharset() );
	}
}
