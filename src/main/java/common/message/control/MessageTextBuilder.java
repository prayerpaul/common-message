/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.*;
import common.exception.MessageException;
import common.message.*;
import common.utility.ByteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Message Data를 PlainText 형식의 문자열로 생성한다.
 * TODO LIST) ==> finished
 * 1. 문자열 내에 Carrige Return(\r), Line Feed(\n) 는 공백으로 변환한다.(완료)
 * 2. build() 함수를 리턴하기 전에 Plain Text 맨 앞에 길이부 자리수 만큼 전문 길이 설정하도록 한다.(완료)
 * 3. XML 이나 JSON 형식의 LIST 형태의 데이터에서 항목은 존재하나 항목에 대한 데이터가 존재하지 않는 경우, LIST 건수를 0, 데이터가 존재하지 않게 하여 생성한다.(완료)
 *    -> 현재는 LIST 건수 1, 1건에 대한 RECORD 사이즈 만큼 공백이 채워짐.
 */
public class MessageTextBuilder extends MessageBuilder
{
	private final Logger logger = LoggerFactory.getLogger( MessageTextBuilder.class );
	protected ByteArrayOutputStream bytes;
	
	public MessageTextBuilder( EncodingEnum ee )
	{
		super( ee );
		
		setupVisitor();
	}
	
	/**
	 * (non-Javadoc)
	 * @see MessageBuilder#build()
	 */
	@Override
	public String build() throws MessageException
	{
		return buildString();
	}
	
	
	/**
	 * Message의 Element들의 값들을 연결하여 PlainText 문자열로 리턴한다.
	 * @return Message를 PlainText로 변환한 문자열
	 */
	public String buildString()
	{
		return new String( buildBytes(), getEncoding().getCharset() );
	}
	
	/**
	 * Message의 Element들의 값들을 연결하여 byte array로 리턴한다.
	 * '\r', '\n' byte 값을 ' '(SPACE) 로 변환한 후에 리턴한다.
	 * @return Message를 byte array로 변환한 문자열
	 */
	public byte[] buildBytes()
	{
		byte[] byteArray = ByteUtil.replaceBytes( bytes.toByteArray(), CharacterEnum.SPACE.getByte(), CharacterEnum.CARRIAGE_RETURN.getByte(), CharacterEnum.LINE_FEED.getByte() );
		return setTextLength( byteArray );
	}
	
	/**
	 * Plain Text 길이부를 설정한다.
	 * @param src Plain Text byte array
	 * @return 길이부에 총 길이가 설정된 byte array
	 */
	public byte[] setTextLength( byte[] src )
	{
		MessageField mf = getLengthMessageField();
		
		String length;
		// 연속거래를 위해서 길이 필드가 지정이 되어 있지 않으면, 그냥 리턴
		if( null == mf )
			return src;
		
		String format = "%0" + mf.getLength() + "d";
		//
		MessageMacroEnum mme = mf.getMacro( MessageMacroEnum.MACRO_LENGTH_INCLUDE );
		if( MessageMacroEnum.MACRO_LENGTH_INCLUDE == mme )
			length = String.format( format, src.length - mf.getLength() );
		else
			length = String.format( format, src.length );
		// 전체 길이 설정
		mf.setStringValue( length, getEncoding() );
		
		return ByteUtil.replaceByteArray( length.getBytes(getEncoding().getCharset()), src, 0 );
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor()
	{
		this.bytes = new ByteArrayOutputStream();
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField mf)
	 */
	@Override
	public MessageField visitField( MessageField mf ) throws MessageException
	{
		try
		{
			if( FieldEnum.NUMBER == mf.getFieldType() )
				bytes.write( mf.getByteValueByLength(CharacterEnum.ZERO.getByte(), getEncoding()) );
			else
				bytes.write( mf.getByteValueByLength(CharacterEnum.SPACE.getByte(), getEncoding()) );
		}
		catch( IOException e )
		{
			throw new MessageException( e );
		}
		
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup mg)
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		logger.info( "visitGroup() -> [{}] [{}] START", mg.getElement(), mg.getElementId() );
		
		pushStackNode( new ParentStackNode(mg) );
		
		for( MessageElement me: mg.getElementList() )
		{
			logger.debug( "[{}], [{}] visit", me.getElement(), me.getElementId() );
			
			visitElement( me );
		}
		
		popStackNode();
		
		logger.info( "visitGroup() -> [{}] [{}] ELEMENT COUNT [{}] END", mg.getElement(), mg.getElementId(), mg.getElementCount() );
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		logger.info( "visitArray() -> [{}] [{}] REFER [{}] START", ma.getElement(), ma.getElementId(), ma.getReferElement() );
		
		try
		{
			for( int i=0, n=ma.getDataCount(); i < n; ++i )
			{
				if( ArrayEnum.NUMBER == ma.getArrayType() )
					bytes.write(ma.getByteValueByLength(i, CharacterEnum.ZERO.getByte(), getEncoding()));
				else
					bytes.write(ma.getByteValueByLength(i, CharacterEnum.SPACE.getByte(), getEncoding()));
			}
		}
		catch( IOException e )
		{
			throw new MessageException( e );
		}
		
		logger.info( "visitArray() -> [{}] [{}] ARRAY COUNT [{}] END", ma.getElement(), ma.getElementId(), ma.getDataCount() );
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList ml)
	 */
	@Override
	public MessageList visitList( MessageList ml ) throws MessageException
	{
		logger.info( "visitList() -> [{}] [{}] REFER [{}] START", ml.getElement(), ml.getElementId(), ml.getReferElement() );
		
		if( ml.containsPreMacro(MessageMacroEnum.MACRO_BLANK_RECORD_SKIP) )
		{
			BlankRecordSkipper brs = skipStack.pop();
			byte[] brsArr = brs.buildBytes();
			logger.info( "visitList() -> [{}] [{}] DATA [{}]", ml.getElement(), ml.getElementId(), new String(brsArr, getEncoding().getCharset()) );
			if( brsArr.length > 0 )
			{
				try
				{
					bytes.write( brsArr );
				}
				catch( IOException e )
				{
					throw new MessageException( e );
				}
			}
			logger.info( ">>>>>>>>>>>>>>> visitList() -> [{}] [{}] BLANK_RECORD_SKIP RECORD COUNT [{}] [{}]", ml.getElement(), ml.getElementId(), brs.getDataRecordCount(), new String(brs.buildBytes(), getEncoding().getCharset()) );
		}
		else
		{
			for( int i = 0, n = ml.getDataRecordCount(); i < n; ++i )
			{
				pushStackNode( new ParentStackNode(ml, i) );
				
				List<MessageElement> list = ml.getDataRecord(i);
				for( MessageElement me : list )
				{
					visitElement(me);
				}
				
				popStackNode();
			}
			logger.info( "visitList() -> [{}] [{}] RECORD COUNT [{}] END", ml.getElement(), ml.getElementId(), ml.getDataRecordCount() );
		}
		
		return ml;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitMessage(Message m)
	 */
	@Override
	public Message visitMessage( Message m ) throws MessageException
	{
		logger.info( "visitMessage() -> [{}] [{}], PATH [{}]", m.getElement(), m.getElementId(), getElementPath() );
		
		pushStackNode( new ParentStackNode(m) );
		
		for( MessageElement me: m.getElementList() )
		{
			visitElement( me );
		}
		
		popStackNode();
		
		logger.info( "visitMessage() -> [{}] [{}], ELEMENT COUNT [{}] END", m.getElement(), m.getElementId(), m.getElementCount() );
		return m;
	}
}
