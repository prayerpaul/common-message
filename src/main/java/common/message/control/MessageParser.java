/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message.control
 * FILE      : MessageParser.java
 * DATE      : 2022.08.14
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.EncodingEnum;

public abstract class MessageParser extends AbstractVisitor
{
	private final String data;
	
	public MessageParser( String data, EncodingEnum ee )
	{
		super( ee );
		this.data = data;
	}
	
	/**
	 * 파서로 파싱할 데이터를 리턴한다.
	 * @return 파싱할 데이터
	 */
	public String getParserStrData() { return data; }
	public byte[] getParserByteData() { return data.getBytes( getEncoding().getCharset() ); }
}
