/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.EncodingEnum;
import common.enums.MessageMacroEnum;
import common.exception.MessageException;
import common.message.*;
import common.utility.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.StringWriter;
import java.io.Writer;
import java.util.List;


public class MessageXMLBuilder extends MessageBuilder
{
	private final Logger logger = LoggerFactory.getLogger( MessageXMLBuilder.class );
	protected Document doc;
	protected boolean  isCntPrint; // 건수 출력 여부. default : false
	
	public MessageXMLBuilder( EncodingEnum ee )
	{
		this( ee, false );
	}
	
	public MessageXMLBuilder( EncodingEnum ee, boolean cntPrint )
	{
		super( ee );
		this.isCntPrint = cntPrint;
		
		setupVisitor();
	}
	
	// Getter
	public String build() throws MessageException
	{
		return build( true );
	}
	public String build( boolean isPretty ) throws MessageException
	{
		Writer writer;
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		
		try
		{
			// XXE Attack Mitigation
			transformerFactory.setFeature( XMLConstants.FEATURE_SECURE_PROCESSING, true );
			transformerFactory.setAttribute( XMLConstants.ACCESS_EXTERNAL_DTD, "" );
			transformerFactory.setAttribute( XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "" );
			
			Transformer transformer;
			if( isPretty )
			{
				// 반드시 아래와 같은 순서로 설정을 해야 XML 데이터가 Pretty 하게 됨.
				transformerFactory.setAttribute("indent-number", 2 );
				transformer = transformerFactory.newTransformer();
				// pretty print XML
				transformer.setOutputProperty( OutputKeys.INDENT, "yes" );
			}
			else
				transformer = transformerFactory.newTransformer();
			
			writer = new StringWriter();
			transformer.transform( new DOMSource(doc), new StreamResult(writer) );
		}
		catch( Exception e )
		{
			throw new MessageException( e );
		}
		
		return writer.toString();
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor()
	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try
		{
			DocumentBuilder dBuilder  = dbFactory.newDocumentBuilder();
			doc = dBuilder.newDocument();
		}
		catch( Exception e )
		{
			logger.error( "{}", StringUtil.printStackTraceString(e) );
		}
	}
	
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField)
	 */
	@Override
	public MessageField visitField( MessageField mf )
	{
		// 건수 출력 여부가 false 이고, 필드 매크로가 목록 건수(#LIST_CNT)이면 XML 에 포함시키지 않는다.
		MessageMacroEnum mme = mf.getMacro( MessageMacroEnum.MACRO_LIST_CNT );
		if( !this.isCntPrint && MessageMacroEnum.MACRO_LIST_CNT == mme )
			return mf;
		
		Element element = getCurrentElement( mf );
		
		String value = mf.getStringValue( getEncoding() );
		
		element.appendChild( doc.createTextNode(value) );
		// 상위 Node에 등록
		logger.debug( "visitField Parent [{}] ::: Current [{}]", ((ParentStackNode)peekStackNode()).getNode(), element );
		
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup)
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		logger.info( "visitGroup() -> [{}] [{}] START", mg.getElement(), mg.getElementId() );
		
		Element element  = getCurrentElement( mg );
		pushStackNode( new ParentStackNode(mg, element) );
		
		List<MessageElement> list = mg.getElementList();
		for( MessageElement me: list )
		{
			visitElement( me );
		}
		
		popStackNode();
		
		logger.info( "visitGroup() -> [{}] [{}] ELEMENT COUNT [{}] END", mg.getElement(), mg.getElementId(), mg.getElementCount() );
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		logger.info( "visitArray() -> [{}] [{}] START", ma.getElement(), ma.getElementId() );
		
		for( int i=0, n=ma.getDataCount(); i < n; ++i )
		{
			Element element = getCurrentElement( ma );
			element.appendChild( doc.createTextNode((String)ma.getDataIndex(i)) );
		}
		
		logger.info( "visitArray() -> [{}] [{}] ARRAY COUNT [{}] END", ma.getElement(), ma.getElementId(), ma.getDataCount() );
		
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList)
	 * Ex) 아래와 같은 형태로 XML 데이터를 생성한다.
	 * <주소이력>
	 *     <전입일>19911201</전입일>
	 *     <변동일>19911201</변동일>
	 *     <변동사유>행정구역변경</변동사유>
	 *     <주소>충청남도 서천군 망우리</주소>
	 *     <도로명주소-이력/>
	 * </주소이력>
	 * <주소이력>
	 *     <전입일>19950101</전입일>
	 *     <변동일>19950101</변동일>
	 *     <변동사유>행정구역변경</변동사유>
	 *     <주소>충청남도 서천군 망우리</주소>
	 *     <도로명주소-이력/>
	 * </주소이력>
	 */
	@Override
	public MessageList visitList( MessageList ml ) throws MessageException
	{
		logger.info( "visitList() -> [{}] [{}] START", ml.getElement(), ml.getElementId() );
		
		int recCnt = ml.getDataRecordCount();
		// 데이터가 있는 경우, Record 만큼 생성
		if( recCnt > 0 )
		{
			for( int i = 0; i < recCnt; ++i )
			{
				Element element = getCurrentElement( ml );
				pushStackNode( new ParentStackNode(ml, i, element) );
				
				List<MessageElement> list = ml.getDataRecord( i );
				for( MessageElement me : list )
				{
					visitElement(me);
				}
				
				popStackNode();
			}
			
			logger.info( "visitList() -> [{}] [{}] RECORD COUNT [{}] END", ml.getElement(), ml.getElementId(), recCnt );
		}
		// 데이터가 없는 경우, 1개의 Record 생성 XML 태그만.
		else
		{
			Element element = getCurrentElement( ml );
			pushStackNode( new ParentStackNode(ml, element) );
			
			List<MessageElement> list = ml.getLayout();
			for( MessageElement me: list )
			{
				visitElement( me );
			}
			
			popStackNode();
			
			logger.info( "visitList() -> [{}] [{}] RECORD COUNT [{}] PSEUDO 1 END", ml.getElement(), ml.getElementId(), recCnt );
		}
		return ml;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitMessage(Message)
	 */
	@Override
	public Message visitMessage( Message m ) throws MessageException
	{
		logger.info( "visitMessage() -> [{}] [{}] PATH [{}]", m.getElement(), m.getElementId(), getElementPath() );
		
		if( !StringUtil.isBlank(getElementPath()) )
		{
			logger.debug( "visitMessage() -> MESSAGE [{}] PATH [{}]", m.getElementId(), getElementPath() );
			Element element = createElementPath( doc, getElementPath() );
			pushStackNode( new ParentStackNode(m, element) );
		}
		
		List<MessageElement> list = m.getElementList();
		for( MessageElement me: list )
		{
			visitElement( me );
		}
		
		if( !emptyStack() )
			popStackNode();
		
		logger.info( "visitMessage() -> [{}] [{}] ELEMENT COUNT [{}] END", m.getElement(), m.getElementId(), m.getElementCount() );
		return m;
	}
	
	/**
	 * Stack을 확인하여 부모 Element 가 존재하면, 부모 Element 에 현재 Element를 연결하고,
	 * 부모 Element가 존재하지 않으면, Document Root에 연결한다.
	 */
	public Element getCurrentElement( MessageElement me )
	{
		Element element = doc.createElement( me.getElementId() );
		
		if( emptyStack() )
		{
			logger.debug( "getCurrentElement : {} append to Root", element.getTagName() );
			doc.appendChild( element );
		}
		else
		{
			logger.debug( "getCurrentElement : {} append to parent", element.getTagName() );
			((Element) ((ParentStackNode) peekStackNode()).getNode()).appendChild( element );
		}
		
		return element;
	}
}
