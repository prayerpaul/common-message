/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message.control
 * FILE      : BlankRecordSkipper.java
 * DATE      : 2022.08.10
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.*;
import common.exception.MessageException;
import common.message.*;
import common.utility.ByteUtil;
import common.utility.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * TODO : BlankRecordSkipper 와 BlankRecordCounter 통합 및 버그 수정 필요.(완료)
 */
public class BlankRecordSkipper extends MessageTextBuilder
{
	private final Logger logger = LoggerFactory.getLogger( BlankRecordSkipper.class );
	private int nonBlankRecordCount;
	private Boolean isRecordBlank;
	
	public BlankRecordSkipper( EncodingEnum ee )
	{
		super( ee );
		
		setupVisitor();
	}
	
	public int getDataRecordCount() { return nonBlankRecordCount; }
	public boolean isListBlank() { return nonBlankRecordCount == 0; }
	
	/**
	 * (non-Javadoc)
	 * @see MessageTextBuilder#buildBytes()
	 */
	@Override
	public byte[] buildBytes()
	{
		return ByteUtil.replaceBytes( bytes.toByteArray(), CharacterEnum.SPACE.getByte(), CharacterEnum.CARRIAGE_RETURN.getByte(), CharacterEnum.LINE_FEED.getByte() );
		/*
		byte[] byteArray = ByteUtil.replaceBytes( bytes.toByteArray(), CharacterEnum.SPACE.getByte(), CharacterEnum.CARRIAGE_RETURN.getByte(), CharacterEnum.LINE_FEED.getByte() );
		return byteArray;
		*/
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor()
	{
		super.setupVisitor();
		nonBlankRecordCount = 0;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField)
	 */
	@Override
	public MessageField visitField( MessageField mf ) throws MessageException
	{
		logger.debug( "visitField -> [{}] [{}] : [{}]", mf.getElement(), mf.getElementId(), mf.getData() );
		
		// MessageField 값이 존재하고, 매크로가 LIST_CNT 가 아니면, 데이터가 있는 것으로 설정한다.
		// 목록의 Record 별로 데이터를 작성하고,
		MessageMacroEnum mme = mf.getMacro( MessageMacroEnum.MACRO_LIST_CNT );
		if( !StringUtil.isBlank(mf.getData()) && MessageMacroEnum.MACRO_LIST_CNT != mme )
		{
			logger.info( "visitField -> DATA EXIST [{}]:[{}] MACRO[{}], DATA[{}],", mf.getElement(), mf.getElementId(), mme, mf.getData() );
			isRecordBlank = false;
		}
		
		try
		{
			ByteArrayOutputStream recByteArr = (ByteArrayOutputStream) ((ParentStackNode) peekStackNode()).getNode();
			if( FieldEnum.NUMBER == mf.getFieldType() )
				recByteArr.write(mf.getByteValueByLength(CharacterEnum.ZERO.getByte(), getEncoding()));
			else
				recByteArr.write(mf.getByteValueByLength(CharacterEnum.SPACE.getByte(), getEncoding()));
		}
		catch( IOException e )
		{
			throw new MessageException(e);
		}
		
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		logger.info( "visitArray() -> [{}] [{}] REFER [{}] START", ma.getElement(), ma.getElementId(), ma.getReferElement() );
		
		try
		{
			if( ma.getDataCount() > 0 )
				isRecordBlank = false;
			
			ByteArrayOutputStream recByteArr = (ByteArrayOutputStream) ((ParentStackNode) peekStackNode()).getNode();
			for( int i=0, n=ma.getDataCount(); i < n; ++i )
			{
				if( ArrayEnum.NUMBER == ma.getArrayType() )
					recByteArr.write(ma.getByteValueByLength(i, CharacterEnum.ZERO.getByte(), getEncoding()));
				else
					recByteArr.write(ma.getByteValueByLength(i, CharacterEnum.SPACE.getByte(), getEncoding()));
			}
		}
		catch( IOException e )
		{
			throw new MessageException( e );
		}
		
		logger.info( "visitArray() -> [{}] [{}] ARRAY COUNT [{}] END", ma.getElement(), ma.getElementId(), ma.getDataCount() );
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup mg)
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		for( MessageElement me: mg.getElementList() )
		{
			visitElement( me );
		}
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList)
	 */
	@Override
	public MessageList visitList( MessageList ml ) throws MessageException
	{
		logger.info( ">>>>>>>>>> visitList -> [{}] [{}] START <<<<<<<<<<", ml.getElement(), ml.getElementId() );
		
		for( int i=0, n=ml.getDataRecordCount(); i < n; ++i )
		{
			logger.info( "********** [{}] RECORD [{}]TH START **********", ml.getElementId(), i );
			ParentStackNode psnRec = new ParentStackNode( ml, i, new ByteArrayOutputStream() );
			pushStackNode( psnRec );
			isRecordBlank = true;
			
			List<MessageElement> list = ml.getDataRecord(i);
			for( MessageElement me : list )
			{
				if( ElementEnum.LIST == me.getElement() && !skipStack.empty() )
				{
					BlankRecordSkipper brs = skipStack.pop();
					byte[] array = brs.buildBytes();
					try
					{
						if( array.length > 0 && !brs.isListBlank() )
						{
							isRecordBlank = false;
							ByteArrayOutputStream recByteArr = (ByteArrayOutputStream) psnRec.getNode();
							logger.debug( "[{}][{}]", brs.isListBlank(), brs.buildString() );
							recByteArr.write( array );
						}
					}
					catch( IOException e )
					{
						throw new MessageException( e );
					}
				}
				else
					visitElement( me );
			}
			
			popStackNode();
			byte[] recArr = ((ByteArrayOutputStream) psnRec.getNode()).toByteArray();
			if( !isRecordBlank )
			{
				nonBlankRecordCount++;
				logger.debug( "!!!!!!!!!! [{}] RECORD [{}]TH IS NOT BLANK [{}]", ml.getElementId(), i, new String(recArr, getEncoding().getCharset()) );
				try
				{
					bytes.write( recArr );
				}
				catch( IOException e )
				{
					throw new MessageException(e);
				}
			}
			logger.info( "********** [{}] RECORD [{}]TH   END : [{}] **********", ml.getElementId(), i, psnRec.getNode().toString() );
		}
		
		logger.info( ">>>>>>>>>> visitList -> [{}] [{}] NON BLANK RECORD COUNT [{}]  END <<<<<<<<<<", ml.getElement(), ml.getElementId(), nonBlankRecordCount );
		
		return ml;
	}
}
