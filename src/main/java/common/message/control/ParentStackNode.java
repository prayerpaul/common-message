/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message.control
 * FILE      : StackMessageNode.java
 * DATE      : 2022.08.12
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.ElementEnum;
import common.enums.MessageMacroEnum;
import common.message.MessageElement;

public class ParentStackNode
{
	private final MessageElement me;
	private int listIndex;
	private Object obj;
	
	public ParentStackNode( MessageElement me )
	{
		this.me = me;
	}
	
	public ParentStackNode( MessageElement me, Object obj )
	{
		this.me = me;
		this.obj = obj;
	}
	
	public ParentStackNode( MessageElement me, int index )
	{
		this.me = me;
		this.listIndex = index;
	}
	
	public ParentStackNode( MessageElement me, int index, Object obj )
	{
		this.me = me;
		this.listIndex = index;
		this.obj = obj;
	}
	
	public String getElementId() { return me.getElementId(); }
	public ElementEnum getElement() { return me.getElement(); }
	public MessageElement getMessageElement() { return me; }
	public int getListIndex() { return listIndex; }
	public Object getNode() { return obj; }
	
	public void setNode( Object obj ) { this.obj = obj; }
}
