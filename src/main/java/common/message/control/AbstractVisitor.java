/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.*;
import common.exception.MessageException;
import common.message.*;
import common.utility.DateUtil;
import common.utility.NumberUtil;
import common.utility.StringUtil;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;

public abstract class AbstractVisitor implements Visitor
{
	public final Logger logger = LoggerFactory.getLogger( AbstractVisitor.class );
	private final EncodingEnum encoding;
	private String path;
	
	protected MessageField   lenField;
	protected Stack<Object>  parentStack;
	public List<MessageElement> visitList;
	public Stack<BlankRecordSkipper> skipStack;
	
	public AbstractVisitor( EncodingEnum ee )
	{
		this.encoding     = ee;
		this.visitList    = new ArrayList<>();
		this.parentStack  = new Stack<>();
		this.skipStack    = new Stack<>();
	}
	
	// Getter
	public EncodingEnum getEncoding() { return this.encoding; }
	public List<MessageElement> getVisitList() { return this.visitList; }
	public String getPathDelimiter() { return CharacterEnum.SLASH.getString(); }
	public String getFindDelimiter() { return CharacterEnum.PERIOD.getString(); }
	
	public MessageField getLengthMessageField() { return this.lenField; }
	
	// parentStack 관련
	public void pushStackNode( Object obj ) { parentStack.push( obj ); }
	public Object popStackNode() { return parentStack.pop(); }
	public Object peekStackNode() { return parentStack.peek(); }
	public Boolean emptyStack() { return parentStack.empty(); }
	
	//
	
	/**
	 * XML 이나 JSON 형식의 데이터를 접근할 때, 접근할 위치를 리턴한다.
	 * @return XML 이나 JSON 형식의 데이터에서 시작 위치 Path
	 */
	public String getElementPath()
	{
		if( StringUtil.isBlank(path) )
			return CharacterEnum.BLANK.getString();
		
		return path;
	}
	
	/**
	 * XML 이나 JSON 형식의 데이터를 접근할 때, 접근할 위치를 설정한다.
	 * @param path XML 이나 JSON 형식의 데이터에서 시작 위치 Path
	 * Ex) parser.setElementPath( "Envelope/Body/Response" );
	 */
	public void setElementPath( String path ) { this.path = path; }
	
	// Setter
	@Override
	public void visit( MessageElement me ) throws MessageException
	{
		visitList.add( me );
		visitElement( me );
	}
	
	/**
	 * MessageElement 에 따라 visit 함수를 분기 처리한다.
	 * @param me Message 내에 각 요소를 나타내는 상위 클래스
	 * @throws MessageException 메시지 처리중에 발생하는 Exception
	 */
	public void visitElement( MessageElement me ) throws MessageException
	{
		switch( me.getElement() )
		{
		case FIELD:
			visitField( (MessageField)me );
			break;
		case GROUP:
			visitGroup( (MessageGroup)me );
			break;
		case ARRAY:
			visitArray( (MessageArray)me );
			break;
		case LIST:
			visitList( (MessageList)me );
			break;
		case MESSAGE:
			visitMessage( (Message)me );
			break;
		default:
			throw new MessageException( "Unsupported MessageElementEnum : " + me.getElement() );
		}
	}
	
	/**
	 * Concrete Visitor 클래스가 실행되기 전에  필요한 setup(초기화) 작업을 수행한다.
	 */
	public abstract void setupVisitor();
	
	/**
	 * Visitor 클래스가 MessageField 를 만났을 때, 처리할 로직을 Concrete Visitor 클래스별로 정의한다.
	 * @param mf MessageField object(객체)
	 * @return 처리된 MessageField object(객체)
	 * @throws MessageException 메시지 처리중에 발생하는 Exception
	 */
	public abstract MessageField visitField( MessageField mf ) throws MessageException;
	
	/**
	 * Visitor 클래스가 MessageGroup 을 만났을 때, 처리할 로직을 Concrete Visitor 클래스별로 정의한다.
	 * @param mg MessageGroup object(객체)
	 * @return 처리된 MessageGroup object(객체)
	 * @throws MessageException 메시지 처리중에 발생하는 Exception
	 */
	public abstract MessageGroup visitGroup( MessageGroup mg ) throws MessageException;
	
	/**
	 * Visitor 클래스가 MessageArray 를 만났을 때, 처리할 로직을 Concrete Visitor 클래스별로 정의한다.
	 * @param ma MessageArray object(객체)
	 * @return 처리된 MessageArray object(객체)
	 * @throws MessageException 메시지 처리중에 발생하는 Exception
	 */
	public abstract MessageArray visitArray( MessageArray ma ) throws MessageException;
	
	/**
	 * Visitor 클래스가 MessageList 를 만났을 때, 처리할 로직을 Concrete Visitor 클래스별로 정의한다.
	 * @param ml MessageList object(객체)
	 * @return 처리된 MessageList object(객체)
	 * @throws MessageException 메시지 처리중에 발생하는 Exception
	 */
	public abstract MessageList visitList( MessageList ml ) throws MessageException;
	/**
	 * Visitor 클래스가 Message 를 만났을 때, 처리할 로직을 Concrete Visitor 클래스별로 정의한다.
	 * @param m Message object(객체)
	 * @return 처리된 Message object(객체)
	 * @throws MessageException 메시지 처리중에 발생하는 Exception
	 */
	public abstract Message visitMessage( Message m ) throws MessageException;
	
	/**
	 * Message 에 대한 MACRO 처리
	 * @param m MACRO 처리할 Message Object
	 * @return 매크로 처리한 Message
	 */
	public Message handleMessagePreMacro( Message m )
	{
		// TODO
		// -. 암호화 방식
		// -.
		return m;
	}
	/**
	 * MessageGroup 에 대한 MACRO 처리
	 * @param mg MACRO 처리할 MessageGroup Object
	 * @return 매크로 처리한 MessageGroup
	 */
	public MessageGroup handleGroupPreMacro( MessageGroup mg )
	{
		return mg;
	}
	
	/**
	 * MessageArray 에 대한 MACRO 처리
	 * @param ma MACRO 처리할 MessageArray Object
	 * @return
	 */
	public MessageArray handleArrayPreMacro( MessageArray ma ) throws MessageException
	{
		for( MessageMacroEnum mme : ma.getPreMacroList() )
		{
			switch( mme )
			{
			case MACRO_FIXED_SIZE:
				logger.info("[{}]::[{}] CURRENT_RECORD [{}], FIXED_SIZE [{}]", ma.getElement(), ma.getElementId(), ma.getDataCount(), ma.getDefaultValue() );
				int limitRecCnt = Integer.parseInt( ma.getDefaultValue() );
				if( limitRecCnt < ma.getDataCount() )
				{
					String errorMsg = String.format( "데이터 항목 [%s]의 건수 [%d]가 설정된 건수 [%d]보다 큽니다.", ma.getElementId(), ma.getDataCount(), limitRecCnt );
					throw new MessageException( errorMsg );
				}
				
				for( int index = ma.getDataCount(); index < limitRecCnt; ++index )
				{
					switch( ma.getArrayType() )
					{
					case STRING:
						ma.addData( new String(CharacterEnum.BLANK.getString()) );
						break;
					case NUMBER:
						if( ma.getPointLength() > 0 )
						{
							ma.addData( new Double("0.0") );
						}
						else
						{
							ma.addData( new Long(0) );
						}
						break;
					default:
						throw new MessageException( "Unsupported MessageArray Type : " + ma.getElement() );
					}
				}
				break;
			default:
				logger.info( "[{}] is not set [{}]", ma.getElementId(), mme );
			}
		}
		
		return ma;
	}
	/**
	 * MessageList 에 대한 MACRO 처리
	 * @param ml MACRO 처리할 MessageList Object
	 * @return 매크로 처리한 MessageList
	 */
	public MessageList handleListPreMacro( MessageList ml ) throws MessageException
	{
		for( MessageMacroEnum mme : ml.getPreMacroList() )
		{
			switch( mme )
			{
			case MACRO_BLANK_RECORD_SKIP:
				logger.info("[{}] is set [{}]", ml.getElementId(), mme);
				break;
			case MACRO_FIXED_SIZE:
				logger.info("[{}]::[{}] CURRENT_RECORD [{}], FIXED_SIZE [{}]", ml.getElement(), ml.getElementId(), ml.getDataRecordCount(), ml.getDefaultValue());
				int limitRecCnt = Integer.parseInt(ml.getDefaultValue());
				if( limitRecCnt < ml.getDataRecordCount() )
				{
					String errorMsg = String.format( "데이터 항목 [%s]의 목록 건수 [%d]가 설정된 목록 건수 [%d]보다 큽니다.", ml.getElementId(), ml.getDataRecordCount(), limitRecCnt );
					throw new MessageException( errorMsg );
				}
				
				for( int index = ml.getDataRecordCount(); index < limitRecCnt; ++index )
				{
					try
					{
						List<MessageElement> cloneList = ml.cloneMessageElementList( ml.getLayout() );
						ml.appendDataRecord( cloneList );
					}
					catch( Exception e )
					{
						logger.error( "[{}]::[{}] MACRO_FIXED_SIZE ERROR", ml.getElement(), ml.getElementId() );
					}
				}
				break;
			case MACRO_DELIMITER_BAR:
				// TODO 정확한 처리는 요건이 나온 후 확정할 예정.
				/*
				MessageField mf = new MessageField( "DELIMITER", "" );
				mf.setLength( 1 );
				mf.setFieldType( FieldEnum.STRING );
				mf.setStringValue( CharacterEnum.VERTICAL_BAR.getString(), getEncoding() );
				for( List<MessageElement> list : ml.getDataList() )
				{
					// Field
					int m = list.size();
					for( int j=0, sm = m + (m-1); j < sm-1; j += 2 )
					{
						list.add( j+1, mf );
					}
					// Record 별로 구분자를 추가하는 방식
					// list.add( mf );
				}
				*/
				break;
			default:
				logger.info( "[{}] is not set [{}]", ml.getElementId(), mme );
			}
		}
		
		return ml;
	}
	/**
	 * MessageField 에 대한 MACRO 처리
	 * @param mf MACRO 처리할 MessageField Object
	 * @return 매크로 처리한 MessageField
	 */
	public MessageField handleFieldPreMacro( MessageField mf )
	{
		for( MessageMacroEnum mme : mf.getPreMacroList() )
		{
			switch( mme )
			{
			case MACRO_DATE_YYYYMMDD:
			case MACRO_TIME_HHMMSS:
			case MACRO_DATETIME:
			case MACRO_TIME_MILISEC:
			case MACRO_DATETIME_MILI:
				// 단순히 값이 존재 유무만 체크하면 됨.
				if( StringUtil.isBlank(mf.getData()) )
				{
					String str = DateUtil.getDateTime(MessageMacroEnum.getFormat(mme));
					mf.setStringValue(str, getEncoding());
					logger.debug("[{}] --> [{}] : [{}][{}][{}]", mme, mf.getElementId(), mf.getDefaultValue(), getEncoding(), mf.getStringValue(getEncoding()));
				}
				break;
			case MACRO_RAND_NUM:
				// 단순히 값이 존재 유무만 체크하면 됨.
				if( StringUtil.isBlank(mf.getData()) )
				{
					String str = DateUtil.getDateTimeMilisec();
					String rand = NumberUtil.getRandomNumber(mf.getLength() - str.length());
					mf.setStringValue(str + rand, getEncoding());
					logger.debug("[{}] --> [{}] : [{}][{}][{}]", mme, mf.getElementId(), mf.getDefaultValue(), getEncoding(), mf.getStringValue(getEncoding()));
				}
				break;
			case MACRO_URL_ENCODE:
				mf.setData(getURLEncodeString(mf.getStringValue(getEncoding())));
				break;
			case MACRO_URL_DECODE:
				mf.setData(getURLDecodeString(mf.getStringValue(getEncoding())));
				break;
			case MACRO_LIST_CNT:
				// TODO 현재는 상위노드 안에서만 MessageList 가 검색이 가능함. 추후에는 상위노드 이외의 MessageList 도 검색이 가능하도록 기능 추가 필요.
				// TODO List<MessageElement> visitList 와 REFER_PATH 를 이용하면 확장 가능.
				String referId = getReferElementPath(mf);
				if( null == referId )
					return mf;
				
				logger.info("FIELD [{}] MACRO [{}] REFER : [{}]", mf.getElementId(), mme, referId);
				ParentStackNode psn = (ParentStackNode) peekStackNode();
				MessageElement me = psn.getMessageElement().findElement(referId);
				switch( me.getElement() )
				{
				case LIST:
					MessageList ml = (MessageList) psn.getMessageElement().findElement(referId);
					
					try
					{
						// MessageList 에서 BLANK_RECORD_SKIP 매크로가 있는지 찾고, 있는 경우와 없는 경우 분리해서 처리
						MessageMacroEnum listMacro = ml.getMacro(MessageMacroEnum.MACRO_BLANK_RECORD_SKIP);
						logger.info("[{}], [{}], MACRO [{}]", ml.getElement(), ml.getElementId(), listMacro);
						// 일반적인 처리
						if( MessageMacroEnum.NULL == listMacro )
						{
							mf.setStringValue(String.valueOf(ml.getDataRecordCount()), getEncoding());
							logger.info("[{}] : [{}] : REFER [{}] : COUNT [{}]", ml.getPreMacro(0), mf.getElementId(), mf.getReferElement(), ml.getDataRecordCount());
						}
						// BLANK_RECORD_SKIP 매크로 처리
						else
						{
							// BlankRecordCounter : Blank Record 제외한 건수를 계산하는 로직 추가
							logger.info("#################### [{}] BlankRecordSkipper START ####################", me.getElementId());
							BlankRecordSkipper brs = new BlankRecordSkipper(getEncoding());
							ml.accept(brs);
							logger.info("[{}] : [{}] : REFER [{}] : COUNT [{}]", ml.getPreMacro(0), mf.getElementId(), mf.getReferElement(), brs.getDataRecordCount());
							logger.info("#################### [{}] BlankRecordSkipper COUNT [{}] END ####################", me.getElementId(), brs.getDataRecordCount());
							mf.setStringValue(String.valueOf(brs.getDataRecordCount()), getEncoding());
							
							// BlankRecordSkipper push to stack
							skipStack.push(brs);
							logger.info("push to BlankRecordSkipper STACK [{}]", ml.getElementId());
						}
					}
					catch( Exception e )
					{
						logger.error(StringUtil.printStackTraceString(e));
					}
					break;
				case ARRAY:
					mf.setStringValue(String.valueOf(((MessageArray) me).getDataCount()), getEncoding());
					break;
				}
				break;
			case MACRO_LENGTH_INCLUDE:
			case MACRO_LENGTH_EXCLUDE:
				// 나중에 MessageTextBuilder 에서 build()시에 해당 필드 값을 보고 처리한다.
				lenField = mf;
				break;
			default:
				//if( StringUtil.isBlank(mf.getStringValue(getEncoding())) && !StringUtil.isBlank(mf.getDefault()) )
				// 단순히 값이 존재 유무만 체크하면 됨.
				if( StringUtil.isBlank(mf.getData()) && !StringUtil.isBlank(mf.getDefaultValue()) )
				{
					mf.setStringValue(mf.getDefaultValue(), getEncoding());
					logger.trace("[{}] [{}] --> [{}] : [{}][{}][{}]", mme, mf.getDefaultValue(), mf.getElementId(), mf.getDefaultValue(), getEncoding(), mf.getStringValue(getEncoding()));
				}
			}
		}
		
		return mf;
	}
	
	// TODO POST_MACRO 처리를 위한 함수. 요건이 나오는데로 추가 예정.
	public MessageField handleFieldPostMacro( MessageField mf ) { return mf; }
	public MessageGroup handleGroupPostMacro( MessageGroup mg ) { return mg; }
	public MessageList handleListPostMacro( MessageList ml ) { return ml; }
	public MessageArray handleArrayPostMacro( MessageArray ma ) { return ma; }
	public Message handleMessagePostMacro( Message m ) { return m; }
	
	/**
	 * 상위 노드안에서 참조하는 MessageElement를 찾을 수 있도록 Stack 에서 상위 노드를 조회하여 Refer 경로를 생성한다.
	 * @param me Refer 경로를 설정할 MessageElement
	 * @return 성공 : 생성된 Refer Path, 실패 : Stack에 상위 노드가 존재하지 않으면 null
	 */
	public String getReferElementPath( MessageElement me )
	{
		// Refer 정보가 없는 경우 null return
		if( null == me.getReferElement() )
			return null;
		
		String referId;
		// 부모 노드가 존재하지 않는 경우 자신의 Refer 정보 return
		if( emptyStack() )
		{
			return me.getReferElement();
		}
		else
		{
			ParentStackNode psn = (ParentStackNode) peekStackNode();
			
			if( ElementEnum.LIST == psn.getElement() )
				referId = psn.getElementId() + CharacterEnum.LEFT_BRACKET.getString() + psn.getListIndex() + CharacterEnum.RIGHT_BRACKET.getString() + getFindDelimiter() + me.getReferElement();
			else
				referId = psn.getElementId() + getFindDelimiter() + me.getReferElement();
		}
		
		return referId;
	}
	
	/**
	 * 문자열을 URLEncoder.encode() 처리한다.
	 * @param value encode 대상 문자열
	 * @return encode 된 문자열
	 */
	public String getURLEncodeString( String value )
	{
		String encValue;
		try
		{
			encValue = URLEncoder.encode( value, getEncoding().getString() );
		}
		catch( UnsupportedEncodingException e )
		{
			logger.error( "String [{}] fail to URLEncoder.encode() using [{}]", value, getEncoding() );
			return value;
		}
		return encValue;
	}
	
	/**
	 * 문자열을 URLDecoder.decode() 처리한다.
	 * @param value decode 대상 문자열
	 * @return decode 된 문자열
	 */
	public String getURLDecodeString( String value )
	{
		String decValue;
		try
		{
			decValue = URLDecoder.decode( value, getEncoding().getString() );
		}
		catch( UnsupportedEncodingException e )
		{
			logger.error( "String [{}] fail to URLDecoder.decode() using [{}]", value, getEncoding() );
			return value;
		}
		return decValue;
	}
	
	/**
	 * JSON 형식 데이터의 접근 위치를 체크하고 존재하지 않으면 해당 JSONObject 를 생성하고 상위 노드와 연결한다.
	 * 최종 위치의 JSONObject를 리턴한다.
	 * @param root 시작 위치의 JSONObject
	 * @param path 시작 위체에서 부터의 하위 Path
	 * @return 최종 노드 path 의 JSONObject
	 */
	public JSONObject createElementPath( JSONObject root, String path )
	{
		// TODO path 파라미터에 대한 Boundary Condition 체크 로직 추가 필요.
		// Ex) 본인정보확인_여신/국내거소신고사실증명/거소인정보/ 의 마지막 "/" 는 제거하고 시작하도록 한다.
		
		String[] arr = path.split( getPathDelimiter() );
		
		JSONObject curr = root;
		for( int i = 0, n = arr.length; i < n; ++i )
		{
			logger.debug( "PATH [{}] : [{}]", i, arr[i] );
			JSONObject obj = (JSONObject) curr.get( arr[i] );
			if( null == obj )
			{
				logger.debug( "[{}] is null", arr[i] );
				JSONObject sub = new JSONObject();
				curr.put( arr[i], sub );
				curr = sub;
			}
			else
				curr = obj;
		}
		
		return curr;
	}
	
	
	/**
	 * XML 형식 데이터의 접근 위치를 체크하고 존재하지 않으면 해당 Element 를 생성하고 상위 노드와 연결한다.
	 * 최종 위치의 Element 를 리턴한다.
	 * @param doc XML Document 객체
	 * @param path XPath 형식("/"의 구분자)으로 이루어진 path 정보
	 * @return 최종 위치의 Element
	 * @throws MessageException
	 */
	public Element createElementPath( Document doc, String path ) throws MessageException
	{
		// TODO path 파라미터에 대한 Boundary Condition 체크 로직 추가 필요.
		// Ex) 본인정보확인_여신/국내거소신고사실증명/거소인정보/ 의 마지막 "/" 는 제거하고 시작하도록 한다.
		
		try
		{
			logger.debug( "createElementPath : path [{}]", path );
			XPath xpath = XPathFactory.newInstance().newXPath();
			
			String[] arr = path.split(getPathDelimiter());
			logger.debug( "createElementPath : [{}][{}]", arr.length, arr );
			
			String tmpPath = getPathDelimiter() + arr[0];
			Element curr = (Element) xpath.evaluate( tmpPath, doc, XPathConstants.NODE );
			if( null == curr )
			{
				curr = doc.createElement( arr[0] );
				doc.appendChild( curr );
			}
			logger.debug( "createElementPath : curr : [{}]", curr );
			for( int i = 1, n = arr.length; i < n; ++i )
			{
				tmpPath = String.format("%s%s%s", tmpPath, getPathDelimiter(), arr[i]);
				logger.debug( "createElementPath : tmpPath : [{}]", tmpPath );
				Element child = (Element) xpath.evaluate(tmpPath, doc, XPathConstants.NODE);
				if( null == child )
				{
					logger.debug( "createElementPath : child is [{}]", child );
					Element ele = doc.createElement( arr[i] );
					curr.appendChild(ele);
					curr = ele;        // no need to cast Node
				}
				else
				{
					logger.debug( "createElementPath : child is not null [{}]", child );
					curr = child;
				}
			}
			
			logger.debug( "createElementPath : curr : [{}]", curr );
			return curr;
		}
		catch( XPathExpressionException e )
		{
			logger.error(StringUtil.printStackTraceString(e) );
			throw new MessageException( e );
		}
	}
}
