/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import java.util.List;

import common.enums.CharacterEnum;
import common.enums.ElementEnum;
import common.enums.EncodingEnum;
import common.enums.MessageMacroEnum;
import common.exception.MessageException;
import common.message.*;
import common.utility.ByteUtil;
import common.utility.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageTextParser extends MessageParser
{
	private final Logger logger = LoggerFactory.getLogger( MessageTextParser.class );
	private byte[] byteData;
	private int    cursor;
	
	public MessageTextParser( String data, EncodingEnum ee )
	{
		super( data, ee );
		
		setupVisitor();
	}
	
	public int getTextCursor() { return cursor; }
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor()
	{
		this.byteData = getParserByteData();
		this.cursor = 0;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField)
	 */
	@Override
	public MessageField visitField( MessageField mf )
	{
		logger.debug( "START CURSOR [{}], MessageField : [{}][{}][{}][{}]", cursor, mf.getElementId(), mf.getFieldType(), mf.getLength(), mf.getStringValue(getEncoding()) );
		byte[] ba = ByteUtil.copyByte( byteData, getTextCursor(), mf.getLength() );
		logger.info( "DATA LENGTH [{}], CUROSR [{}], FIELD [{}] LENGTH [{}], [{}]", byteData.length, getTextCursor(), mf.getElementId(), mf.getLength(), new String(ba, getEncoding().getCharset()) );
		mf.setByteValue( ByteUtil.trimByte(ba), getEncoding() );
		cursor += mf.getLength();
		logger.debug( "  END CURSOR [{}], MessageField : [{}][{}][{}][{}]", cursor, mf.getElementId(), mf.getFieldType(), mf.getLength(), mf.getStringValue(getEncoding()) );
		
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup)
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		logger.info( "visitGroup() -> [{}] [{}] START", mg.getElement(), mg.getElementId() );
		
		ParentStackNode begPsn = new ParentStackNode( mg );
		pushStackNode( begPsn );
		logger.debug( "visitGroup() -> stack.push() : [{}] : [{}]", begPsn.getElement(), begPsn.getElementId() );
		
		logger.debug( "[{}] : [{}] START", mg.getElement(), mg.getElementId() );
		List<MessageElement> list = mg.getElementList();
		for( MessageElement me: list )
		{
			logger.debug( "visitGroup() -> [{}] : [{}] visit", me.getElement(), me.getElementId() );
			visitElement( me );
		}
		logger.debug( "[{}] : [{}]   END", mg.getElement(), mg.getElementId() );
		
		//
		ParentStackNode endPsn = (ParentStackNode) popStackNode();
		logger.debug( "visitGroup() -> stack.pop() : [{}] : [{}]", endPsn.getElement(), endPsn.getElementId() );
		
		logger.info( "visitGroup() -> [{}] [{}] ELEMENT COUNT [{}] END", mg.getElement(), mg.getElementId(), mg.getElementCount() );
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		logger.info( "visitArray() -> [{}] [{}] START", ma.getElement(), ma.getElementId() );
		String referId = ma.getReferElement();
		
		int countData = 0;
		ParentStackNode psn = (ParentStackNode) peekStackNode();
		
		// ARRAY 의 DATA COUNT 계산
		if( StringUtil.isBlank(referId) )
		{
			MessageMacroEnum mme = ma.getMacro( MessageMacroEnum.MACRO_FIXED_SIZE);
			if( MessageMacroEnum.MACRO_FIXED_SIZE == mme )
				countData = Integer.parseInt( ma.getDefaultValue() );
		}
		else
		{
			if( ElementEnum.LIST == psn.getElement() )
				referId = psn.getElementId() + CharacterEnum.LEFT_BRACKET.getString() + psn.getListIndex() + CharacterEnum.RIGHT_BRACKET.getString() + getFindDelimiter() + referId;
			else
				referId = psn.getElementId() + getFindDelimiter() + referId;
			logger.info( "visitArray() -> REFER : [{}]", referId );
			MessageField mf = (MessageField) psn.getMessageElement().findElement( referId );
			if( null != mf )
			{
				logger.info( "visitArray() -> [{}] [{}], REFER : [{}], FIELD : [{}][{}]", ma.getElement(), ma.getElementId(), referId, mf.getElementId(), mf.getData() );
				MessageMacroEnum mme = mf.getMacro( MessageMacroEnum.MACRO_LIST_CNT );
				if( MessageMacroEnum.MACRO_LIST_CNT == mme && !StringUtil.isBlank(mf.getData()) )
				{
					countData = (Integer) mf.getNumberValue();
				}
			}
		}
		
		try
		{
			for( int i = 0; i < countData; ++i )
			{
				byte[] ba = ByteUtil.copyByte( byteData, getTextCursor(), ma.getLength() );
				String data = new String( ba, getEncoding().getCharset() );
				logger.debug( "DATA LENGTH [{}], CUROSR [{}], FIELD [{}] LENGTH [{}], [{}]", byteData.length, getTextCursor(), ma.getElementId(), ma.getLength(), data );
				ma.addData( data );
				cursor += ma.getLength();
			}
		}
		catch( Exception  e )
		{
			throw new MessageException( e );
		}
		
		logger.info( "visitArray() -> [{}] [{}] ARRAY COUNT [{}] END", ma.getElement(), ma.getElementId(), ma.getDataCount() );
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList)
	 */
	@Override
	public MessageList visitList( MessageList ml ) throws MessageException
	{
		String referId = getReferElementPath( ml );
		logger.info( "visitList() -> [{}] [{}] REFER [{}] START", ml.getElement(), ml.getElementId(), referId );
		
		int countRecord = 0;
		ParentStackNode psn = (ParentStackNode) peekStackNode();
		
		// LIST 의 RECORD COUNT 계산
		if( StringUtil.isBlank(referId) )
		{
			MessageMacroEnum mme = ml.getMacro( MessageMacroEnum.MACRO_FIXED_SIZE);
			if( MessageMacroEnum.MACRO_FIXED_SIZE == mme )
				countRecord = Integer.parseInt( ml.getDefaultValue() );
		}
		else
		{
			MessageField mf = (MessageField) psn.getMessageElement().findElement( referId );
			if( null != mf )
			{
				logger.debug("visitList() -> LIST [{}], REFER : [{}], FIELD : [{}][{}]", ml.getElementId(), referId, mf.getElementId(), mf.getData());
				MessageMacroEnum mme = mf.getMacro( MessageMacroEnum.MACRO_LIST_CNT );
				if( MessageMacroEnum.MACRO_LIST_CNT == mme && !StringUtil.isBlank(mf.getData()) )
				{
					countRecord = (Integer) mf.getNumberValue();
				}
			}
		}
		
		try
		{
			for( int i = 0; i < countRecord; ++i )
			{
				ParentStackNode begRecPsn = new ParentStackNode( ml, i );
				pushStackNode( begRecPsn );
				logger.debug( "visitList() -> stack.push() : Parent [{}], [{}] RECORD : [{}]", psn.getElementId(), begRecPsn.getElementId(), i );

				List<MessageElement> cloneList = ml.cloneMessageElementList( ml.getLayout() );
				ml.appendDataRecord( cloneList );
				
				for( MessageElement me : cloneList )
				{
					visitElement(me);
				}
				
				ParentStackNode endRecPsn = (ParentStackNode) popStackNode();
				logger.debug( "visitList() -> stack.pop() : Parent [{}], [{}] RECORD : [{}]", psn.getElementId(), endRecPsn.getElementId(), i );
			}
		}
		catch( Exception  e )
		{
			throw new MessageException( e );
		}
		
		logger.info( "visitList() -> [{}] [{}] RECORD COUNT [{}] END", ml.getElement(), ml.getElementId(), ml.getDataRecordCount() );
		
		return ml;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitMessage(Message)
	 */
	@Override
	public Message visitMessage( Message m ) throws MessageException
	{
		logger.info( "visitMessage() -> [{}] [{}], PATH [{}]", m.getElement(), m.getElementId(), getElementPath() );
		pushStackNode( new ParentStackNode(m) );
		logger.debug( "visitMessage() -> stack.push() : [{}] : [{}]", ((ParentStackNode)peekStackNode()).getElement(), ((ParentStackNode)peekStackNode()).getElementId() );
		
		List<MessageElement> list = m.getElementList();
		
		for( MessageElement me: list )
		{
			visitElement( me );
		}
		
		popStackNode();
		
		logger.info( "visitMessage() -> [{}] [{}], ELEMENT COUNT [{}] END", m.getElement(), m.getElementId(), m.getElementCount() );
		return m;
	}
}
