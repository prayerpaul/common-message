/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message.control
 * FILE      : MessageBuilder.java
 * DATE      : 2022.08.14
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class MessageBuilder extends AbstractVisitor
{
	private final Logger logger = LoggerFactory.getLogger( MessageBuilder.class );
	
	public MessageBuilder( EncodingEnum ee )
	{
		super( ee );
	}
	
	public abstract String build() throws MessageException;
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitElement(MessageElement)
	 */
	@Override
	public void visitElement( MessageElement me ) throws MessageException
	{
		// 출력여부 결정
		if( !me.isPrintable() )
		{
			logger.info( ">>>>>>>>>> visitElement() => [{}]::[{}], PRINT [{}] and then SKIP!!! <<<<<<<<<<", me.getElement(), me.getElementId(), me.isPrintable() );
			return;
		}
		
		switch( me.getElement() )
		{
		case FIELD:
			handleMessageField( (MessageField)me );
			break;
		case GROUP:
			handleMessageGroup( (MessageGroup)me );
			break;
		case ARRAY:
			handleMessageArray( (MessageArray)me );
			break;
		case LIST:
			handleMessageList( (MessageList)me );
			break;
		case MESSAGE:
			handleMessage( (Message)me );
			break;
		default:
			throw new MessageException( "Unsupported MessageElementEnum : " + me.getElement() );
		}
	}
	
	/**
	 * MessageField 를 처리한다. 이 함수는 PreMacro 처리, MessageField 처리, PostMacro 처리로 이루어져 있다.
	 * @param mf MessageField Object
	 * @throws MessageException
	 */
	protected void handleMessageField( MessageField mf ) throws MessageException
	{
		// PRE_MACRO
		handleFieldPreMacro( mf );
		
		// call visitField
		MessageField amf = visitField( mf );
		
		// POST_MACRO
		handleFieldPostMacro( amf );
	}
	
	/**
	 * MessageGroup 를 처리한다. 이 함수는 PreMacro 처리, MessageGroup 처리, PostMacro 처리로 이루어져 있다.
	 * @param mg MessageGroup Object
	 * @throws MessageException
	 */
	protected void handleMessageGroup( MessageGroup mg ) throws MessageException
	{
		// PRE_MACRO
		handleGroupPreMacro( mg );
		
		// call visitField
		MessageGroup amg = visitGroup( mg );
		
		// POST_MACRO
		handleGroupPostMacro( amg );
	}
	
	/**
	 * MessageArray 를 처리한다. 이 함수는 PreMacro 처리, MessageArray 처리, PostMacro 처리로 이루어져 있다.
	 * @param ma MessageArray Object
	 * @throws MessageException
	 */
	protected void handleMessageArray( MessageArray ma ) throws MessageException
	{
		// PRE_MACRO
		handleArrayPreMacro( ma );
		
		// call visitArray
		MessageArray ama = visitArray( ma );
		
		// POST_MACRO
		handleArrayPostMacro( ama );
	}
	
	/**
	 * MessageList 를 처리한다. 이 함수는 PreMacro 처리, MessageList 처리, PostMacro 처리로 이루어져 있다.
	 * @param ml MessageList Object
	 * @throws MessageException
	 */
	protected void handleMessageList( MessageList ml ) throws MessageException
	{
		// PRE_MACRO
		handleListPreMacro( ml );
		
		// call visitArray
		MessageList aml = visitList( ml );
		
		// POST_MACRO
		handleListPostMacro( aml );
	}
	
	/**
	 * Message 를 처리한다. 이 함수는 PreMacro 처리, Message 처리, PostMacro 처리로 이루어져 있다.
	 * @param m Message Object
	 * @throws MessageException
	 */
	protected void handleMessage( Message m ) throws MessageException
	{
		// PRE_MACRO
		handleMessagePreMacro( m );
		
		// call visitArray
		Message am = visitMessage( m );
		
		// POST_MACRO
		handleMessagePostMacro( am );
	}
}
