/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.ElementEnum;
import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.*;
import common.utility.StringUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class MessageJSONParser extends MessageParser
{
	private final Logger logger = LoggerFactory.getLogger( MessageJSONParser.class );
	protected JSONParser parser;
	protected JSONObject root;
	
	public MessageJSONParser( String data, EncodingEnum ee )
	{
		super( data, ee );
		
		setupVisitor();
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor()
	{
		parser = new JSONParser();
		
		try
		{
			root = (JSONObject) parser.parse( getParserStrData() );
		}
		catch( ParseException e )
		{
			logger.error( "{}", StringUtil.printStackTraceString(e) );
		}
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField)
	 */
	@Override
	public MessageField visitField( MessageField mf ) throws MessageException
	{
		JSONObject parent = (JSONObject)((ParentStackNode)peekStackNode()).getNode();
		logger.debug( "FIELD : [{}], PARENT : [{}]", mf.getElementId(), parent );

		switch( mf.getFieldType() )
		{
		case STRING:
		case VARIABLE:
		case DELIMITER:
			String data = (String) parent.get(mf.getElementId());
			if( null == data )
				return mf;
			
			logger.debug("visitField -> [{}][{}], [{}]", mf.getElementId(), mf.getFieldType(), data);
			mf.setStringValue(data, getEncoding());
			break;
		case NUMBER:
			if( mf.getPointLength() > 0 )
			{
				Double dblData = (Double) parent.get( mf.getElementId() );
				if( null == dblData )
					return mf;
				
				logger.debug("visitField -> [{}][{}], [{}]", mf.getElementId(), mf.getFieldType(), dblData );
				mf.setDoubleValue( dblData );
			}
			else
			{
				Long longData = (Long) parent.get( mf.getElementId() );
				if( null == longData )
					return mf;
				
				logger.debug("visitField -> [{}][{}], [{}]", mf.getElementId(), mf.getFieldType(), longData );
				mf.setLongValue( longData );
			}
			break;
		default:
			throw new MessageException( "Unsupported FieldEnum : " + mf.getFieldType() );
		}
		
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup)
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		logger.info( "visitGroup() -> [{}] [{}] START", mg.getElement(), mg.getElementId() );
		
		JSONObject grp = (JSONObject) getCurrentObject( mg );
		pushStackNode( new ParentStackNode(mg, grp) );
		
		for( MessageElement me: mg.getElementList() )
		{
			visitElement( me );
		}
		
		JSONObject pop = (JSONObject)((ParentStackNode)popStackNode()).getNode();
		logger.info( "visitGroup() -> [{}] [{}] ELEMENT COUNT [{}]  END", mg.getElement(), mg.getElementId(), mg.getElementCount() );
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		logger.info( "visitArray() -> [{}] [{}] START", ma.getElement(), ma.getElementId() );
		
		JSONArray array = (JSONArray) getCurrentObject( ma );
		logger.info( "[{}] [{}] {}", ma.getElement(), ma.getElementId(), array );
		
		switch( ma.getArrayType() )
		{
		case STRING:
			for( int i=0, n=array.size(); i < n; ++i )
			{
				String data = (String) array.get( i );
				logger.info( "STRING : [{}]", data );
				ma.addData( data );
			}
			break;
		case NUMBER:
			if( ma.getPointLength() > 0 )
			{
				for( int i=0, n=array.size(); i < n; ++i )
				{
					Double data = (Double) array.get( i );
					logger.info( "DOUBLE : [{}]", data );
					ma.addData( data );
				}
			}
			else
			{
				for( int i=0, n=array.size(); i < n; ++i )
				{
					Long data = (Long) array.get( i );
					logger.info( "LONG : [{}]", data );
					ma.addData( data );
				}
			}
			break;
		default:
			throw new MessageException( "Unsupported FieldEnum : " + ma.getArrayType() );
		}
		
		logger.info( "visitArray() -> [{}] [{}] ARRAY COUNT [{}] END", ma.getElement(), ma.getElementId(), ma.getDataCount() );
		
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList)
	 */
	@Override
	public MessageList visitList( MessageList ml ) throws MessageException
	{
		logger.info( "visitList() -> [{}] [{}] START", ml.getElement(), ml.getElementId() );
		
		JSONArray arr = (JSONArray) getCurrentObject( ml );
		
		try
		{
			for( int i=0, n=arr.size(); i < n; ++i )
			{
				JSONObject rec = (JSONObject)arr.get( i );
				logger.info( "[{}]", rec );
				
				pushStackNode( new ParentStackNode(ml, i, rec) );
				List<MessageElement> cloneList = ml.cloneMessageElementList( ml.getLayout() );
				ml.appendDataRecord( cloneList );
				
				for( MessageElement me : cloneList )
				{
					visitElement( me );
				}
				
				popStackNode();
			}
		}
		catch( CloneNotSupportedException  e )
		{
			throw new MessageException( e );
		}
		
		logger.info( "visitList() -> [{}] [{}] RECORD COUNT [{}] END", ml.getElement(), ml.getElementId(), ml.getDataRecordCount() );
		
		return ml;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitMessage(Message)
	 */
	@Override
	public Message visitMessage( Message m ) throws MessageException
	{
		logger.info( "visitMessage() -> [{}] [{}] START", m.getElement(), m.getElementId() );
		
		pushStackNode( new ParentStackNode(m, getCurrentObject(m)) );
		
		for( MessageElement me: m.getElementList() )
		{
			visitElement( me );
		}
		
		if( !emptyStack() )
			popStackNode();
		
		logger.info( "visitMessage() -> [{}] [{}] ELEMENT COUNT [{}] END", m.getElement(), m.getElementId(), m.getElementCount() );
		
		return m;
	}
	
	private Object getCurrentObject( MessageElement me )
	{
		if( emptyStack() )
		{
			logger.info( "[{}] [{}] PATH [{}]", me.getElement(), me.getElementId(), getElementPath() );
			if( StringUtil.isBlank(getElementPath()) )
			{
				// MESSAGE 는 자신의 ID 를 PATH 로 사용하지 않는다.
				if( ElementEnum.MESSAGE == me.getElement() )
					return root;
				else
				{
					JSONObject obj = (JSONObject) root.get( me.getElementId() );
					logger.info( "[{}] [{}]  [{}]", me.getElement(), me.getElementId(), obj );
					return obj;
				}
			}
			else
			{
				return createElementPath( root, getElementPath() );
			}
		}
		else
		{
			// 상위 Node Stack 에서 PEEK
			JSONObject parent = (JSONObject)((ParentStackNode)peekStackNode()).getNode();
			logger.info( "[{}] [{}] {}", me.getElement(), me.getElementId(), parent.toJSONString() );
			if( ElementEnum.LIST == me.getElement() || ElementEnum.ARRAY == me.getElement() )
			{
				return (JSONArray) parent.get( me.getElementId() );
			}
			else
			{
				return (JSONObject) parent.get( me.getElementId() );
			}
		}
	}
}
