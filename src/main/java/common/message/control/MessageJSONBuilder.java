/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.ElementEnum;
import common.enums.EncodingEnum;
import common.enums.FieldEnum;
import common.exception.MessageException;
import common.message.*;
import common.utility.StringUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MessageJSONBuilder extends MessageBuilder
{
	private final Logger logger = LoggerFactory.getLogger( MessageJSONBuilder.class );
	protected JSONObject root;
	
	public MessageJSONBuilder( EncodingEnum ee )
	{
		super( ee );
		
		setupVisitor();
	}
	
	public String build()
	{
		return build( true );
	}
	
	public String build( boolean isPretty )
	{
		return root.toJSONString();
	}
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor()
	{
		root = new JSONObject();
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField)
	 */
	@Override
	public MessageField visitField( MessageField mf )
	{
		if( mf.getFieldType() == FieldEnum.NUMBER && StringUtil.isBlank(mf.getStringValue(getEncoding())) )
			return mf;
		
		JSONObject parent = (JSONObject)((ParentStackNode)peekStackNode()).getNode();
		switch( mf.getFieldType() )
		{
		case STRING:
		case VARIABLE:
		case DELIMITER:
			parent.put( mf.getElementId(), mf.getStringValue(getEncoding()) );
			break;
		case NUMBER:
			parent.put( mf.getElementId(), mf.getNumberValue() );
			break;
		default:
		}
		
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup)
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		logger.info( "visitGroup() -> [{}] [{}] START", mg.getElement(), mg.getElementId() );
		JSONObject obj = (JSONObject) getCurrentObject( mg );
		pushStackNode( new ParentStackNode(mg, obj) );
		
		for( MessageElement me: mg.getElementList() )
		{
			visitElement( me );
		}
		
		popStackNode();
		
		setCurrentObject( mg, obj );
		
		logger.info( "visitGroup() -> [{}] [{}] ELEMENT COUNT [{}] END", mg.getElement(), mg.getElementId(), mg.getElementCount() );
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		logger.info( "visitArray() -> [{}] [{}] START", ma.getElement(), ma.getElementId() );
		
		JSONArray array = (JSONArray) getCurrentObject( ma );
		for( int i=0, n=ma.getDataCount(); i < n; ++i )
		{
			switch( ma.getArrayType() )
			{
			case STRING:
				array.add( (String)ma.getDataIndex(i) );
				break;
			case NUMBER:
				if( ma.getPointLength() > 0 )
				{
					array.add( (Double)ma.getDataIndex(i) );
				}
				else
				{
					array.add( (Long)ma.getDataIndex(i) );
				}
				break;
			default:
				throw new MessageException( "Unsupported FieldEnum : " + ma.getArrayType() );
			}
		}
		
		setCurrentObject( ma, array );
		
		logger.info( "visitArray() -> [{}] [{}] ARRAY COUNT [{}] END", ma.getElement(), ma.getElementId(), ma.getDataCount() );
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList)
	 */
	@Override
	public MessageList visitList( MessageList ml ) throws MessageException
	{
		JSONArray list = new JSONArray();
		ParentStackNode psnList = new ParentStackNode( ml, list );
		pushStackNode( psnList );
		
		logger.info( "visitList -> [{}] record count [{}]", ml.getElementId(), ml.getDataRecordCount() );
		for( int i=0, n=ml.getDataRecordCount(); i < n; ++i )
		{
			ParentStackNode psnRec = new ParentStackNode( ml, i, new JSONObject() );
			pushStackNode( psnRec );
			
			List<MessageElement> rec = ml.getDataRecord( i );
			
			for( MessageElement me: rec )
			{
				visitElement( me );
			}
			
			popStackNode();
			list.add( psnRec.getNode() );
		}
		
		popStackNode();
		if( emptyStack() )
		{
			root.put( ml.getElementId(), list );
		}
		else
		{
			((JSONObject)((ParentStackNode)peekStackNode()).getNode()).put( ml.getElementId(), list );
		}
		
		return ml;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitMessage(Message)
	 */
	@Override
	public Message visitMessage( Message m ) throws MessageException
	{
		logger.info( "visitMessage() -> [{}] [{}] PATH [{}]", m.getElement(), m.getElementId(), getElementPath() );
		
		JSONObject obj = (JSONObject) getCurrentObject( m );
		pushStackNode( new ParentStackNode(m, obj) );
		
		for( MessageElement me: m.getElementList() )
		{
			visitElement( me );
		}
		
		if( !emptyStack() )
			popStackNode();
		
		logger.info( "visitMessage() -> [{}] [{}] ELEMENT COUNT [{}] END", m.getElement(), m.getElementId(), m.getElementCount() );
		return m;
	}
	
	/**
	 * Stack을 확인하여 부모 Element 가 존재하면, 부모 Element 에 현재 Element를 연결하고,
	 * 부모 Element가 존재하지 않으면, Document Root에 연결한다.
	 */
	public Object getCurrentObject( MessageElement me )
	{
		if( emptyStack() )
		{
			logger.debug( "[{}] [{}] PATH [{}]", me.getElement(), me.getElementId(), getElementPath() );
			if( StringUtil.isBlank(getElementPath()) )
			{
				// MESSAGE 는 자신의 ID 를 PATH 로 사용하지 않는다.
				if( ElementEnum.MESSAGE == me.getElement() )
					return root;
				else
				{
					if( ElementEnum.LIST == me.getElement() || ElementEnum.ARRAY == me.getElement() )
						return new JSONArray();
					else
						return new JSONObject();
				}
			}
			else
			{
				return createElementPath( root, getElementPath() );
			}
		}
		else
		{
			if( ElementEnum.LIST == me.getElement() || ElementEnum.ARRAY == me.getElement() )
				return new JSONArray();
			else
				return new JSONObject();
		}
	}
	
	public void setCurrentObject( MessageElement me, Object obj )
	{
		JSONObject parent;
		if( emptyStack() )
		{
			parent = root;
		}
		else
		{
			parent = (JSONObject) ((ParentStackNode)peekStackNode()).getNode();
		}
		
		logger.debug( "setCurrentObject() -> [{}][{}] [{}]", me.getElement(), me.getElementId(), parent );
		
		if( ElementEnum.LIST == me.getElement() || ElementEnum.ARRAY == me.getElement() )
			parent.put( me.getElementId(), (JSONArray)obj );
		else
			parent.put( me.getElementId(), (JSONObject)obj );
	}
}
