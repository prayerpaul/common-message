/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.*;
import common.exception.MessageException;
import common.message.*;
import common.utility.ByteUtil;
import common.utility.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Message 의 모든 항목을 출력하고 해당 길이만큼 Padding 하여 출력한다.
 */
public class MessageTextPrinter extends AbstractVisitor
{
	private final Logger logger = LoggerFactory.getLogger( MessageTextPrinter.class );
	protected StringBuilder sb;
	private final int indentWidth;
	private int currIndent;
	
	public MessageTextPrinter( EncodingEnum ee )
	{
		this( ee, 2 );
	}
	
	public MessageTextPrinter( EncodingEnum ee, int width )
	{
		super( ee );
		this.indentWidth = width;
		this.currIndent  = 0;
		//this.currIndent  = width;
		
		setupVisitor();
	}
	
	public String print()
	{
		return sb.toString();
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor()
	{
		sb = new StringBuilder();
		sb.append( CharacterEnum.LINE_FEED.getString() );
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField)
	 */
	@Override
	public MessageField visitField( MessageField mf ) throws MessageException
	{
		String padding = getCurrentIndent( mf );

		byte[] idBytes = ByteUtil.rpadBytes( mf.getElementId().getBytes(getEncoding().getCharset()), CharacterEnum.SPACE.getByte(), 50 );
		
		if( FieldEnum.NUMBER == mf.getFieldType() )
			sb.append( String.format("%s[%-5s][%s][%6s][%04d][%B][%s]%n", padding, mf.getElement(), new String(idBytes, getEncoding().getCharset()), mf.getFieldType(), mf.getLength(), mf.isPrintable(), mf.getStringValueByLength(getEncoding(), CharacterEnum.ZERO.getByte())) );
		else
			sb.append( String.format("%s[%-5s][%s][%6s][%04d][%B][%s]%n", padding, mf.getElement(), new String(idBytes, getEncoding().getCharset()), mf.getFieldType(), mf.getLength(), mf.isPrintable(), mf.getStringValueByLength(getEncoding(), CharacterEnum.SPACE.getByte())) );
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup)
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		String padding = getCurrentIndent( mg );
		
		// ****************************************************************************************************
		sb.append(StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.ASTERISK.getChar())).append(CharacterEnum.LINE_FEED.getString());
		// INDENT[ELEMENT] : [ELEMENT ID][ELEMENT NAME][RECORD COUNT]
		//     [GROUP] : [세대주정보][]
		sb.append( String.format("%s[%-5s] : [%s][%s]\n", padding, mg.getElement(), mg.getElementId(), mg.getElementName()) );
		// ----------------------------------------------------------------------------------------------------
		//sb.append(StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.HYPHEN.getChar())).append(CharacterEnum.LINE_FEED.getString());
		
		// 들여쓰기 추가
		currIndent += indentWidth;
		pushStackNode( new ParentStackNode(mg, String.format("%s%s", StringUtil.rpad(CharacterEnum.BLANK.getString(), indentWidth, CharacterEnum.SPACE.getChar()), padding)) );
		
		for( int i=0, n=mg.getElementCount(); i < n; ++i )
		{
			visitElement( mg.getElement(i) );
		}
		
		//들여쓰기 삭제
		popStackNode();
		currIndent -= indentWidth;
		
		// ****************************************************************************************************
		sb.append(StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.ASTERISK.getChar())).append(CharacterEnum.LINE_FEED.getString());
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		String padding = getCurrentIndent( ma );
		
		// ****************************************************************************************************
		sb.append(StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.ASTERISK.getChar())).append(CharacterEnum.LINE_FEED.getString());
		// INDENT[ELEMENT] : [ELEMENT ID][ELEMENT NAME][RECORD COUNT]
		//     [ARRAY] : [세대주정보][]
		sb.append( String.format("%s[%-5s] : [%s][%s] COUNT [%d]\n", padding, ma.getElement(), ma.getElementId(), ma.getElementName(), ma.getDataCount()) );
		// ----------------------------------------------------------------------------------------------------
		//sb.append( StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.HYPHEN.getChar()) ).append( CharacterEnum.LINE_FEED.getString() );
		
		int countData = 0;
		
		String referId = getReferElementPath( ma );
		logger.info( "visitList -> [{}], [{}], REFER [{}]", ma.getElement(), ma.getElementId(), referId );
		ParentStackNode psn = (ParentStackNode)peekStackNode();
		logger.info( "PARENT [{}] : [{}]", psn.getElement(), psn.getElementId() );
		
		// ARRAY 의 DATA COUNT 계산
		if( StringUtil.isBlank(referId) )
		{
			MessageMacroEnum mme = ma.getMacro( MessageMacroEnum.MACRO_FIXED_SIZE );
			if( MessageMacroEnum.MACRO_FIXED_SIZE == mme )
				countData = Integer.parseInt( ma.getDefaultValue() );
		}
		else
		{
			MessageField mf = (MessageField) (psn.getMessageElement()).findElement( referId );
			if( null == mf )
			{
				MessageMacroEnum mme = ma.getMacro( MessageMacroEnum.MACRO_FIXED_SIZE );
				if( MessageMacroEnum.MACRO_FIXED_SIZE == mme )
					countData = Integer.parseInt( ma.getDefaultValue() );
			}
			else
			{
				if( StringUtil.isBlank(mf.getData()) )
					countData = 0;
				else
				{
					countData = Integer.parseInt( mf.getData() );
					logger.info( "[{}] : [{}] FIELD DATA [{}], LIST [{}] COUNT [{}]", mf.getElement(), mf.getElementId(), countData, ma.getElementId(), ma.getDataCount() );
				}
			}
		}
		
		padding = String.format( "%s%s", StringUtil.rpad(CharacterEnum.BLANK.getString(), indentWidth, CharacterEnum.SPACE.getChar()), padding );
		for( int i=0; i < countData; ++i )
		{
			if( ArrayEnum.NUMBER == ma.getArrayType() )
				sb.append( String.format("%s[%03d][%6s][%04d][%B][%s]\n", padding, i, ma.getArrayType(), ma.getLength(), ma.isPrintable(), ma.getStringValueByLength(i, CharacterEnum.ZERO.getByte(), getEncoding())) );
			else
				sb.append( String.format("%s[%03d][%6s][%04d][%B][%s]\n", padding, i, ma.getArrayType(), ma.getLength(), ma.isPrintable(), ma.getStringValueByLength(i, CharacterEnum.SPACE.getByte(), getEncoding())) );
		}
		
		currIndent -= indentWidth;
		// ****************************************************************************************************
		sb.append(StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.ASTERISK.getChar())).append(CharacterEnum.LINE_FEED.getString());
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList)
	 */
	@Override
	public MessageList visitList( MessageList ml ) throws MessageException
	{
		int countRecord = 0;
		
		String referId = getReferElementPath( ml );
		logger.info( "visitList -> [{}], [{}], REFER [{}]", ml.getElement(), ml.getElementId(), referId );
		ParentStackNode psn = (ParentStackNode)peekStackNode();
		logger.info( "PARENT [{}] : [{}]", psn.getElement(), psn.getElementId() );
		
		// LIST 의 RECORD COUNT 계산
		if( StringUtil.isBlank(referId) )
		{
			MessageMacroEnum mme = ml.getMacro( MessageMacroEnum.MACRO_FIXED_SIZE );
			if( MessageMacroEnum.MACRO_FIXED_SIZE == mme )
				countRecord = Integer.parseInt( ml.getDefaultValue() );
		}
		else
		{
			MessageField mf = (MessageField) (psn.getMessageElement()).findElement( referId );
			if( null == mf )
			{
				MessageMacroEnum mme = ml.getMacro( MessageMacroEnum.MACRO_FIXED_SIZE );
				if( MessageMacroEnum.MACRO_FIXED_SIZE == mme )
					countRecord = Integer.parseInt( ml.getDefaultValue() );
			}
			else
			{
				if( StringUtil.isBlank(mf.getData()) )
					countRecord = 0;
				else
				{
					countRecord = Integer.parseInt( mf.getData() );
					logger.info( "[{}] : [{}] FIELD DATA [{}], LIST [{}] COUNT [{}]", mf.getElement(), mf.getElementId(), countRecord, ml.getElementId(), ml.getDataRecordCount() );
				}
			}
		}
		
		String padding = getCurrentIndent( ml );
		
		// ****************************************************************************************************
		sb.append( StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.ASTERISK.getChar()) ).append( CharacterEnum.LINE_FEED.getString() );
		// INDENT[ELEMENT] : [ELEMENT ID][ELEMENT NAME][RECORD COUNT]
		//     [LIST ] : [주소이력][] COUNT [5]
		sb.append( String.format("%s[%-5s] : [%s][%s] COUNT [%d]\n", padding, ml.getElement(), ml.getElementId(), ml.getElementName(), ml.getDataRecordCount()) );
		// ----------------------------------------------------------------------------------------------------
		//sb.append( StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.HYPHEN.getChar()) ).append( CharacterEnum.LINE_FEED.getString() );
		
		padding = String.format( "%s%s", StringUtil.rpad(CharacterEnum.BLANK.getString(), indentWidth, CharacterEnum.SPACE.getChar()), padding );
		// List 의 Record Count 에서 건수 필드의 건수 값만큼 Loop 가 실행되도록 수정
		for( int i=0; i < countRecord; ++i )
		{
			String recPad = String.format( "%s[%03d]", padding, i );
			
			//
			pushStackNode( new ParentStackNode(ml, i, recPad) );
			
			List<MessageElement> list = ml.getDataRecord( i );
			for( MessageElement me: list )
			{
				// INDENT[RECORD NUM]
				//     [000]
				// sb.append( String.format("%s[%03d]", StringUtil.rpad(CharacterEnum.BLANK.getString(), idnt, CharacterEnum.SPACE.getChar()), i) );
				visitElement( me );
			}
			// ----------------------------------------------------------------------------------------------------
			sb.append( StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.HYPHEN.getChar()) ).append( CharacterEnum.LINE_FEED.getString() );
			
			//
			popStackNode();
		}
		
		// ****************************************************************************************************
		sb.append( StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.ASTERISK.getChar()) ).append( CharacterEnum.LINE_FEED.getString() );
		
		return ml;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitMessage(Message)
	 */
	@Override
	public Message visitMessage( Message m ) throws MessageException
	{
		String padding = getCurrentIndent( m );
		// ####################################################################################################
		sb.append( StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.HASH.getChar())).append(CharacterEnum.LINE_FEED.getString() );
		// [ELEMENT] : [ELEMENT ID][ELEMENT NAME][ENCODING]
		// Ex) [MESSAGE] : [본인정보확인_여신][][UTF-8]
		sb.append( String.format("%s[%-7s] : [%s][%s][%s]\n", padding, m.getElement(), m.getElementId(), m.getElementName(), getEncoding().getString()) );
		
		currIndent += indentWidth;
		padding = String.format( "%s%s", StringUtil.rpad(CharacterEnum.BLANK.getString(), indentWidth, CharacterEnum.SPACE.getChar()), padding );
		pushStackNode( new ParentStackNode(m, padding) );
		//pushStackNode( new ParentStackNode(m, StringUtil.rpad(CharacterEnum.BLANK.getString(), currIndent, CharacterEnum.SPACE.getChar())) );
		
		for( MessageElement me: m.getElementList() )
		{
			visitElement( me );
		}
		
		//들여쓰기 삭제
		popStackNode();
		
		// ####################################################################################################
		sb.append( StringUtil.rpad(CharacterEnum.BLANK.getString(), 100, CharacterEnum.HASH.getChar())).append(CharacterEnum.LINE_FEED.getString() );
		currIndent -= indentWidth;
		return m;
	}
	
	public String getCurrentIndent( MessageElement me ) throws MessageException
	{
		if( emptyStack() )
		{
			if( currIndent == 0 )
				return "";
			else
				return StringUtil.rpad( CharacterEnum.BLANK.getString(), currIndent, CharacterEnum.SPACE.getChar() );
		}
		else
		{
			return (String)((ParentStackNode)peekStackNode()).getNode();
		}
	}
}
