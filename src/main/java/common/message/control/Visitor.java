/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.exception.MessageException;
import common.message.MessageElement;
public interface Visitor
{
	void visit( MessageElement me ) throws MessageException;
}