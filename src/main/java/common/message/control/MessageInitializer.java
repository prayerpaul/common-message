/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message.control
 * FILE      : MessageInitializer.java
 * DATE      : 2022.08.09
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageInitializer extends AbstractVisitor
{
	private final Logger logger = LoggerFactory.getLogger( MessageInitializer.class );
	public MessageInitializer()
	{
		super( EncodingEnum.NULL );
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor() {}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField)
	 */
	@Override
	public MessageField visitField( MessageField mf )
	{
		mf.clearData();
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup)
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		for( MessageElement me : mg.getElementList() )
		{
			visitElement( me );
		}
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		ma.clearData();
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList)
	 */
	@Override
	public MessageList visitList( MessageList ml )
	{
		ml.clearData();
		return ml;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitMessage(Message)
	 */
	@Override
	public Message visitMessage( Message m ) throws MessageException
	{
		for( MessageElement me : m.getElementList() )
		{
			visitElement( me );
		}
		
		return m;
	}
}
