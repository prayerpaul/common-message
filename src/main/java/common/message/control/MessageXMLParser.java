/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message.control
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message.control;

import common.enums.CharacterEnum;
import common.enums.ElementEnum;
import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.*;
import common.utility.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

public class MessageXMLParser extends MessageParser
{
	private final Logger logger = LoggerFactory.getLogger( MessageXMLParser.class );
	protected Document doc;
	protected XPath    xpath;
	//protected Stack<String> stack = new Stack<>();
	
	public MessageXMLParser( String xml, EncodingEnum ee )
	{
		super( xml, ee );
		
		setupVisitor();
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#setupVisitor()
	 */
	@Override
	public void setupVisitor()
	{
		try
		{
			InputSource is = new InputSource( new StringReader(getParserStrData()) );
			DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
			
			// XXE Attack Mitigation
			f.setFeature  ( "http://apache.org/xml/features/disallow-doctype-decl"   , true  );
			f.setFeature  ( "http://xml.org/sax/features/external-general-entities"  , false );
			f.setFeature  ( "http://xml.org/sax/features/external-parameter-entities", false );
			f.setAttribute( XMLConstants.ACCESS_EXTERNAL_DTD   , CharacterEnum.BLANK.getString() );
			f.setAttribute( XMLConstants.ACCESS_EXTERNAL_SCHEMA, CharacterEnum.BLANK.getString() );
			
			doc = f.newDocumentBuilder().parse( is );
			
			xpath = XPathFactory.newInstance().newXPath();
		}
		catch( SAXException | IOException | ParserConfigurationException e )
		{
			logger.error( "XML Parse Exception : ", e );
		}
	}
	
	/**
	 * XML 파싱을 위해서 XPATH 를 설정한다.
	 * @param me 파싱하고자 하는 Message 에 정의된 MessageElement
	 * @return 설정된 XPATH 경로
	 */
	private String getCurrentPath( MessageElement me )
	{
		String parent = "";
		String current = "";
		if( emptyStack() )
		{
			if( StringUtil.isBlank(getElementPath()) )
			{
				// MESSAGE 는 자신의 ID 를 XML PATH 로 사용하지 않는다.
				if( ElementEnum.MESSAGE == me.getElement() )
					current = "";
				else
					current = me.getElementId();
			}
			else
				current = getElementPath();
		}
		else
		{
			parent = (String) ((ParentStackNode) peekStackNode()).getNode();
			current = parent + getPathDelimiter() + me.getElementId();
		}
		
		return current;
	}
	
	// Getter
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitField(MessageField)
	 * Ex)
	 * <과세년도>2021</과세년도>
	 */
	@Override
	public MessageField visitField( MessageField mf ) throws MessageException
	{
		String current = getCurrentPath( mf );
		
		try
		{
			Node node = (Node) xpath.evaluate( current, doc, XPathConstants.NODE );
			if( null == node )
			{
				logger.error( "[{}][{}] is null", mf.getElementId(), current );
				return mf;
			}
			
			mf.setStringValue( node.getTextContent(), getEncoding() );
			logger.debug( "[{}][{}][{}]", mf.getElement(), mf.getElementId(), mf.getStringValue(getEncoding()) );
		}
		catch( Exception e )
		{
			throw new MessageException( e );
		}
		
		return mf;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitGroup(MessageGroup)
	 * Ex)
	 * <발급일자>
	 *     <년/>
	 *     <월/>
	 *     <일/>
	 * </발급일자>
	 */
	@Override
	public MessageGroup visitGroup( MessageGroup mg ) throws MessageException
	{
		String current = getCurrentPath( mg );
		logger.info( "visitGroup() -> [{}] [{}], PATH [{}]", mg.getElement(), mg.getElementId(), current );
		
		pushStackNode( new ParentStackNode(mg, current) );
		logger.info( "visitGroup() -> stack.push() : [{}]", ((ParentStackNode)peekStackNode()).getNode() );
		
		List<MessageElement> list = mg.getElementList();
		for( MessageElement me: list )
		{
			visitElement( me );
		}
		
		String path = (String) ((ParentStackNode)popStackNode()).getNode();
		logger.info( "visitGroup() -> stack.pop() : [{}]", path );
		
		logger.info( "visitGroup() -> [{}] [{}], ELEMENT COUNT [{}] END", mg.getElement(), mg.getElementId(), mg.getElementCount() );
		
		return mg;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitArray(MessageArray)
	 * Ex) 과세년도, 자료유형과 같은 동일한 XML Element 가 반복적으로 있는 형태의 데이터 구조를 처리함.
	 * <증명내용>
	 *     <출력정보>
	 *         <과세년도>2021</과세년도>
	 *         <자료유형>C 연말정산</자료유형>
	 *         <과세년도>2020</과세년도>
	 *         <자료유형>C 연말정산</자료유형>
	 *         <과세년도>2019</과세년도>
	 *         <자료유형>C 연말정산</자료유형>
	 *     </출력정보>
	 * </증명내용>
	 */
	@Override
	public MessageArray visitArray( MessageArray ma ) throws MessageException
	{
		String current = getCurrentPath( ma );
		logger.info( "visitArray() -> [{}] [{}], PATH [{}]", ma.getElement(), ma.getElementId(), current );
		
		String recCntExpr = "count(" + current + ")";
		try
		{
			XPathExpression expr = xpath.compile( recCntExpr );
			Double count = (Double)expr.evaluate( doc, XPathConstants.NUMBER );
			logger.info( "COUNT : [{}]", count.intValue() );
			
			for( int i=1, n=count.intValue(); i <= n; ++i )
			{
				String arrPath = current + "[" + i + "]";
				Node node = (Node) xpath.evaluate( arrPath, doc, XPathConstants.NODE );
				logger.info( "[{}]th : [{}]", i, node.getTextContent() );
				ma.addData( node.getTextContent() );
			}
			
			logger.info( "visitArray() -> [{}] [{}], ARRAY COUNT [{}] END", ma.getElement(), ma.getElementId(), ma.getDataCount() );
		}
		catch( Exception  e )
		{
			throw new MessageException( e );
		}
		
		return ma;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitList(MessageList)
	 * Ex) 아래와 같이 XML Element 와 자식 Element 들이 반복적으로 있는 형태의 데이터 구조를 처리함.
	 * <주소이력>
	 *     <전입일>19911201</전입일>
	 *     <변동일>19911201</변동일>
	 *     <변동사유>행정구역변경</변동사유>
	 *     <주소>충청남도 서천군 망우리</주소>
	 *     <도로명주소-이력/>
	 * </주소이력>
	 * <주소이력>
	 *     <전입일>19950101</전입일>
	 *     <변동일>19950101</변동일>
	 *     <변동사유>행정구역변경</변동사유>
	 *     <주소>충청남도 서천군 망우리</주소>
	 *     <도로명주소-이력/>
	 * </주소이력>
	 */
	@Override
	public MessageList visitList( MessageList ml ) throws MessageException
	{
		String current = getCurrentPath( ml );
		logger.info( "visitList() -> [{}] [{}], PATH [{}]", ml.getElement(), ml.getElementId(), current );
		
		String recCntExpr = "count(" + current + ")";
		try
		{
			XPathExpression expr = xpath.compile( recCntExpr );
			Double count = (Double)expr.evaluate( doc, XPathConstants.NUMBER );
			logger.info( "COUNT : [{}]", count.intValue() );
			
			for( int i=1, n=count.intValue(); i <= n; ++i )
			{
				String recPath = current + "[" + i + "]";
				pushStackNode( new ParentStackNode(ml, i, recPath) );
				logger.info( "visitList() -> stack.push() : RECORD [{}][{}]", i, ((ParentStackNode)peekStackNode()).getNode() );
				
				List<MessageElement> cloneList = ml.cloneMessageElementList( ml.getLayout() );
				ml.appendDataRecord( cloneList );
				
				logger.info( "RECORD : [{}][{}]", i, recPath );
				for( MessageElement me: cloneList )
				{
					logger.info( "visitElement start -> [{}][{}]", me.getElement(), me.getElementId() );
					visitElement( me );
					logger.info( "visitElement   end -> [{}][{}]", me.getElement(), me.getElementId() );
				}
				
				String path = (String) ((ParentStackNode)popStackNode()).getNode();
				logger.info( "visitList() -> stack.pop() : RECORD [{}][{}]", i, path );
			}
			
			logger.info( "visitList() -> [{}] [{}], RECORD COUNT [{}] END", ml.getElement(), ml.getElementId(), ml.getDataRecordCount() );
		}
		catch( Exception  e )
		{
			throw new MessageException( e );
		}
		
		return ml;
	}
	
	/**
	 * (non-Javadoc)
	 * @see AbstractVisitor#visitMessage(Message)
	 */
	@Override
	public Message visitMessage( Message m ) throws MessageException
	{
		String current = getCurrentPath( m );
		logger.info( "visitMessage() -> [{}] [{}], PATH [{}]", m.getElement(), m.getElementId(), current );
		
		pushStackNode( new ParentStackNode(m, current) );
		//pushStackNode( new ParentStackNode(m, getElementPath()) );
		logger.debug( "visitMessage() -> stack.push() : [{}]", ((ParentStackNode)peekStackNode()).getNode() );
		
		List<MessageElement> list = m.getElementList();
		
		for( MessageElement me: list )
		{
			visitElement( me );
		}
		
		String root = (String) ((ParentStackNode)popStackNode()).getNode();
		logger.debug( "visitMessage() -> stack.pop() : [{}]", root );
		
		logger.info( "visitMessage() -> [{}] [{}], ELEMENT COUNT [{}] END", m.getElement(), m.getElementId(), m.getElementCount() );
		return m;
	}
}
