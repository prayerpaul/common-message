/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import common.enums.CharacterEnum;
import common.utility.StringUtil;

import common.enums.ElementEnum;
import common.enums.EncodingEnum;
import common.enums.FieldEnum;
import common.exception.MessageException;
import common.message.control.Visitor;
import common.utility.ByteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author prayerpaul
 *
 */
public class MessageField extends MessageElement
{
    private final Logger logger = LoggerFactory.getLogger( MessageField.class );
    /**
     *
     */
    private static final long serialVersionUID = 5805128556330361774L;
    private int length;
    private int point;
    private FieldEnum fe;
    private String data;
    
    public MessageField( String id, String name )
    {
        super( id, name, ElementEnum.FIELD );
        this.fe = FieldEnum.STRING;
        this.data = CharacterEnum.BLANK.getString();
        this.setLength( 0 );
        this.setPoint( 0 );
    }
    
    /* Getter */
    public FieldEnum getFieldType()
    {
        return this.fe;
    }
    
    public int getPointLength()
    {
        return this.point;
    }
    
    public int getLength()
    {
        return this.length;
    }
    
    public String getData() { return this.data; }
    
    public String getStringValueByLength( EncodingEnum ee )
    {
        switch( getFieldType() )
        {
        case STRING:
        case VARIABLE:
        case DELIMITER:
            return getStringValueByLength( ee, CharacterEnum.SPACE.getByte() );
        case NUMBER:
            return getStringValueByLength( ee, CharacterEnum.ZERO.getByte() );
        default:
            return null;
        }
    }
    public String getStringValueByLength( EncodingEnum ee, byte ch )
    {
        return new String( getByteValueByLength(ch, ee), ee.getCharset() );
    }
    
    public String getStringValue( EncodingEnum ee )
    {
        byte[] b = getByteValue( ee );
        if( null == b )
            return null;
        
        String value = new String( b, ee.getCharset() );
        logger.debug( "getStringValue() -> [{}] : [{}] [{}]", getElementId(), ee, value );
        
        return value;
    }
    
    /**
     * MessageField의 길이만큼의 byte array 데이터를 리턴한다.
     *
     * @param ch 데이터가 길이보다 작을 때, padding할 character
     * @param ee EncodingEnum
     * @return MessageField의 길이만큼의 byte array
     * <p>
     * ex) getByteValueByLength( '0', EncodingEnum.EUCKR );
     */
    public byte[] getByteValueByLength( byte ch, EncodingEnum ee )
    {
        int length = getLength();
        byte[] strArr = getByteValue( ee );
        if( null == strArr )
            strArr = new byte[0];
    
        switch( getFieldType() )
        {
        case STRING:
        case VARIABLE:
        case DELIMITER:
            return ByteUtil.getStringByteByLength( strArr, length, ch );
        case NUMBER:
            return ByteUtil.getNumericByteByLength( strArr, length, getPointLength(), ch );
        default:
            return null;
        }
        /*
        if( strArr.length == length )
            return strArr;
        else if( strArr.length > length )
            return ByteUtil.copyByte( strArr, 0, length );
        else
        {
            if( FieldEnum.NUMBER == getFieldType() )
                return ByteUtil.lpadBytes( strArr, ch, getLength() );
            else
                return ByteUtil.rpadBytes( strArr, ch, getLength() );
        }
        */
    }
    
    public byte[] getByteValue( EncodingEnum ee )
    {
        byte[] value = null;
        if( null == data ) return value;
        
        value = data.getBytes( ee.getCharset() );
        
        return value;
    }
    
    public Object getValue( EncodingEnum ee )
    {
        switch( getFieldType() )
        {
        case STRING:
        case DELIMITER:
        case VARIABLE:
            return getStringValue(ee);
        case NUMBER:
            return getNumberValue();
        default:
        }
        
        return null;
    }
    
    public Object getNumberValue()
    {
        if( this.point > 0 )
            return Double.valueOf( data );
        else
            return Long.valueOf( data );
    }
    
    
    public int getByteLength( EncodingEnum ee )
    {
        if( StringUtil.isBlank(data) ) return 0;
        
        if( EncodingEnum.NULL == ee )
            return data.getBytes().length;
    
        return data.getBytes(ee.getCharset()).length;
    }
    
    /* Setter */
    public void setData( String value) { this.data = value; }
    public void setFieldType( FieldEnum fe )
    {
        this.fe = fe;
    }
    
    public void setLength( int length )
    {
        this.length = length;
    }
    
    public void setPoint( int point )
    {
        this.point = point;
    }
    
    public void setStringValue( String value, EncodingEnum ee )
    {
        if( null == value )
            return;
        byte[] bytes = value.getBytes( ee.getCharset() );
    
        this.data = new String( bytes, ee.getCharset() );
        logger.debug( "setStringValue() -> [{}] : [{}][{}][{}]", getElementId(), ee, getStringValue(ee), getStringValueByLength(ee) );
    }
    
    public void setByteValue( byte[] value, EncodingEnum ee )
    {
        this.data = new String( value, ee.getCharset() );
        logger.debug( "setByteValue() -> [{}] : [{}][{}][{}]", getElementId(), ee, getStringValue(ee), getStringValueByLength(ee) );
    }
    
    public void setLongValue( long value )
    {
        this.data = Long.toString( value );
    }
    
    public void setDoubleValue( double value )
    {
        this.data = Double.toString( value );
    }
    
    public void clearData() { data = CharacterEnum.BLANK.getString(); }
    /**
     * (non-Javadoc)
     * @see common.message.control.Visitor#visit(MessageElement)
     */
    @Override
    public void accept( Visitor visitor ) throws MessageException
    {
        visitor.visit( this );
    }
    
    /**
     * (non-Javadoc)
     * @see MessageElement#findElement(String)
     */
    @Override
    public MessageElement findElement( String elementId )
    {
        if( elementId.equals(getElementId()) )
            return this;
        return null;
    }
    
    /**
     * (non-Javadoc)
     * @see Object#clone()
     */
    @Override
    public MessageField clone() throws CloneNotSupportedException
    {
        return (MessageField)super.clone();
    }
}
