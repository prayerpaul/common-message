/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.message
 * FILE      : MessageConstable.java
 * DATE      : 2022.08.08
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import common.enums.CharacterEnum;

public interface MessageConstable
{
	// XML Tag
	String TAG_MESSAGE  = "Message";
	String TAG_GROUP    = "Group";
	String TAG_LIST     = "List";
	String TAG_ARRAY    = "Array";
	String TAG_FIELD    = "Field";
	
	// XML Attribute
	String ATTR_ID      = "ID";
	String ATTR_NAME    = "NAME";
	String ATTR_TYPE    = "TYPE";
	String ATTR_LENGTH  = "LENGTH";
	String ATTR_PRINT   = "PRINT";
	String ATTR_REFER   = "REFER";
	String ATTR_DEFAULT = "DEFAULT";
	String ATTR_MACRO   = "MACRO";
	String ATTR_PRE_MACRO    = "PRE_" + ATTR_MACRO;
	String ATTR_POST_MACRO   = "POST_" + ATTR_MACRO;
	String ATTR_DATA_MACRO   = "DATA_" + ATTR_MACRO;
	
	// XML XPath
	String XPATH_MESSAGE = CharacterEnum.SLASH.getString() + TAG_MESSAGE;
	
	//
	String PRINT_YES = "Y";
}
