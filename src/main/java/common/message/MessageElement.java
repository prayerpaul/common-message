/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import common.enums.ElementEnum;
import common.enums.MessageMacroEnum;
import common.exception.MessageException;
import common.message.control.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author prayerpaul
 *
 */
public abstract class MessageElement implements Cloneable, Serializable
{
    private final Logger logger = LoggerFactory.getLogger( MessageElement.class );
    /**
	 * 
	 */
	protected static final long serialVersionUID = 9112061271599729007L;
	private String      id;
    private String      name;
    private ElementEnum elem;
    private boolean     mandatory;   // true:필수, false:옵션
    private boolean     printable;  // true:출력, false:출력제외
    private boolean     encrypted;  // true:암호화대상, false:암호화대상아님
    private String      defaultVal;
    private List<MessageMacroEnum> preMacroList;
    private List<MessageMacroEnum> postMacroList;
    private List<MessageMacroEnum> dataMacroList;
    private String           refer;
    
    // TODO mandatory 에 대한 정의 필요. XML 에서 ATTRIBUTE 를 어떻게 정의할 지 결정 필요.
    
    public MessageElement( String id, String name, ElementEnum e )
    {
        this.id        = id;
        this.name      = name;
        this.elem      = e;
        this.mandatory = true;
        this.printable = true;
        this.encrypted = false;
        this.preMacroList  = new ArrayList<>();
        this.postMacroList = new ArrayList<>();
        this.dataMacroList = new ArrayList<>();
    }
    
    /* Getter */
    public String      getElementId()   { return this.id;   }
    public String      getElementName() { return this.name; }
    public ElementEnum getElement()     { return this.elem; }
    public String      getDefaultValue()     { return this.defaultVal; }
    
    /**
     * PreMacro, DataMacro 목록에서 원하는 매크로가 있는지 검색해서 해당 매크로를 리턴한다.
     * @param mme 검색하고자 하는 MessageMacroEnum
     * @return 성공 : MessageMacroEnum, 실패 : MessageMacroEnum.NULL
     */
    public MessageMacroEnum getMacro( MessageMacroEnum mme )
    {
        for( MessageMacroEnum e : preMacroList )
        {
            if( mme == e )
                return e;
        }
        for( MessageMacroEnum e : dataMacroList )
        {
            if( mme == e )
                return e;
        }
        return MessageMacroEnum.NULL;
    }
    public MessageMacroEnum getPreMacro( int index )
    {
        if( preMacroList.size() == 0 )
            return MessageMacroEnum.NULL;
        return this.preMacroList.get( index );
    }
    public MessageMacroEnum getPostMacro( int index )
    {
        if( postMacroList.size() == 0 )
            return MessageMacroEnum.NULL;
        return this.postMacroList.get( index );
    }
    public MessageMacroEnum getDataMacro( int index )
    {
        if( dataMacroList.size() == 0 )
            return MessageMacroEnum.NULL;
        return this.dataMacroList.get( index );
    }
    public List<MessageMacroEnum> getPreMacroList() { return this.preMacroList; }
    public List<MessageMacroEnum> getPostMacroList() { return this.postMacroList; }
    public List<MessageMacroEnum> getDataMacroList() { return this.dataMacroList; }
    //public MessageMacroEnum getMacro()  { return this.macro; }
    public String      getReferElement()  { return this.refer; }
    /* Setter */
    public void setElementId( String id ) { this.id = id; }
    public void setElementName( String name ) { this.name = name; }
    public void setElement( ElementEnum elem ) { this.elem = elem; }
    public void setMandatory( boolean mandatory ) { this.mandatory = mandatory; }
    public void setPrintable( boolean printable ) { this.printable = printable; }
    public void setEncrypt( boolean encrypted ) { this.encrypted = encrypted; }
    public void setDefaultValue( String value ) { this.defaultVal = value; }
    //public void setMacro( MessageMacroEnum mme ) { this.macro = mme; }
    public void setReferElement( String refer ) { this.refer = refer; }
    
    public boolean isMandatory()  { return this.mandatory; }
    public boolean isPrintable() { return this.printable; }
    public boolean isEncrypted() { return this.encrypted; }
    
    /* Method */
    public void addPreMacro( MessageMacroEnum mme ) { this.preMacroList.add( mme ); }
    public void addPostMacro( MessageMacroEnum mme ) { this.postMacroList.add( mme ); }
    public void addDataMacro( MessageMacroEnum mme ) { this.dataMacroList.add( mme ); }
    public boolean containsPreMacro( MessageMacroEnum mme )
    {
        for( MessageMacroEnum e : preMacroList )
        {
            if( mme == e )
                return true;
        }
        return false;
    }
    public boolean containsPostMacro( MessageMacroEnum mme )
    {
        for( MessageMacroEnum e : postMacroList )
        {
            if( mme == e )
                return true;
        }
        return false;
    }
    public boolean containsDataMacro( MessageMacroEnum mme )
    {
        for( MessageMacroEnum e : dataMacroList )
        {
            if( mme == e )
                return true;
        }
        return false;
    }
    public abstract void           accept( Visitor visitor ) throws MessageException;
    public abstract MessageElement findElement( String elementId );
    
    // TODO findElement 메소드 내에서 elementId 에 대한 Boundary Condition 을 체크하는 메소드 추가 필요.
    // Ex) 본인정보확인_여신.국내거소신고사실증명.거소인정보. 의 마지막 "." 는 제거하는 메소드가 필요.
    
    /**
     * (non-Javadoc)
     * @see Object#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException
    {
        MessageElement clone = (MessageElement)super.clone();
        clone.id        = this.getElementId();
        clone.name      = this.getElementName();
        clone.elem      = this.getElement();
        clone.mandatory = this.isMandatory();
        clone.printable = this.isPrintable();
        
        return clone;
    }
    
    /**
     * MessageGroup, MessageList의 List<MessageElement>를 복제한다.
     * @param list 복제 대상 List<MessageElement>
     * @return 복제된 List<MessageElement>
     * @throws CloneNotSupportedException clone()을 실행할 때, 발생하는 Exception
     */
    public List<MessageElement> cloneMessageElementList( List<MessageElement> list ) throws CloneNotSupportedException
    {
        try
        {
            // Serializing
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream    out  = new ObjectOutputStream( baos );
            out.writeObject( list );
            out.flush();
        
            // Deserializing
            ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() );
            ObjectInputStream    in   = new ObjectInputStream( bais );
            return (List<MessageElement>)in.readObject();
        }
        catch( IOException ie )
        {
            throw new CloneNotSupportedException( "Serialization/Deserialization failed. : " + ie );
        }
        catch( ClassNotFoundException ce )
        {
            throw new CloneNotSupportedException("Deserialization failed. : " + ce);
        }
    }
}
