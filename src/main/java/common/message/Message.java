/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import common.enums.ElementEnum;
import common.enums.EncodingEnum;
import common.exception.MessageException;
import common.message.control.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Message extends MessageElement
{
	private final Logger logger = LoggerFactory.getLogger( Message.class );
	protected List<MessageElement> elementList;
	
	public Message( String id, String name )
	{
		super( id, name, ElementEnum.MESSAGE );
		this.elementList = new ArrayList<>();
	}
	
	// Getter
	public List<MessageElement> getElementList() { return elementList; }
	
	public MessageElement getElement( int index ) { return elementList.get( index ); }
	
	public int getElementCount() { return elementList.size(); }
	
	public MessageElement getElement( String key )
	{
		return findElement( key );
	}
	public String getStringValue( String key, EncodingEnum ee ) throws MessageException
	{
		MessageElement me = getElement( key );
		if( null == me )
			return null;
		else if( me.getElement() != ElementEnum.FIELD )
			throw new MessageException( "Unsupported MessageElement : [" + me.getElement() + "]" );
		
		return ((MessageField)me).getStringValue( ee );
	}
	// Setter
	public void setElementList( List<MessageElement> list )
	{
		elementList.clear();
		elementList = list;
	}

	public void appendElement( MessageElement me ) { elementList.add( me ); }
	
	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		try
		{
			Message clone = (Message)super.clone();
			// Serializing
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream    out  = new ObjectOutputStream( baos );
			out.writeObject( clone );
			out.flush();
			
			// Deserializing
			ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() );
			ObjectInputStream in   = new ObjectInputStream( bais );
			return in.readObject();
		}
		catch( IOException ie )
		{
			throw new CloneNotSupportedException( "Serialization/Deserialization failed." );
		}
		catch( ClassNotFoundException cnfe )
		{
			throw new CloneNotSupportedException( "Deserialization failed." );
		}
		catch( CloneNotSupportedException cnse )
		{
			throw cnse;
		}
	}
	
	
	@Override
	public void accept( Visitor visitor ) throws MessageException
	{
		visitor.visit( this );
	}
	
	/**
	 * (non-Javadoc)
	 * @see common.message.MessageElement#findElement(String)
	 *
	 * ex) 아래의 케이스를 다 만족시켜야 한다.
	 * case 1) GroupId
	 * case 2) GroupId.FieldId
	 * case 3) GroupId.SubGroupId
	 * case 4) GroupId.SubGroupId.FieldId
	 * case 5) GroupId.ListId
	 * case 6) GroupId.ListId[index].FieldId
	 * case 7) GroupId.ListId[index].GroupId
	 * case 8) GroupId.ListId[index].SubListId
	 * case 9) ListId
	 * case 10) ListId[index].FieldId
	 * case 11) ListId[index].SubGroupId
	 * case 12) ListId[index].SubGroupId.FieldId
	 * case 13) ListId[index].ListId
	 * case 14) ListId[index].ListId[index].FieldId
	 * case 15) ListId[index].ListId[index].GroupId
	 * case 16) ListId[index].ListId[index].SubListId
	 * 예외사항) MessageElement를 리턴하지 않기 때문에 아래의 케이스는 처리하지 않음.
	 * GroupId.ListId[index]
	 * ListId[index]
	 * ListId[index].SubListId[index]
	 */
	@Override
	public MessageElement findElement( String elementId )
	{
		for( MessageElement me: getElementList() )
		{
			MessageElement element = me.findElement( elementId );
			if( null != element )
				return element;
		}
		return null;
	}
}
