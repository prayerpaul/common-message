/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.message
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.message;

import common.enums.*;
import common.exception.MessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MessageManager implements MessageConstable
{
	protected Logger logger = LoggerFactory.getLogger( MessageManager.class );
	
	protected Map<String, Message> msgRepo;
	
	protected MessageManager()
	{
		msgRepo  = new ConcurrentHashMap<>();
	}
	
	private static class SingletonHelper
	{
		private static final MessageManager INSTANCE = new MessageManager();
	}
	
	public static MessageManager getInstance()
	{
		return SingletonHelper.INSTANCE;
	}
	
	public boolean containMessage( String id ) { return msgRepo.containsKey( id ); }
	public int getMessageCount() { return msgRepo.size(); }
	public Message getMessage( String msgId ) throws CloneNotSupportedException
	{
		Message m = msgRepo.get( msgId );
		return (Message)m.clone();
	}
	public void load( String file ) throws MessageException
	{
		parseMessageXML( file );
	}
	
	/**
	 * 메시지 정보 XML 을 파싱한다.
	 * @param filePath XML 파일 패스
	 * @throws MessageException
	 */
	public void parseMessageXML( String filePath ) throws MessageException
	{
		try
		{
			InputSource is = new InputSource( Files.newBufferedReader(Paths.get(filePath), EncodingEnum.UTF8.getCharset()) );		// non-blocking I/O and auto-closable
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			
			// XXE Attack Mitigation
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			
			Document document = dbf.newDocumentBuilder().parse(is);
			
			XPath xPath = XPathFactory.newInstance().newXPath();
			
			Node msgNode = (Node)xPath.evaluate( XPATH_MESSAGE, document, XPathConstants.NODE );
			String msgId = msgNode.getAttributes().getNamedItem(ATTR_ID).getTextContent();
			String msgNm;
			try { msgNm = msgNode.getAttributes().getNamedItem(ATTR_NAME).getTextContent(); } catch( Exception e ) { msgNm = ""; }
			logger.debug( "[{}] : [{}]", msgId, msgNm );
			
			Message msg = new Message( msgId, msgNm );
			
			String path = XPATH_MESSAGE + "/*";
			NodeList nodes = (NodeList)xPath.evaluate( path, document, XPathConstants.NODESET );
			
			msg.setElementList( parseNodeList(nodes) );
			
			msgRepo.put ( msg.getElementId(), msg );
		}
		catch( XPathExpressionException | SAXException | IOException | ParserConfigurationException e )
		{
			throw new MessageException( e );
		}
	}
	
	/**
	 *
	 * @param nodeList
	 * @return
	 * @throws MessageException
	 */
	public List<MessageElement> parseNodeList( NodeList nodeList ) throws MessageException
	{
		List<MessageElement> elemList = new ArrayList<>();
		
		for( int i=0; null != nodeList && i < nodeList.getLength(); ++i )
		{
			Node node = nodeList.item( i );
			if( node.getNodeType() != Node.ELEMENT_NODE )  continue;
			
			//logger.trace("[{}] : [{}]", node.getNodeName(), node.getAttributes().getNamedItem(ATTR_ID).getTextContent() );
			ElementEnum ee = ElementEnum.getEnum( node.getNodeName() );
			switch( ee )
			{
			case FIELD:
				//logger.trace("[{}] : [{}] add", node.getNodeName(), node.getAttributes().getNamedItem(ATTR_ID).getTextContent() );
				elemList.add( parseField(node) );
				//msg.addOutputElement( parseField(node) );
				break;
			case GROUP:
				//logger.trace("[{}] : [{}] add", node.getNodeName(), node.getAttributes().getNamedItem(ATTR_ID).getTextContent() );
				elemList.add( parseGroup(node) );
				//msg.addOutputElement( parseGroup(node) );
				break;
			case ARRAY:
				elemList.add( parseArray(node) );
				break;
			case LIST:
				//logger.trace("[{}] : [{}] add", node.getNodeName(), node.getAttributes().getNamedItem(ATTR_ID).getTextContent() );
				elemList.add( parseList(node) );
				//msg.addOutputElement( parseList(node) );
				break;
			case MESSAGE:
				elemList.add( parseMessage(node) );
				break;
			default:
				throw new MessageException( "Unsupported ElementEnum : [" + ee + "]" );
			}
		}
		
		return elemList;
	}
	
	/**
	 * 메시지(Message) 정보를 XML 정보로부터 파싱한다.
	 * @param node 메시지(Message) XML Node
	 * @return
	 * @throws MessageException
	 */
	protected MessageElement parseMessage( Node node ) throws MessageException
	{
		String msgId = node.getAttributes().getNamedItem(ATTR_ID).getTextContent();
		String msgNm;
		try
		{
			msgNm = node.getAttributes().getNamedItem(ATTR_NAME).getTextContent();
		}
		catch( Exception e ) { msgNm = ""; }
		
		Message m = new Message( msgId, msgNm );
		
		setElementAttribute( m, node );
		
		if( !node.hasChildNodes() ) return m;
		
		NodeList list = node.getChildNodes();
		m.setElementList( parseNodeList(list) );
		
		return m;
	}
	
	/**
	 * 메시지 그룹(MessageGroup) 정보를 XML 정보로부터 파싱한다.
	 * @param node 메시지 그룹(MessageGroup) XML Node
	 * @return
	 * @throws MessageException
	 */
	protected MessageElement parseGroup( Node node ) throws MessageException
	{
		String grpId = node.getAttributes().getNamedItem(ATTR_ID).getTextContent();
		// NAME
		String grpNm;
		try	{ grpNm = node.getAttributes().getNamedItem(ATTR_NAME).getTextContent(); } catch( Exception e ) { grpNm = ""; }
		
		MessageGroup mg = new MessageGroup( grpId, grpNm );
		
		setElementAttribute( mg, node );
		
		if( !node.hasChildNodes() ) return mg;
		
		NodeList list = node.getChildNodes();
		mg.setElementList( parseNodeList(list) );
		
		return mg;
	}
	
	/**
	 * 메시지 목록(MessageList) 정보를 XML 정보로부터 파싱한다.
	 * @param node 메시지 목록(MessageList) XML Node
	 * @return
	 * @throws MessageException
	 */
	protected MessageElement parseList( Node node ) throws MessageException
	{
		String listId  = node.getAttributes().getNamedItem(ATTR_ID).getTextContent();
		String listNm;
		try	{ listNm  = node.getAttributes().getNamedItem(ATTR_NAME).getTextContent(); } catch( Exception e ) { listNm = ""; }
		
		MessageList ml = new MessageList( listId, listNm );
		
		setElementAttribute( ml, node );
		
		if( !node.hasChildNodes() ) return ml;
		
		NodeList list = node.getChildNodes();
		ml.setLayoutList( parseNodeList(list) );
		
		return ml;
	}
	
	/**
	 * 메시지 배열(MessageArray) 정보를 XML 정보로부터 파싱한다.
	 * @param node 메시지 배열(MessageArray) XML Node
	 * @return
	 */
	protected MessageElement parseArray( Node node )
	{
		String arrayId      = node.getAttributes().getNamedItem(ATTR_ID).getTextContent();
		String arrayNm;
		String arrayTp;
		String arrayLen;
		int    length;
		int    point = 0;
		ArrayEnum ae;
		
		try	{ arrayNm = node.getAttributes().getNamedItem(ATTR_NAME).getTextContent(); } catch( Exception e ) { arrayNm = ""; }
		
		MessageArray ma = new MessageArray( arrayId, arrayNm );
		
		// 타입
		try
		{
			arrayTp = node.getAttributes().getNamedItem(ATTR_TYPE).getTextContent();
			ae = ArrayEnum.getEnum( arrayTp );
		} catch( Exception e ) { ae = ArrayEnum.STRING; }
		
		// 길이
		try
		{
			arrayLen  = node.getAttributes().getNamedItem(ATTR_LENGTH).getTextContent();
			
			String[] arr = arrayLen.split( CharacterEnum.COMMA.getString() );
			length = Integer.parseInt( arr[0] );
			if( ArrayEnum.NUMBER == ae )
			{
				if( 2 == arr.length )
					point = Integer.parseInt( arr[1] );
			}
		}
		catch( Exception e ) { length = 0; point  = 0; }
		
		ma.setArrayType( ae );
		ma.setLength   ( length );
		ma.setPointLength( point );
		
		setElementAttribute( ma, node );
		
		return ma;
	}
	
	/**
	 * 메시지 필드(MessageField) 정보를 XML 정보로부터 파싱한다.
	 * @param node 메시지 필드(MessageField) XML Node
	 * @return
	 */
	protected MessageElement parseField( Node node )
	{
		String fieldId      = node.getAttributes().getNamedItem(ATTR_ID).getTextContent();
		String fieldName;
		String fieldType;
		String fieldLen;
		int    length;
		int    point = 0;
		FieldEnum fe;
		
		logger.trace( "FIELD : [{}]", fieldId );
		// 필드 명
		try
		{
			fieldName = node.getAttributes().getNamedItem(ATTR_NAME).getTextContent();
		}
		catch( Exception e ) { fieldName = ""; }
		
		MessageField mf = new MessageField( fieldId, fieldName );
		
		// 필드 타입
		try
		{
			fieldType = node.getAttributes().getNamedItem(ATTR_TYPE).getTextContent();
			fe = FieldEnum.getEnum( fieldType );
		}
		catch( Exception e ) { fe = FieldEnum.STRING; }
		
		// 필드 길이
		try
		{
			fieldLen  = node.getAttributes().getNamedItem(ATTR_LENGTH).getTextContent();
			
			String[] arr = fieldLen.split( CharacterEnum.COMMA.getString() );
			length = Integer.parseInt( arr[0] );
			if( FieldEnum.NUMBER == fe )
			{
				if( 2 == arr.length )
					point = Integer.parseInt( arr[1] );
			}
		}
		catch( Exception e ) { length = 0; point  = 0; }
		
		mf.setFieldType( fe );
		mf.setLength   ( length );
		mf.setPoint    ( point );
		
		setElementAttribute( mf, node );

		return mf;
	}
	
	/**
	 * MessageElement 의 공통 속성을 설정한다.
	 * @param me MessageElement
	 * @param node XML Node of MessageElement
	 */
	private void setElementAttribute( MessageElement me, Node node )
	{
		// PRINT
		try
		{
			String isPrintable  = node.getAttributes().getNamedItem(ATTR_PRINT).getTextContent();
			boolean printable = PRINT_YES.equalsIgnoreCase( isPrintable );
			me.setPrintable( printable );
		}
		catch( Exception e ) {}
		// REFER
		try
		{
			String refer  = node.getAttributes().getNamedItem(ATTR_REFER).getTextContent();
			me.setReferElement( refer );
		}
		catch( Exception e ) {}
		// DEFAULT
		try
		{
			String defaultVal  = node.getAttributes().getNamedItem(ATTR_DEFAULT).getTextContent();
			me.setDefaultValue( defaultVal );
		}
		catch( Exception e ) {}
		// MACRO
		try
		{
			// PRE_MACRO
			String preMacro  = node.getAttributes().getNamedItem(ATTR_MACRO).getTextContent();
			String[] macroArr = preMacro.split( CharacterEnum.COLON.getString() );
			for( int i=0, n=macroArr.length; i < n; ++i )
			{
				MessageMacroEnum mme = MessageMacroEnum.getEnum( macroArr[i] );
				me.addPreMacro( mme );
			}
		} catch( Exception e ) {}
		
		try
		{
			// DATA_MACRO
			String dataMacro  = node.getAttributes().getNamedItem(ATTR_DATA_MACRO).getTextContent();
			String[] dataMacroArr = dataMacro.split( CharacterEnum.COLON.getString() );
			for( int i=0, n=dataMacroArr.length; i < n; ++i )
			{
				MessageMacroEnum mme = MessageMacroEnum.getEnum( dataMacroArr[i] );
				me.addDataMacro( mme );
			}
		} catch( Exception e ) {}
		
		try
		{
			// POST_MACRO
			String postMacro  = node.getAttributes().getNamedItem(ATTR_POST_MACRO).getTextContent();
			String[] postMacroArr = postMacro.split( CharacterEnum.COLON.getString() );
			for( int i=0, n=postMacroArr.length; i < n; ++i )
			{
				MessageMacroEnum mme = MessageMacroEnum.getEnum( postMacroArr[i] );
				me.addPostMacro( mme );
			}
		} catch( Exception e ) {}
	}
}
