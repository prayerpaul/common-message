/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common;

/**
 * @author prayerpaul
 *
 */
public interface Constable
{
	/** System Property Key */
	public static final String KEY_OS_NAME        = "os.name";
	public static final String KEY_OS_ARCH        = "os.arch";
	public static final String KEY_JAVA_VERSION   = "java.specification.version";
	public static final String KEY_USER_COUNTRY   = "user.country";
	public static final String KEY_USER_LANGUAGE  = "user.language";
	public static final String KEY_FILE_ENCODING  = "file.encoding";
	public static final String KEY_FILE_SEPERATOR = "file.seperator";
	public static final String KEY_PATH_SEPERATOR = "path.seperator";
	public static final String KEY_LINE_SEPERATOR = "line.seperator";

	/** System Property */
	public static final String OS_NAME        = System.getProperty( KEY_OS_NAME        );
	public static final String OS_ARCH        = System.getProperty( KEY_OS_ARCH        );
	public static final String JAVA_VERSION   = System.getProperty( KEY_JAVA_VERSION   );
	public static final String USER_COUNTRY   = System.getProperty( KEY_USER_COUNTRY   );
	public static final String USER_LANGUAGE  = System.getProperty( KEY_USER_LANGUAGE  );
	public static final String FILE_ENCODING  = System.getProperty( KEY_FILE_ENCODING  );
	public static final String FILE_SEPERATOR = System.getProperty( KEY_FILE_SEPERATOR );
	public static final String PATH_SEPERATOR = System.getProperty( KEY_PATH_SEPERATOR );
	public static final String LINE_SEPERATOR = System.getProperty( KEY_LINE_SEPERATOR );
	
	public static final double MILLI_SECOND    = 1/1000;
	public static final double MICRO_SECOND    = 1/100000;
	
	public static final long   KILO_BYTE      = 1024;
	public static final long   MEGA_BYTE      = KILO_BYTE * 1024;
	public static final long   GIGA_BYTE      = MEGA_BYTE * 1024;
	public static final long   TERA_BYTE      = GIGA_BYTE * 1024;

	public static final int    FILE_EOF       = -1;
	
	public static final String SOAP_ROOT_TAG   = "Envelope";
	public static final String SOAP_XML_BEGIN  = "<Envelope>";
	public static final String SOAP_XML_END    = "</Envelope>";
	public static final String DOT_FOR_SPLIT   = "\\.";
}
