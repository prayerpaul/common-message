/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum FieldEnum implements Enumerable
{
    STRING("S"), NUMBER("N"), VARIABLE("V"), DELIMITER("R"), NULL("");
    
    private final String type;
    
    FieldEnum( String type ) { this.type = type; }
    
    public static FieldEnum getEnum( String type )
    {
        final FieldEnum[] list = FieldEnum.values();
        for( FieldEnum ee : list )
        {
            if( ee.getString().equalsIgnoreCase(type) )
                return ee;
        }
        
        return FieldEnum.NULL;
    }
    
    @Override
    public String getString() { return type; }
}
