/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum TokenPurposeEnum
{
    FINANCE_MYDATA7, FINANCE_MYDATA90, FINANCE_MYDATA365, FINANCE_SUPPORT, COOCON_ADMIN
}
