/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum ElementEnum implements Enumerable
{
    FIELD("FIELD"), ARRAY("ARRAY"), LIST("LIST"), GROUP("GROUP"), MESSAGE("MESSAGE"), NULL("");
    
    private String type;
    
    ElementEnum( String elem ) { this.type = elem; }
    
    public static ElementEnum getEnum( String type )
    {
        final ElementEnum[] list = ElementEnum.values();
        for( ElementEnum ee : list )
        {
            if( ee.getString().equalsIgnoreCase(type) )
                return ee;
        }
        
        return NULL;
    }
    
    @Override
    public String getString() { return type; }
}
