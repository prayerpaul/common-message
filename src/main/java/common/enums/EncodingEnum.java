/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

import java.nio.charset.Charset;

public enum EncodingEnum implements Enumerable
{
    UTF8("UTF-8"), MS949("MS949"), EUCKR("EUC-KR"), ASCII("US-ASCII"), NULL("");
    
    private String type;
    
    EncodingEnum( String elem ) { this.type = elem; }
    
    public static EncodingEnum getEnum( String type )
    {
        final EncodingEnum[] list = EncodingEnum.values();
        for( EncodingEnum ee : list )
        {
            if( ee.getString().equalsIgnoreCase(type) )
                return ee;
        }
        
        return NULL;
    }
    
    public Charset getCharset() { return Charset.forName(type); }

    @Override
    public String getString() { return type; }
}
