/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;


public enum HttpMethodEnum implements Enumerable
{
    GET("GET")
  , POST("POST")
  , CONNECT("CONNECT")
  , PUT("PUT")
  , DELETE("DELETE")
  , OPTIONS("OPTIONS")
  , PATCH("PATCH")
  , NULL("");
    
    private String type;
    
    HttpMethodEnum( String elem ) { this.type = elem; }
    
    public static HttpMethodEnum getEnum( String type )
    {
        final HttpMethodEnum[] list = HttpMethodEnum.values();
        for( HttpMethodEnum ee : list )
        {
            if( ee.getString().equalsIgnoreCase(type) )
                return ee;
        }
        
        return NULL;
    }
    
    @Override
    public String getString() { return type; }
}
