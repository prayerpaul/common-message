/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;


public enum FormatEnum implements Enumerable
{
    TEXT("TEXT"), JSON("JSON"), XML("XML"), NULL("");
    
    private String type;
    
    FormatEnum( String type ) { this.type = type; }
    
    public static FormatEnum getEnum( String type )
    {
        final FormatEnum[] list = FormatEnum.values();
        for( FormatEnum ee : list )
        {
            if( ee.getString().equalsIgnoreCase(type) )
                return ee;
        }
        
        return NULL;
    }
    
    @Override
    public String getString() { return type; }
}
