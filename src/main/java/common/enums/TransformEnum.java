/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum TransformEnum implements Enumerable
{
    XML2TEXT("XML2TEXT"), TEXT2XML("TEXT2XML"), NULL("");
    
    private String type;

    TransformEnum( String elem ) { this.type = elem; }
    
    public static TransformEnum getEnum( String type )
    {
        final TransformEnum[] list = TransformEnum.values();
        for( TransformEnum ee : list )
        {
            if( ee.getString().equalsIgnoreCase(type) )
                return ee;
        }
        
        return NULL;
    }

    @Override
    public String getString() { return type; }
}
