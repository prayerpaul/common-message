/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum CharacterEnum implements Enumerable
{
	  BLANK("")
	, SPACE(" ")
	, EXCLAMATION("!")
	, DOUBLE_QUOTE("\"")
	, HASH("#")
	, DOLLAR("$")
	, PERCENT("%")
	, AMPERSAND("&")
	, SINGLE_QUOTE("'")
	, LEFT_PARENTHESIS("(")
	, RIGHT_PARENTHESIS(")")
	, ASTERISK("*")
	, PLUS("+")
	, COMMA(",")
	, HYPHEN("-")
	, PERIOD(".")
	, SLASH("/")
	, COLON(":")
	, SEMI_COLON(";")
	, LESS_THAN("<")
	, EQUAL("=")
	, GREATER_THAN(">")
	, QUESTION("?")
	, AT("@")
	, LEFT_BRACKET("[")
	, BACK_SLASH("\\")
	, RIGHT_BRACKET("]")
	, CARET("^")
	, UNDERSCORE("_")
	, GRAVE_ACCENT("`")
	, LEFT_BRACE("{")
	, VERTICAL_BAR("|")
	, RIGHT_BRACE("}")
	, TILDE("~")
	, ZERO("0")
	, CARRIAGE_RETURN("\r")
	, LINE_FEED("\n")
	, NULL(null)
	;

	private String oneChar;

	CharacterEnum( String value ) { oneChar = value; }
	
	public static CharacterEnum getEnum( String pad )
	{
		if( null == pad ) return CharacterEnum.NULL;
		
		final CharacterEnum[] list = CharacterEnum.values();
		for( CharacterEnum ce : list )
		{
			if( ce.oneChar.equals(pad) )
				return ce;
		}
		return CharacterEnum.NULL;
	}

	public char   getChar  () { return oneChar.charAt(0); }
	public byte   getByte  () { return (byte)oneChar.charAt(0); }

	@Override
	public String getString()
	{
		return oneChar;
	}
}
