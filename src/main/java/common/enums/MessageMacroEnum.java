/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.enums
 * FILE      : MessageMacroEnum
 * DATE      : 2022.08.10
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum MessageMacroEnum  implements Enumerable
{
		MACRO_LENGTH_INCLUDE( "#LEN_INC" )
	,   MACRO_LENGTH_EXCLUDE ( "#LEN_EXC" )
	,   MACRO_DATE_YYYYMMDD( "#YYYYMMDD" )
	,   MACRO_TIME_HHMMSS( "#HHMMSS" )
	,   MACRO_TIME_MILISEC( "#HHMMSS.ZZZ" )
	,   MACRO_DATETIME( "#YYYYMMDDHHMMSS" )
	,   MACRO_DATETIME_MILI( "#YYYYMMDDHHMMSS.ZZZ" )
	,   MACRO_LIST_CNT( "#LIST_CNT" )
	,   MACRO_RAND_NUM( "#RAND_NUM" )
		// @TODO
		// MessageField 가 숫자 타입, PlainText 형식으로 메시지 작성시 값이 음수인 경우 처리
		// Ex) 길이가 15 : -00000123456789
	,   MACRO_MINUS_PAD_NUM( "#MINUS_PAD_NUM" )
		// @TODO
		// MessageField 가 숫자 타입, PlainText 형식으로 메시지 작성시 값이 음수인 경우 처리
		// Ex) 길이가 15 : 00000-123456789
	,   MACRO_PAD_MINUS_NUM( "#PAD_MINUS_NUM" )
	,   MACRO_URL_ENCODE( "#URL_ENCODE" )
	,   MACRO_URL_DECODE( "#URL_DECODE" )
		// MessageList MACROS
	,   MACRO_BLANK_RECORD_SKIP( "#BLANK_RECORD_SKIP" )
	,   MACRO_FIXED_SIZE( "#FIXED_SIZE" )
	,   MACRO_DELIMITER_BAR( "#DELIMETER_BAR" )
	,   NULL( "" )
	;
		
	private final String strMacro;
	
	MessageMacroEnum( String macro )
	{
		strMacro = macro;
	}
	
	@Override
	public String getString()
	{
		return strMacro;
	}
	
	public static String getFormat( MessageMacroEnum mme )
	{
		switch( mme )
		{
		case MACRO_DATE_YYYYMMDD:
			return "yyyyMMdd";
		case MACRO_TIME_HHMMSS:
			return "HHmmss";
		case MACRO_TIME_MILISEC:
			return "HHmmss.SSS";
		case MACRO_DATETIME:
			return "yyyyMMddHHmmss";
		case MACRO_DATETIME_MILI:
			return "yyyyMMddHHmmss.SSS";
		default:
			return CharacterEnum.BLANK.getString();
		}
	}
	
	public static MessageMacroEnum getEnum( String macro )
	{
		if( null == macro ) return MessageMacroEnum.NULL;
		
		final MessageMacroEnum[] list = MessageMacroEnum.values();
		for( MessageMacroEnum mme : list )
		{
			if( mme.getString().equals(macro) )
				return mme;
		}
		return MessageMacroEnum.NULL;
	}
}
