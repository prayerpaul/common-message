/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.enums
 * FILE      : ArrayEnum.java
 * DATE      : 2022.09.14
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum ArrayEnum implements Enumerable
{
	STRING("S"), NUMBER("N"), NULL("");
	
	private String type;
	
	ArrayEnum( String type ) { this.type = type; }
	
	public static ArrayEnum getEnum( String type )
	{
		final ArrayEnum[] list = ArrayEnum.values();
		for( ArrayEnum ee : list )
		{
			if( ee.getString().equalsIgnoreCase(type) )
				return ee;
		}
		
		return ArrayEnum.NULL;
	}
	
	@Override
	public String getString() { return type; }
}
