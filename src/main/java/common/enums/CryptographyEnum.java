/********************************************************************************
 * PROJECT   : common-message
 * PACKAGE   : common.enums
 * FILE      : CryptographyEnum.java
 * DATE      : 2022.08.10
 * DEVELOPER : PAUL LEE(prayerpaul)
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum CryptographyEnum implements Enumerable
{
		KCREDIT_ENCRYPT( "KCREDIT_ENCRYPT" )
	,   KCREDIT_DECRYPT( "KCREDIT_DECRYPT" )
	,   NULL( "" )
	;
	
	private final String crypto;
	CryptographyEnum( String crypto )
	{
		this.crypto = crypto;
	}
	
	@Override
	public String getString()
	{
		return crypto;
	}
	
	public static CryptographyEnum getEnum( String crypto )
	{
		if( null == crypto ) return CryptographyEnum.NULL;
		
		final CryptographyEnum[] list = CryptographyEnum.values();
		for( CryptographyEnum mme : list )
		{
			if( mme.getString().equals(crypto) )
				return mme;
		}
		return CryptographyEnum.NULL;
	}
}
