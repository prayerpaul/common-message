/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;


public enum InOutEnum implements Enumerable
{
    INPUT("IN"), OUTPUT("OUT"), ALL("ALL"), NULL("");
    
    private String type;
    
    InOutEnum( String elem ) { this.type = elem; }
    
    public static InOutEnum getEnum( String type )
    {
        final InOutEnum[] list = InOutEnum.values();
        for( InOutEnum ee : list )
        {
            if( ee.getString().equalsIgnoreCase(type) )
                return ee;
        }
        
        return NULL;
    }

    @Override
    public String getString() { return type; }
}
