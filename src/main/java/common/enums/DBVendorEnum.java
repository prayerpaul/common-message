/********************************************************************************
 * PROJECT   : Common-Message
 * PACKAGE   : common.enums
 * DATE      : 2022.07.23
 * DEVELOPER : PAUL LEE
 * GIT       : https://gitlab.com/prayerpaul/common-message.git
 ********************************************************************************/
package common.enums;

public enum DBVendorEnum implements Enumerable
{
      ORACLE("ORACLE")
    , TIBERO("TIBERO")
    , POSTGRESQL("POSTGRESQL")
    , MYSQL("MYSQL")
    , MARIA("MARIA")
    , NULL("")
    ;

    private String vendor;

    DBVendorEnum( String value ) { vendor = value; }
    
    public static DBVendorEnum getEnum( String vendor )
    {
        if( null == vendor ) return DBVendorEnum.NULL;
        
        final DBVendorEnum[] list = DBVendorEnum.values();
        for( DBVendorEnum ce : list )
        {
            if( ce.getString().equalsIgnoreCase(vendor) )
                return ce;
        }
        return DBVendorEnum.NULL;
    }
    
    @Override
    public String getString() { return vendor; }
}
